using System;
using System.Collections.Generic;
using System.Linq;
using Games.KingOfTokyo;
using Games.KingOfTokyo.State.Models;
using Games.KingOfTokyo.State.Player;

namespace Games.Tests
{
  public static class StateHelper
  {
    public static PlayerState GetPlayerState(this KingOfTokyoGame state, Guid player)
    {
      var playerState = (state.PlayerState ?? (state.PlayerState = new Dictionary<Guid, PlayerState>()));
      return playerState[player] == null ? (playerState[player] = PlayerState.CreateDefaultState()) : playerState[player];
    }

    public static void AddPlayerState(this KingOfTokyoGame state, Guid player)
    {
      var playerState = (state.PlayerState ?? (state.PlayerState = new Dictionary<Guid, PlayerState>()));
      if (!playerState.ContainsKey(player))
      {
        playerState[player] = PlayerState.CreateDefaultState();
      }
    }

    public static KingOfTokyoGame SetPlayerEnergy(this KingOfTokyoGame state, Guid player, int energy)
    {
      var playerState = (state.PlayerState ?? (state.PlayerState = new Dictionary<Guid, PlayerState>()));
      if (!playerState.ContainsKey(player))
      {
        playerState[player] = PlayerState.CreateDefaultState();
      }
      playerState[player].Energy = energy;
      return state;
    }

    public static KingOfTokyoGame SetPlayerPoints(this KingOfTokyoGame state, Guid player, int points)
    {
      var playerState = (state.PlayerState ?? (state.PlayerState = new Dictionary<Guid, PlayerState>()));
      if (!playerState.ContainsKey(player))
      {
        playerState[player] = PlayerState.CreateDefaultState();
      }
      playerState[player].VictoryPoints = points;
      return state;
    }

    public static KingOfTokyoGame SetPlayerHealth(this KingOfTokyoGame state, Guid player, int hp)
    {
      var playerState = (state.PlayerState ?? (state.PlayerState = new Dictionary<Guid, PlayerState>()));
      if (!playerState.ContainsKey(player))
      {
        playerState[player] = PlayerState.CreateDefaultState();
      }
      playerState[player].HitPoints = hp;
      return state;
    }

    public static KingOfTokyoGame AddPlayerCard(this KingOfTokyoGame state, Guid player, CardType cardType)
    {
      var inventoryState = (state.PlayerInventories ?? (state.PlayerInventories = new Dictionary<Guid, CardType[]>()));
      var playerCards = (inventoryState.ContainsKey(player) ? inventoryState[player] : (inventoryState[player] = new CardType[0]));
      var newCards = playerCards.Append(cardType).ToArray();
      inventoryState[player] = newCards;
      return state;
    }
  }
}