using System;
using System.Collections.Generic;
using FluentAssertions;
using Games.KingOfTokyo;
using Games.KingOfTokyo.State;
using Games.KingOfTokyo.State.Player;
using Games.Shared;

namespace Games.Tests
{
    public static class ResultHelper
    {
        public static ActionHandleResult<KingOfTokyoGame> EmptyKoT
        {
            get
            {
                var gameId = Guid.NewGuid();
                var creatorId = Guid.NewGuid();
                var creator = new KingOfTokyoPlayer
                {
                    Id = creatorId,
                    GameId = gameId,
                    Name = "Test"
                };
                var game = new KingOfTokyoGame(gameId, creator);
                game.Players.Add(creator);
                game.PlayerState = new Dictionary<Guid, PlayerState> { { creatorId, PlayerState.CreateDefaultState() } };
                return new ActionHandleResult<KingOfTokyoGame>(
                    game,
                    creatorId
                );
            }
        }

        public static void ShouldHaveEmittedNoEvents(this ActionHandleResult<KingOfTokyoGame> result)
        {
            result.Events.Should().BeEmpty();
        }

        public static void ShouldHaveEmitted(this ActionHandleResult<KingOfTokyoGame> result, params IGameEvent<KingOfTokyoGame>[] events)
        {
            result.Events.Should().HaveCount(events.Length);
            foreach (var (evnt, expected) in result.Events.Zip(events))
            {
                evnt.Should().BeEquivalentTo(expected);
            }
        }

        public static void ShouldHaveState<T>(this ActionHandleResult<KingOfTokyoGame> result, T state)
            where T : IState
        {
            result.GameState.State.Should().BeEquivalentTo(state);
        }

    }
}