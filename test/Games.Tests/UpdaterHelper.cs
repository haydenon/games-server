using System.Collections.Generic;
using FluentAssertions;
using Games.Shared;
using Games.Shared.StateUpdates;

namespace Games.Tests
{
    public static class UpdaterHelper
    {
        public static MockStateUpdater<T> Value<T>() where T : IGame => new MockStateUpdater<T>();
    }

    public class MockStateUpdater<T> : IStateUpdater<T> where T : IGame
    {
        private readonly ICollection<IStateUpdateDefinition> updates = new List<IStateUpdateDefinition>();

        UpdateFunc<T> IStateUpdater<T>.Update(IStateUpdateDefinition definition)
        {
            updates.Add(definition);
            return (res) => res;
        }


        public void ShouldHaveUpdatedWith(params IStateUpdateDefinition[] definitions)
        {
            updates.Should().HaveCount(definitions.Length);
            foreach (var (expected, received) in definitions.Zip(updates))
            {
                received.Should().Equals(expected);
            }
        }

        public void ShouldNotHaveUpdated()
        {
            updates.Should().BeEmpty();
        }
    }
}