using System;
using FluentAssertions;
using Games.KingOfTokyo;
using Games.KingOfTokyo.Events;
using Games.KingOfTokyo.State.Models;
using Games.KingOfTokyo.State.Player;
using Games.KingOfTokyo.Updates;
using Xunit;

namespace Games.Tests.KingOfTokyo.Updates
{
  public class ChangeVictoryPointsUpdaterTests
  {
    [Fact]
    public void Given_health_increase_then_adds_health()
    {
      // Arrange
      var updater = UpdaterHelper.Value<KingOfTokyoGame>();
      var result = ResultHelper.EmptyKoT;
      var start = 2;
      var add = 7;
      result.GameState.SetPlayerPoints(result.ExecutingPlayer, start);

      // Act
      ChangeVictoryPointsUpdater.HandlePointsChange(updater, new ChangeVictoryPointsUpdate(result.ExecutingPlayer, add))(result);

      // Assert
      result.GameState.GetPlayerState(result.ExecutingPlayer).VictoryPoints.Should().Be(start + add);
    }

    [Fact]
    public void Given_change_player_is_not_executing_player_then_updates_other_player()
    {
      // Arrange
      var updater = UpdaterHelper.Value<KingOfTokyoGame>();
      var result = ResultHelper.EmptyKoT;
      var otherPlayer = Guid.NewGuid();
      var start = 2;
      var add = 7;
      result.GameState.SetPlayerPoints(otherPlayer, start);

      // Act
      ChangeVictoryPointsUpdater.HandlePointsChange(updater, new ChangeVictoryPointsUpdate(otherPlayer, add))(result);

      // Assert
      result.GameState.GetPlayerState(otherPlayer).VictoryPoints.Should().Be(start + add);
      result.GameState.GetPlayerState(result.ExecutingPlayer).VictoryPoints.Should().Be(PlayerState.CreateDefaultState().VictoryPoints);
    }

    [Fact]
    public void Given_health_increase_then_emits_health_change_event()
    {
      // Arrange
      var updater = UpdaterHelper.Value<KingOfTokyoGame>();
      var result = ResultHelper.EmptyKoT;
      var otherPlayer = Guid.NewGuid();
      var start = 2;
      var add = 7;
      result.GameState.SetPlayerPoints(otherPlayer, start);

      // Act
      ChangeVictoryPointsUpdater.HandlePointsChange(updater, new ChangeVictoryPointsUpdate(otherPlayer, add))(result);

      // Assert
      result.ShouldHaveEmitted(new ChangePointsEvent(
          result.GameState.LockState,
          result.ExecutingPlayer,
          result.ExecutingPlayer,
          add,
          new CardType[0]));
    }

    [Fact]
    public void Given_health_decrease_then_removes_health()
    {
      // Arrange
      var updater = UpdaterHelper.Value<KingOfTokyoGame>();
      var result = ResultHelper.EmptyKoT;
      var start = 7;
      var minus = -2;
      result.GameState.SetPlayerPoints(result.ExecutingPlayer, start);

      // Act
      ChangeVictoryPointsUpdater.HandlePointsChange(updater, new ChangeVictoryPointsUpdate(result.ExecutingPlayer, minus))(result);

      // Assert
      result.GameState.GetPlayerState(result.ExecutingPlayer).VictoryPoints.Should().Be(start + minus);
    }

    [Fact]
    public void Given_decrease_larger_then_current_amount_then_stops_at_zero()
    {
      // Arrange
      var updater = UpdaterHelper.Value<KingOfTokyoGame>();
      var result = ResultHelper.EmptyKoT;
      var start = 2;
      var minus = -10;
      result.GameState.SetPlayerPoints(result.ExecutingPlayer, start);

      // Act
      ChangeVictoryPointsUpdater.HandlePointsChange(updater, new ChangeVictoryPointsUpdate(result.ExecutingPlayer, minus))(result);

      // Assert
      result.GameState.GetPlayerState(result.ExecutingPlayer).VictoryPoints.Should().Be(0);
    }

    [Fact]
    public void Given_increase_over_max_then_stops_at_max()
    {
      // Arrange
      var updater = UpdaterHelper.Value<KingOfTokyoGame>();
      var result = ResultHelper.EmptyKoT;
      var start = 15;
      var add = 10;
      result.GameState.SetPlayerPoints(result.ExecutingPlayer, start);

      // Act
      ChangeVictoryPointsUpdater.HandlePointsChange(updater, new ChangeVictoryPointsUpdate(result.ExecutingPlayer, add))(result);

      // Assert
      result.GameState.GetPlayerState(result.ExecutingPlayer).VictoryPoints.Should().Be(20);
    }
  }
}