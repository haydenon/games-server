using Games.KingOfTokyo;
using Games.KingOfTokyo.Events;
using Games.KingOfTokyo.State;
using Games.KingOfTokyo.Updates;
using Moq;
using Xunit;

namespace Games.Tests.KingOfTokyo.Updates
{
    public class BuyCardsUpdaterTests
    {
        [Fact]
        public void Given_player_has_less_energy_than_threshold_then_skips_buy()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            result.GameState.SetPlayerEnergy(result.ExecutingPlayer, BuyCardsUpdater.DiscardCost - 1);

            // Act
            BuyCardsUpdater.BuyCards(updater)(result);

            // Assert
            result.ShouldHaveEmitted(new RefuseCardBuyEvent(result.GameState.LockState, result.ExecutingPlayer, It.IsAny<string>()));
            updater.ShouldHaveUpdatedWith(new EndOfTurnUpdate());
        }

        [Fact]
        public void Given_player_is_dead_then_skips_buy()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            result.GameState.SetPlayerHealth(result.ExecutingPlayer, 0);
            result.GameState.SetPlayerEnergy(result.ExecutingPlayer, BuyCardsUpdater.DiscardCost + 1);

            // Act
            BuyCardsUpdater.BuyCards(updater)(result);

            // Assert
            result.ShouldHaveEmittedNoEvents();
            updater.ShouldHaveUpdatedWith(new EndOfTurnUpdate());
        }

        [Fact]
        public void Given_player_has_more_energy_than_theshold_then_can_buy()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            result.GameState.SetPlayerEnergy(result.ExecutingPlayer, BuyCardsUpdater.DiscardCost);

            // Act
            BuyCardsUpdater.BuyCards(updater)(result);

            // Assert
            result.ShouldHaveEmitted(new BuyCardRequestEvent(result.GameState.LockState, result.ExecutingPlayer));
            result.ShouldHaveState(new BuyCardsState { Player = result.ExecutingPlayer });
            updater.ShouldNotHaveUpdated();
        }
    }
}