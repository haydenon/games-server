using System;
using System.Linq;
using FluentAssertions;
using Games.KingOfTokyo;
using Games.KingOfTokyo.Events;
using Games.KingOfTokyo.State;
using Games.KingOfTokyo.State.Models;
using Games.KingOfTokyo.Updates;
using Xunit;

namespace Games.Tests.KingOfTokyo.Updates
{
    public class EndOfTurnUpdaterTests
    {
        [Fact]
        public void Given_game_has_ended_then_updates_with_game_end()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            result.GameState.SetPlayerHealth(result.ExecutingPlayer, 10);
            result.GameState.SetPlayerHealth(Guid.NewGuid(), 10);
            result.GameState.SetPlayerPoints(Guid.NewGuid(), 20);

            // Act
            EndOfTurnUpdater.EndOfTurn(updater, new EndOfTurnUpdate())(result);

            // Assert
            result.ShouldHaveEmittedNoEvents();
            updater.ShouldHaveUpdatedWith(new EndGameByPointsUpdate());
        }

        [Fact]
        public void Given_game_has_not_ended_then_dont_update_with_game_end()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            result.GameState.SetPlayerHealth(result.ExecutingPlayer, 10);
            result.GameState.SetPlayerHealth(Guid.NewGuid(), 10);

            // Act
            EndOfTurnUpdater.EndOfTurn(updater, new EndOfTurnUpdate())(result);

            // Assert
            updater.ShouldNotHaveUpdated();
        }

        [Fact]
        public void Given_game_has_not_ended_then_select_next_alive_player()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            var players = new Guid[] {
                Guid.NewGuid(),
                result.ExecutingPlayer,
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid()
            };

            var deadPlayer = players[2];
            var nextPlayer = players[3];

            result.GameState.Players.Clear();
            foreach (var playerId in players)
            {
                var health = playerId == deadPlayer ? 0 : 10;
                result.GameState.SetPlayerHealth(playerId, health);
                result.GameState.Players.Add(new KingOfTokyoPlayer
                {
                    Id = playerId
                });
            }

            // Act
            EndOfTurnUpdater.EndOfTurn(updater, new EndOfTurnUpdate())(result);

            // Assert
            result.GameState.State.Should().BeEquivalentTo(new PlayerTurnStartState { Player = nextPlayer });
            result.ShouldHaveEmitted(new PlayerTurnStartEvent(result.GameState.LockState, nextPlayer));
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void Given_next_player_in_tokyo_then_add_points(bool isBay)
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            var nextPlayer = Guid.NewGuid(); ;
            var players = new Guid[] {
                result.ExecutingPlayer,
                nextPlayer,
            };


            result.GameState.Players.Clear();
            foreach (var playerId in players)
            {
                result.GameState.SetPlayerHealth(playerId, 10);
                result.GameState.Players.Add(new KingOfTokyoPlayer
                {
                    Id = playerId
                });
            }

            if (isBay)
            {
                result.GameState.PlayerInTokyoCity = nextPlayer;
            }
            else
            {
                result.GameState.PlayerInTokyoCity = nextPlayer;
            }


            // Act
            EndOfTurnUpdater.EndOfTurn(updater, new EndOfTurnUpdate())(result);

            // Assert
            updater.ShouldHaveUpdatedWith(new ChangeVictoryPointsUpdate(nextPlayer, 1));
        }

        [Fact]
        public void Given_next_player_not_in_tokyo_then_dont_add_points()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            var nextPlayer = Guid.NewGuid(); ;
            var players = new Guid[] {
                result.ExecutingPlayer,
                nextPlayer,
            };


            result.GameState.Players.Clear();
            foreach (var playerId in players)
            {
                result.GameState.SetPlayerHealth(playerId, 10);
                result.GameState.Players.Add(new KingOfTokyoPlayer
                {
                    Id = playerId
                });
            }

            result.GameState.PlayerInTokyoCity = result.ExecutingPlayer;

            // Act
            EndOfTurnUpdater.EndOfTurn(updater, new EndOfTurnUpdate())(result);

            // Assert
            updater.ShouldNotHaveUpdated();
        }

        [Fact]
        public void Given_player_has_rooting_for_the_underdog_with_least_points_then_adds_point_to_player()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            var initialPoints = 4;
            result.GameState.SetPlayerPoints(result.ExecutingPlayer, initialPoints);
            result.GameState.AddPlayerCard(result.ExecutingPlayer, CardType.RootingForTheUnderdog);
            foreach (var idx in Enumerable.Range(0, 3 - 1))
            {
                result.GameState.SetPlayerPoints(Guid.NewGuid(), initialPoints + idx);
            }

            // Act
            EndOfTurnUpdater.EndOfTurn(updater, new EndOfTurnUpdate())(result);

            // Assert
            updater.ShouldHaveUpdatedWith(new ChangeVictoryPointsUpdate(result.ExecutingPlayer, 1, CardType.RootingForTheUnderdog));
        }

        [Fact]
        public void Given_player_has_rooting_for_the_underdog_without_the_least_points_then_doesnt_add_points_to_player()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            var initialPoints = 4;
            result.GameState.SetPlayerPoints(result.ExecutingPlayer, initialPoints);
            result.GameState.AddPlayerCard(result.ExecutingPlayer, CardType.RootingForTheUnderdog);
            foreach (var idx in Enumerable.Range(0, Constants.BayEnabledPlayers - 1))
            {
                result.GameState.SetPlayerPoints(Guid.NewGuid(), initialPoints - 1 + idx);
            }

            // Act
            EndOfTurnUpdater.EndOfTurn(updater, new EndOfTurnUpdate())(result);

            // Assert
            updater.ShouldNotHaveUpdated();
        }

        [Theory]
        [InlineData(6, 1)]
        [InlineData(11, 1)]
        [InlineData(12, 2)]
        [InlineData(17, 2)]
        [InlineData(18, 3)]
        public void Given_player_has_energy_hoarder_then_adds_correct_amount_of_energy(
            int amount,
            int increase)
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            result.GameState.SetPlayerEnergy(result.ExecutingPlayer, amount);
            result.GameState.AddPlayerCard(result.ExecutingPlayer, CardType.EnergyHoarder);

            result.GameState.SetPlayerHealth(new Guid(), 10);

            // Act
            EndOfTurnUpdater.EndOfTurn(updater, new EndOfTurnUpdate())(result);

            // Assert
            updater.ShouldHaveUpdatedWith(new ChangeEnergyUpdate(result.ExecutingPlayer, increase, CardType.EnergyHoarder));
        }

        [Fact]
        public void Given_player_has_inufficient_energy_with_energy_hoarder_then_adds_no_energy()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            result.GameState.SetPlayerEnergy(result.ExecutingPlayer, 5);
            result.GameState.AddPlayerCard(result.ExecutingPlayer, CardType.EnergyHoarder);

            result.GameState.SetPlayerHealth(new Guid(), 10);

            // Act
            EndOfTurnUpdater.EndOfTurn(updater, new EndOfTurnUpdate())(result);

            // Assert
            updater.ShouldNotHaveUpdated();
        }
    }
}