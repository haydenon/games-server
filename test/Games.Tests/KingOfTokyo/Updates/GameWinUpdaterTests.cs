using System;
using System.Linq;
using FluentAssertions;
using Games.KingOfTokyo;
using Games.KingOfTokyo.Events;
using Games.KingOfTokyo.State;
using Games.KingOfTokyo.Updates;
using Xunit;

namespace Games.Tests.KingOfTokyo.Updates
{
    public class GameWinUpdaterTests
    {
        [Fact]
        public void Given_player_with_20_points_then_marks_player_as_won()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            var otherPlayer = Guid.NewGuid();
            result.GameState.SetPlayerPoints(result.ExecutingPlayer, 20);
            result.GameState.SetPlayerHealth(otherPlayer, 10);

            // Act
            GameWinUpdater.EndGame(updater, new EndGameByPointsUpdate())(result);

            // Assert
            result.GameState.State.Should().BeOfType<PlayerWinState>();
            result.GameState.State.As<PlayerWinState>().Player.Should().Be(result.ExecutingPlayer);
            result.ShouldHaveEmitted(new PlayerWinEvent(result.GameState.LockState, result.ExecutingPlayer));
        }

        [Fact]
        public void Given_one_player_left_alive_then_marks_player_as_won()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            var otherPlayer = Guid.NewGuid();
            result.GameState.SetPlayerHealth(result.ExecutingPlayer, 1);
            foreach (var _ in Enumerable.Range(0, Constants.BayEnabledPlayers - 1))
            {
                result.GameState.SetPlayerHealth(Guid.NewGuid(), 0);
            }

            // Act
            GameWinUpdater.EndGameByCombat(updater, new EndGameByCombatUpdate())(result);

            // Assert
            result.GameState.State.Should().BeOfType<PlayerWinState>();
            result.GameState.State.As<PlayerWinState>().Player.Should().Be(result.ExecutingPlayer);
            result.ShouldHaveEmitted(new PlayerWinEvent(result.GameState.LockState, result.ExecutingPlayer));
        }

        [Fact]
        public void Given_no_players_left_alive_then_marks_no_player_as_won()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            var otherPlayer = Guid.NewGuid();
            result.GameState.SetPlayerHealth(result.ExecutingPlayer, 0);
            foreach (var _ in Enumerable.Range(0, Constants.BayEnabledPlayers - 1))
            {
                result.GameState.SetPlayerHealth(Guid.NewGuid(), 0);
            }

            // Act
            GameWinUpdater.EndGameByCombat(updater, new EndGameByCombatUpdate())(result);

            // Assert
            result.GameState.State.Should().BeOfType<PlayerWinState>();
            result.GameState.State.As<PlayerWinState>().Player.Should().Be(null);
            result.ShouldHaveEmitted(new PlayerWinEvent(result.GameState.LockState, null));
        }
    }
}