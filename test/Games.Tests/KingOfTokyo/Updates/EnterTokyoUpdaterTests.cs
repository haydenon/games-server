using System;
using System.Linq;
using FluentAssertions;
using Games.KingOfTokyo;
using Games.KingOfTokyo.Updates;
using Xunit;

namespace Games.Tests.KingOfTokyo.Updates
{
    public class EnterTokyoUpdaterTests
    {
        [Fact]
        public void Given_player_already_in_tokyo_then_dont_enter()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            result.GameState.PlayerInTokyoCity = result.ExecutingPlayer;
            foreach (var _ in Enumerable.Range(0, Constants.BayEnabledPlayers - 1))
            {
                result.GameState.SetPlayerHealth(Guid.NewGuid(), 10);
            }

            // Act
            EnterTokyoUpdater.EnterTokyo(updater, new EnterTokyoUpdate())(result);

            // Assert
            result.GameState.PlayerInTokyoCity.Should().Be(result.ExecutingPlayer);
            result.GameState.PlayerInTokyoBay.Should().Be(null);
        }

        [Fact]
        public void Given_tokyo_already_full_then_dont_enter()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            result.GameState.PlayerInTokyoCity = Guid.NewGuid();
            result.GameState.PlayerInTokyoBay = Guid.NewGuid();
            foreach (var _ in Enumerable.Range(0, Constants.BayEnabledPlayers - 1))
            {
                result.GameState.SetPlayerHealth(Guid.NewGuid(), 10);
            }

            // Act
            EnterTokyoUpdater.EnterTokyo(updater, new EnterTokyoUpdate())(result);

            // Assert
            result.GameState.PlayerInTokyoCity.Should().NotBe(result.ExecutingPlayer);
            result.GameState.PlayerInTokyoBay.Should().NotBe(result.ExecutingPlayer);
        }

        [Fact]
        public void Given_bay_disabled_then_dont_enter()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            result.GameState.PlayerInTokyoCity = Guid.NewGuid();
            foreach (var _ in Enumerable.Range(0, Constants.BayEnabledPlayers - 2))
            {
                result.GameState.SetPlayerHealth(Guid.NewGuid(), 10);
            }

            // Act
            EnterTokyoUpdater.EnterTokyo(updater, new EnterTokyoUpdate())(result);

            // Assert
            result.GameState.PlayerInTokyoCity.Should().NotBe(result.ExecutingPlayer);
            result.GameState.PlayerInTokyoBay.Should().Be(null);
        }

        [Fact]
        public void Given_city_empty_then_enter_city()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            foreach (var _ in Enumerable.Range(0, Constants.BayEnabledPlayers - 1))
            {
                result.GameState.SetPlayerHealth(Guid.NewGuid(), 10);
            }

            // Act
            EnterTokyoUpdater.EnterTokyo(updater, new EnterTokyoUpdate())(result);

            // Assert
            result.GameState.PlayerInTokyoCity.Should().Be(result.ExecutingPlayer);
            result.GameState.PlayerInTokyoBay.Should().Be(null);
        }

        [Fact]
        public void Given_city_full_with_bay_empty_then_enter_bay()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            result.GameState.PlayerInTokyoCity = Guid.NewGuid();
            foreach (var _ in Enumerable.Range(0, Constants.BayEnabledPlayers - 1))
            {
                result.GameState.SetPlayerHealth(Guid.NewGuid(), 10);
            }

            // Act
            EnterTokyoUpdater.EnterTokyo(updater, new EnterTokyoUpdate())(result);

            // Assert
            result.GameState.PlayerInTokyoCity.Should().NotBe(result.ExecutingPlayer);
            result.GameState.PlayerInTokyoBay.Should().Be(result.ExecutingPlayer);
        }
    }
}