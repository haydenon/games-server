using System;
using FluentAssertions;
using Games.KingOfTokyo;
using Games.KingOfTokyo.Events;
using Games.KingOfTokyo.State.Models;
using Games.KingOfTokyo.State.Player;
using Games.KingOfTokyo.Updates;
using Xunit;

namespace Games.Tests.KingOfTokyo.Updates
{
  public class ChangeEnergyUpdaterTests
  {
    [Fact]
    public void Given_energy_increase_then_adds_energy()
    {
      // Arrange
      var updater = UpdaterHelper.Value<KingOfTokyoGame>();
      var result = ResultHelper.EmptyKoT;
      var start = 2;
      var add = 7;
      result.GameState.SetPlayerEnergy(result.ExecutingPlayer, start);

      // Act
      ChangeEnergyUpdater.HandleEnergyChange(updater, new ChangeEnergyUpdate(result.ExecutingPlayer, add))(result);

      // Assert
      result.GameState.GetPlayerState(result.ExecutingPlayer).Energy.Should().Be(start + add);
    }

    [Fact]
    public void Given_change_player_is_not_executing_player_then_updates_other_player()
    {
      // Arrange
      var updater = UpdaterHelper.Value<KingOfTokyoGame>();
      var result = ResultHelper.EmptyKoT;
      var otherPlayer = Guid.NewGuid();
      var start = 2;
      var add = 7;
      result.GameState.SetPlayerEnergy(otherPlayer, start);

      // Act
      ChangeEnergyUpdater.HandleEnergyChange(updater, new ChangeEnergyUpdate(otherPlayer, add))(result);

      // Assert
      result.GameState.GetPlayerState(otherPlayer).Energy.Should().Be(start + add);
      result.GameState.GetPlayerState(result.ExecutingPlayer).Energy.Should().Be(PlayerState.CreateDefaultState().Energy);
    }

    [Fact]
    public void Given_energy_increase_then_emits_energy_change_event()
    {
      // Arrange
      var updater = UpdaterHelper.Value<KingOfTokyoGame>();
      var result = ResultHelper.EmptyKoT;
      var otherPlayer = Guid.NewGuid();
      var start = 2;
      var add = 7;
      result.GameState.SetPlayerEnergy(otherPlayer, start);

      // Act
      ChangeEnergyUpdater.HandleEnergyChange(updater, new ChangeEnergyUpdate(otherPlayer, add))(result);

      // Assert
      result.ShouldHaveEmitted(new ChangeEnergyEvent(
          result.GameState.LockState,
          result.ExecutingPlayer,
          otherPlayer,
          add,
          new CardType[0]));
    }

    [Fact]
    public void Given_energy_decrease_then_removes_energy()
    {
      // Arrange
      var updater = UpdaterHelper.Value<KingOfTokyoGame>();
      var result = ResultHelper.EmptyKoT;
      var start = 7;
      var minus = -2;
      result.GameState.SetPlayerEnergy(result.ExecutingPlayer, start);

      // Act
      ChangeEnergyUpdater.HandleEnergyChange(updater, new ChangeEnergyUpdate(result.ExecutingPlayer, minus))(result);

      // Assert
      result.GameState.GetPlayerState(result.ExecutingPlayer).Energy.Should().Be(start + minus);
    }

    [Fact]
    public void Given_decrease_larger_then_current_amount_then_stops_at_zero()
    {
      // Arrange
      var updater = UpdaterHelper.Value<KingOfTokyoGame>();
      var result = ResultHelper.EmptyKoT;
      var start = 2;
      var minus = -10;
      result.GameState.SetPlayerEnergy(result.ExecutingPlayer, start);

      // Act
      ChangeEnergyUpdater.HandleEnergyChange(updater, new ChangeEnergyUpdate(result.ExecutingPlayer, minus))(result);

      // Assert
      result.GameState.GetPlayerState(result.ExecutingPlayer).Energy.Should().Be(0);
    }

    [Fact]
    public void Given_energy_increase_when_player_has_friend_of_children_then_adds_extra_energy()
    {
      // Arrange
      var updater = UpdaterHelper.Value<KingOfTokyoGame>();
      var result = ResultHelper.EmptyKoT;
      var start = 2;
      var add = 7;
      result.GameState.SetPlayerEnergy(result.ExecutingPlayer, start);
      result.GameState.AddPlayerCard(result.ExecutingPlayer, CardType.FriendOfChildren);

      // Act
      ChangeEnergyUpdater.HandleEnergyChange(updater, new ChangeEnergyUpdate(result.ExecutingPlayer, add))(result);

      // Assert
      result.GameState.GetPlayerState(result.ExecutingPlayer).Energy.Should().Be(start + add + 1);
      result.ShouldHaveEmitted(new ChangeEnergyEvent(
          result.GameState.LockState,
          result.ExecutingPlayer,
          result.ExecutingPlayer,
          add + 1,
          new[] { CardType.FriendOfChildren }));
    }
  }
}