using System;
using System.Collections.Generic;
using Games.KingOfTokyo;
using Games.KingOfTokyo.Cards;
using Games.KingOfTokyo.State.Models;
using Games.KingOfTokyo.State.Player;
using Games.KingOfTokyo.Updates;
using Games.Shared.StateUpdates;
using Xunit;
using static Games.KingOfTokyo.Cards.CardStatUpdate.CardStatType;
using static Games.KingOfTokyo.Cards.CardStatUpdate.CardTarget;

namespace Games.Tests.KingOfTokyo.Updates
{
    public class CardStatChangeUpdaterTests
    {
        private const CardType AnyCardType = (CardType)1000;
        private static object Wrap<A, B>(Func<A, B> func) => func;
        private static object Wrap<A, B, C>(Func<A, B, C> func) => func;

        public static IEnumerable<object[]> GetStats()
        {
            yield return new object[] {
                Energy,
                Wrap<PlayerState, int>((state) => state.Energy),
                Wrap<Guid, int, IStateUpdateDefinition>((players, delta) => new ChangeEnergyUpdate(players, delta, AnyCardType))
            };
            yield return new object[] {
                HitPoints,
                Wrap<PlayerState, int>((state) => state.HitPoints),
                Wrap<Guid, int, IStateUpdateDefinition>((players, delta) => new ChangeHealthUpdate(players, delta, AnyCardType))
            };
            yield return new object[] {
                VictoryPoints,
                Wrap<PlayerState, int>((state) => state.VictoryPoints),
                Wrap<Guid, int, IStateUpdateDefinition>((players, delta) => new ChangeVictoryPointsUpdate(players, delta, AnyCardType))
            };
        }

        [Theory]
        [MemberData(nameof(GetStats))]
        public void Given_card_with_dynamic_change_then_calculates_change_correctly(
            CardStatUpdate.CardStatType statType,
            Func<PlayerState, int> getStat,
            Func<Guid, int, IStateUpdateDefinition> getDefinition)
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            result.GameState
                .SetPlayerEnergy(result.ExecutingPlayer, 2)
                .SetPlayerHealth(result.ExecutingPlayer, 2)
                .SetPlayerPoints(result.ExecutingPlayer, 2);
            var dynamicUpdate = CardStatUpdate.Update(statType, Self, state => getStat(state) * 4);

            // Act
            CardStatChangeUpdater.ActionCardStatUpdate(updater, new ActionCardStatUpdate(AnyCardType, new[] { dynamicUpdate }))(result);

            // Assert
            var player = result.GameState.GetPlayerState(result.ExecutingPlayer);
            updater.ShouldHaveUpdatedWith(getDefinition(result.ExecutingPlayer, 4));
        }

    }
}