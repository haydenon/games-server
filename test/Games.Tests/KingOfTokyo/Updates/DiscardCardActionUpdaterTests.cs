using Games.KingOfTokyo;
using Games.KingOfTokyo.State.Models;
using Games.KingOfTokyo.Updates;
using Games.KingOfTokyo.Events;
using Xunit;
using FluentAssertions;
using System;
using System.Linq;

namespace Games.Tests.KingOfTokyo.Updates
{
    public class DiscardCardActionUpdaterTests
    {
        [Theory]
        [InlineData(CardType.Heal)]
        [InlineData(CardType.JetFighters)]
        [InlineData(CardType.CommuterTrain)]
        public void Given_normal_discard_card_then_does_nothing(CardType type)
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;

            // Act
            DiscardCardActionUpdater.ActionDiscardCard(updater, new ActionDiscardCardUpdate(type))(result);

            // Assert
            result.ShouldHaveEmittedNoEvents();
        }

        [Theory]
        [InlineData(false)]
        [InlineData(true)]
        public void Given_death_from_above_when_player_in_tokyo_then_do_nothing(bool inBay)
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            if (inBay)
            {
                result.GameState.PlayerInTokyoBay = result.ExecutingPlayer;
            }
            else
            {
                result.GameState.PlayerInTokyoCity = result.ExecutingPlayer;
            }

            // Act
            DiscardCardActionUpdater.HandleDeathFromAbove(updater, new DeathFromAboveUpdate())(result);

            // Assert
            result.ShouldHaveEmittedNoEvents();
        }

        [Fact]
        public void Given_no_players_in_tokyo_then_enters()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;

            // Act
            DiscardCardActionUpdater.HandleDeathFromAbove(updater, new DeathFromAboveUpdate())(result);

            // Assert
            result.GameState.PlayerInTokyoCity.Should().Be(result.ExecutingPlayer);
            result.ShouldHaveEmitted(new EnteredTokyoEvent(
                result.GameState.LockState,
                result.ExecutingPlayer,
                false
            ));
        }

        [Fact]
        public void Given_player_in_tokyo_city_but_not_bay_with_bay_enabled_then_enters_bay()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            var otherPlayer = Guid.NewGuid();
            result.GameState.PlayerInTokyoCity = otherPlayer;
            result.GameState.SetPlayerHealth(result.ExecutingPlayer, 10);
            foreach (var _ in Enumerable.Range(0, Constants.BayEnabledPlayers - 1))
            {
                result.GameState.SetPlayerHealth(Guid.NewGuid(), 10);
            }

            // Act
            DiscardCardActionUpdater.HandleDeathFromAbove(updater, new DeathFromAboveUpdate())(result);

            // Assert
            result.GameState.PlayerInTokyoCity.Should().Be(otherPlayer);
            result.GameState.PlayerInTokyoBay.Should().Be(result.ExecutingPlayer);
            result.ShouldHaveEmitted(new EnteredTokyoEvent(
                result.GameState.LockState,
                result.ExecutingPlayer,
                true
            ));
        }

        [Fact]
        public void Given_player_in_tokyo_city_but_not_bay_with_bay_disabled_then_enters_city()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            var otherPlayer = Guid.NewGuid();
            result.GameState.PlayerInTokyoCity = otherPlayer;
            result.GameState.SetPlayerHealth(result.ExecutingPlayer, 10);
            foreach (var _ in Enumerable.Range(0, Constants.BayEnabledPlayers - 2))
            {
                result.GameState.SetPlayerHealth(Guid.NewGuid(), 10);
            }

            // Act
            DiscardCardActionUpdater.HandleDeathFromAbove(updater, new DeathFromAboveUpdate())(result);

            // Assert
            result.GameState.PlayerInTokyoCity.Should().Be(result.ExecutingPlayer);
            result.ShouldHaveEmitted(
              new LeaveTokyoEvent(
                result.GameState.LockState,
                otherPlayer,
                true
              ),
              new EnteredTokyoEvent(
                result.GameState.LockState,
                result.ExecutingPlayer,
                true
              )
            );
        }

        [Fact]
        public void Given_player_in_tokyo_city_and_bay_with_bay_enabled_then_enters_city()
        {
            // Arrange
            var updater = UpdaterHelper.Value<KingOfTokyoGame>();
            var result = ResultHelper.EmptyKoT;
            var otherPlayerCity = Guid.NewGuid();
            var otherPlayerBay = Guid.NewGuid();
            result.GameState.PlayerInTokyoCity = otherPlayerCity;
            result.GameState.PlayerInTokyoBay = otherPlayerBay;
            result.GameState.SetPlayerHealth(result.ExecutingPlayer, 10);
            foreach (var _ in Enumerable.Range(0, Constants.BayEnabledPlayers - 1))
            {
                result.GameState.SetPlayerHealth(Guid.NewGuid(), 10);
            }

            // Act
            DiscardCardActionUpdater.HandleDeathFromAbove(updater, new DeathFromAboveUpdate())(result);

            // Assert
            result.GameState.PlayerInTokyoCity.Should().Be(result.ExecutingPlayer);
            result.GameState.PlayerInTokyoBay.Should().Be(otherPlayerBay);
            result.ShouldHaveEmitted(
              new LeaveTokyoEvent(
                result.GameState.LockState,
                otherPlayerCity,
                true
              ),
              new EnteredTokyoEvent(
                result.GameState.LockState,
                result.ExecutingPlayer,
                true
              )
            );
        }
    }
}