using System;
using System.Collections.Generic;
using Games.Data.Entities;

namespace Games.Data.Interfaces
{
    public interface IUserRecordStore<T> : IRecordStore<T> where T : IUserEntity
    {
        IAsyncEnumerable<T> GetAll(Guid userId);
    }
}