using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Games.Data.Entities;
using LanguageExt;

namespace Games.Data.Interfaces
{
    public interface IRecordStore<T> where T : IEntity
    {
        Task<T> Create(T item);

        Task<T> Update(T item);

        Task<Unit> Delete(Guid itemId);

        Task<T?> GetById(Guid itemId);

        IAsyncEnumerable<T> GetAll();
    }
}