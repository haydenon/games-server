using Games.Data.Entities;

namespace Games.Data.Interfaces
{
    public interface IStoreFactory
    {
        IUserRecordStore<T> GetUserRecordStore<T>() where T : IUserEntity;

        IRecordStore<T> GetRecordStore<T>() where T : IEntity;
    }
}