using System;
using System.Threading.Tasks;
using LanguageExt;

namespace Games.Data.Interfaces
{
    public interface ILookupStore
    {
        Task<Unit> Create(string lookup, Guid id);

        OptionAsync<Guid> GetLookupId(string lookup);

        Task<string[]> GetLookups();

        Task<Unit> Delete(string lookup);
    }
}