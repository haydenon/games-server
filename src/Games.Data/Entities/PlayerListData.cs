using System.Collections.Generic;
using Google.Cloud.Firestore;

namespace Games.Data.Entities
{
    [FirestoreData]
    public class PlayerListData : IEntity
    {
        [FirestoreProperty]
        public string? Id { get; set; }

        [FirestoreProperty]
        public List<string>? Players { get; set; }
    }
}