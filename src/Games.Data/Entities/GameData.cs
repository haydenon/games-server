using System;
using Google.Cloud.Firestore;

namespace Games.Data.Entities
{
    [FirestoreData]
    public class GameData : IUserEntity
    {
        [FirestoreProperty]
        public string? Id { get; set; }

        [FirestoreProperty]
        public string? OwnerId { get; set; }

        [FirestoreProperty]
        public string? Type { get; set; }

        [FirestoreProperty]
        public string? Lookup { get; set; }

        [FirestoreProperty]
        public DateTime LastUpdated { get; set; }

        [FirestoreProperty]
        public bool? HasJoined { get; set; }
    }
}