namespace Games.Data.Entities
{
    public interface IUserEntity : IEntity
    {
        string? OwnerId { get; set; }
    }
}