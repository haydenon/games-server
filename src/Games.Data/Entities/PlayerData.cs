using Google.Cloud.Firestore;

namespace Games.Data.Entities
{
    [FirestoreData]
    public class PlayerData : IUserEntity
    {
        [FirestoreProperty]
        public string? Id { get; set; }

        [FirestoreProperty]
        public string? OwnerId { get; set; }

        [FirestoreProperty]
        public string? GameId { get; set; }

        [FirestoreProperty]
        public string? Name { get; set; }
    }
}