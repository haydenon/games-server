using System;
using System.Collections.Generic;
using Google.Cloud.Firestore;

namespace Games.Data.Entities.KingOfTokyo
{
    [FirestoreData]
    public class KingOfTokyoGameData : IEntity
    {
        [FirestoreProperty]
        public string? Id { get; set; }

        [FirestoreProperty]
        public int LockState { get; set; }

        [FirestoreProperty]
        public KingOfTokyoStateData? State { get; set; }

        [FirestoreProperty]
        public string? CreatorPlayerId { get; set; }

        [FirestoreProperty]
        public List<KingOfTokyoPlayerData>? Players { get; set; }

        [FirestoreProperty]
        public IDictionary<string, PlayerStateData>? PlayerState { get; set; }

        [FirestoreProperty]
        public IDictionary<string, string?>? Avatars { get; set; }

        [FirestoreProperty]
        public IDictionary<string, string[]>? PlayerInventories { get; set; }

        [FirestoreProperty]
        public string? PlayerInTokyoCity { get; set; }

        [FirestoreProperty]
        public string? PlayerInTokyoBay { get; set; }

        [FirestoreProperty]
        public DiceRollData[]? DiceRoll { get; set; }

        [FirestoreProperty]
        public CardData[]? CardStore { get; set; }

        [FirestoreProperty]
        public string[]? DiscardedCards { get; set; }
    }
}