using Google.Cloud.Firestore;

namespace Games.Data.Entities.KingOfTokyo
{

    [FirestoreData]
    public class CardData
    {
        [FirestoreProperty]
        public string? DisplayName { get; set; }

        [FirestoreProperty]
        public string? CardType { get; set; }

        [FirestoreProperty]
        public int EnergyCost { get; set; }

        [FirestoreProperty]
        public bool IsDiscard { get; set; }
    }
}