using Google.Cloud.Firestore;

namespace Games.Data.Entities.KingOfTokyo
{
    [FirestoreData]
    public class PlayerStateData
    {
        [FirestoreProperty]
        public int HitPoints { get; set; }

        [FirestoreProperty]
        public int VictoryPoints { get; set; }

        [FirestoreProperty]
        public int Energy { get; set; }
    }
}