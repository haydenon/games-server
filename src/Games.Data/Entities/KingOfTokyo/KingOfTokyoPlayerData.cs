using Google.Cloud.Firestore;

namespace Games.Data.Entities.KingOfTokyo
{
    [FirestoreData]
    public class KingOfTokyoPlayerData
    {
        [FirestoreProperty]
        public string? Id { get; set; }

        [FirestoreProperty]
        public string? GameId { get; set; }

        [FirestoreProperty]
        public string? Name { get; set; }
    }
}