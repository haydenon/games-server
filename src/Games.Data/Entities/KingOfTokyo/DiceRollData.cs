using Google.Cloud.Firestore;

namespace Games.Data.Entities.KingOfTokyo
{
    [FirestoreData]
    public class DiceRollData
    {
        [FirestoreProperty]
        public string? DiceRollType { get; set; }

        [FirestoreProperty]
        public bool WasPreserved { get; set; }
    }
}