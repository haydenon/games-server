using System.Collections.Generic;
using Google.Cloud.Firestore;

namespace Games.Data.Entities.KingOfTokyo
{
    [FirestoreData]
    public class KingOfTokyoStateData
    {
        [FirestoreProperty]
        public string? State { get; set; }

        [FirestoreProperty]
        public Dictionary<string, object?>? Attributes { get; set; }
    }

    public enum StateType
    {
        BuyCards,
        LeaveTokyo,
        LobbyState,
        PlayerRollStart,
        PlayerRoll,
        PlayerTurnStart,
        PlayerWin
    }
}