namespace Games.Data.Entities
{
    public interface IEntity
    {
        string? Id { get; set; }
    }
}