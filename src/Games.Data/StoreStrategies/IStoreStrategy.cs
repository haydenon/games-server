namespace Games.Data.StoreStrategies
{
    public interface IStoreStrategy
    {
        IStoreCollection<T> Collection<T>(string collectionName);
    }
}