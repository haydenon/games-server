using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Games.Data.Entities;
using Games.Data.Interfaces;
using Games.Data.StoreStrategies;
using Google.Cloud.Firestore;
using LanguageExt;
using NewRelic.Api.Agent;
using static LanguageExt.Prelude;

namespace Games.Data
{
    public class RecordStore<T> : IRecordStore<T> where T : IEntity
    {
        protected readonly string modelName;
        protected readonly IStoreStrategy strategy;

        public RecordStore(IStoreStrategy strategy)
        {
            this.strategy = strategy;
            modelName = typeof(T).Name;
        }

        [Trace]
        public async Task<T> Create(T item)
        {
            if (string.IsNullOrWhiteSpace(item.Id) || item.Id == Guid.Empty.ToString())
            {
                item.Id = Guid.NewGuid().ToString();
            }

            await strategy.Collection<T>(modelName).SetAsync(item.Id.ToString(), item);

            return item;
        }

        [Trace]
        public async Task<T> Update(T item)
        {
            if (item.Id != null)
            {
                await strategy.Collection<T>(modelName).SetAsync(item.Id.ToString(), item);
            }
            return item;
        }

        [Trace]
        public async Task<Unit> Delete(Guid itemId)
        {
            await strategy.Collection<T>(modelName).DeleteAsync(itemId.ToString());
            return unit;
        }

        [Trace]
        public Task<T?> GetById(Guid itemId)
            => strategy.Collection<T>(modelName).GetAsync(itemId.ToString());

        [Trace]
        public async IAsyncEnumerable<T> GetAll()
        {
            await foreach (var value in strategy.Collection<T>(modelName).GetAllAsync())
            {
                if (value != null)
                {
                    yield return value;
                }
            }
        }
    }

    public class UserRecordStore<T> : RecordStore<T>, IUserRecordStore<T> where T : IUserEntity
    {
        public UserRecordStore(IStoreStrategy strategy) : base(strategy) { }

        [Trace]
        public IAsyncEnumerable<T> GetAll(Guid userId)
            => strategy.Collection<T>(modelName).WhereEqualTo(nameof(IUserEntity.OwnerId), userId.ToString());
    }

    public class LookupStore : ILookupStore
    {
        private const string LookupKey = "Lookup";
        private readonly IStoreStrategy stategy;

        [FirestoreData]
        public class Lookup
        {
            [FirestoreProperty]
            public string? Id { get; set; }

            [FirestoreProperty]
            public string? LookupValue { get; set; }
        }

        public LookupStore(IStoreStrategy strategy)
        {
            this.stategy = strategy;
        }

        [Trace]
        public Task<Unit> Create(string lookup, Guid id)
            => stategy.Collection<Lookup>(LookupKey).SetAsync(lookup, new Lookup { Id = id.ToString(), LookupValue = lookup });

        private async Task<Option<Guid>> GetLookupIdValue(string lookup)
        {
            var remoteLookup = await stategy.Collection<Lookup>(LookupKey).GetAsync(lookup);
            return remoteLookup != null && remoteLookup.Id != null ? Guid.Parse(remoteLookup.Id) : None;
        }

        [Trace]
        public OptionAsync<Guid> GetLookupId(string lookup)
            => GetLookupIdValue(lookup).ToAsync();

        [Trace]
        public async Task<string[]> GetLookups()
        {
            var keys = await stategy.Collection<Lookup>(LookupKey).GetAllAsync().ToListAsync();
            return keys.Select(lookup => lookup?.LookupValue!).ToArray();
        }

        [Trace]
        public Task<Unit> Delete(string lookup)
            => stategy.Collection<Lookup>(LookupKey).DeleteAsync(lookup);
    }
}