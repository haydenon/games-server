using Games.Data.Entities;
using Games.Data.Interfaces;
using Games.Data.StoreStrategies;

namespace Games.Data
{
    public class StoreFactory : IStoreFactory
    {
        private readonly IStoreStrategy strategy;

        public StoreFactory(IStoreStrategy strategy)
        {
            this.strategy = strategy;
        }

        public IRecordStore<T> GetRecordStore<T>() where T : IEntity
            => new RecordStore<T>(strategy);

        public IUserRecordStore<T> GetUserRecordStore<T>() where T : IUserEntity
            => new UserRecordStore<T>(strategy);
    }
}