using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Games.Data.Interfaces;
using Games.Interfaces;
using Games.Errors;
using Games.Games.Models;
using Games.Players.Interfaces;
using Games.Players.Models;
using Games.Shared;
using LanguageExt;
using Microsoft.Extensions.Configuration;
using static Games.Errors.AppError;
using static Games.Prelude;
using static LanguageExt.Prelude;
using System.Text.Json;
using Games.Lookups;
using System.Linq;
using Microsoft.Extensions.Logging;
using Games.Data.Entities;
using AutoMapper;
using Games.Repository;

namespace Games
{
    public class GameService : IGameService
    {
        private const int InactiveExpiryHours = 3;

        private readonly IConfiguration configuration;
        private readonly IPlayerService playerService;
        private readonly ILookupService lookupService;
        private readonly IGameConnector connector;
        private readonly IUserRepository<Game> gameRepository;
        private readonly ILogger<GameService> logger;


        public GameService(
            ILogger<GameService> logger,
            IConfiguration configuration,
            IPlayerService playerService,
            ILookupService lookupService,
            IUserRepository<Game> gameRepository,
            IGameConnector connector)
        {
            this.logger = logger;
            this.playerService = playerService;
            this.lookupService = lookupService;
            this.connector = connector;
            this.gameRepository = gameRepository;
            this.configuration = configuration;
        }

        public async Task<IEnumerable<Game>> GetGames(Guid userId)
        {
            var list = await gameRepository.GetAllForUser(userId).ToListAsync();
            return list;
        }

        public async Task<Game> CreateGame(GameType type, Guid userId)
        {
            var gameId = Guid.NewGuid();
            var lookup = await lookupService.CreateLookup(gameId);
            var game = new Game
            {
                Id = gameId,
                Type = type,
                OwnerId = userId,
                Lookup = lookup,
                LastUpdated = DateTime.UtcNow
            };

            var createGame = await gameRepository.Create(game);
            var creator = await playerService.GetPlayerForGame(userId, gameId);
            await connector.CreateGameInstance(game, creator);
            return createGame;
        }

        public EitherAsync<AppError, Game> GetGameForUser(Guid userId, Guid gameId)
            => GetGame(gameId)
                .Bind<Game>(game => game.OwnerId == userId ?
                    (EitherAsync<AppError, Game>)game :
                    NotFoundError(gameId));

        public EitherAsync<AppError, Game> GetGame(Guid gameId)
            => GetGameOption(gameId)
                .ToEitherAsync(NotFoundError(gameId));

        private async Task<Option<Game>> GetGameOption(Guid gameId)
        {
            var game = await gameRepository.GetById(gameId);
            return game != null ? game : None;
        }

        public EitherAsync<AppError, Game> GetGameAndMarkUpdated(Guid gameId)
        {
            var game = GetGame(gameId);
            return game.Map(g =>
            {
                g.LastUpdated = DateTime.UtcNow;
                return g;
            });
        }

        public EitherAsync<AppError, Game> GetGameForLookup(string lookup, Guid userId)
         => from gameId in lookupService.GetIdFromLookup(lookup).ToEither(NotFound($"No game exists with the lookup value {lookup}"))
            from game in GetGame(gameId)
            from player in rightAsync<Player, AppError>(playerService.GetPlayerForGame(userId, game.Id))
            from hasJoined in rightAsync<Game, AppError>(MarkJoined(game, player))
            select game;

        private async Task<Game> MarkJoined(Game game, Player player)
        {
            var joined = await connector.HasJoinedGame(game, player);
            game.HasJoined = joined;
            return game;
        }

        public EitherAsync<AppError, IGameView> GetGameState(Guid gameId, Guid userId, int? stateLock)
            => from game in GetGame(gameId)
               from player in rightAsync<Player, AppError>(playerService.GetPlayerForGame(userId, gameId))
               from state in connector.GetGameState(game, player, stateLock).MapLeft(AppError.LockStateError)
               select state;

        public EitherAsync<AppError, IGameView> SetGameState(Guid gameId, Guid userId, JsonElement state)
        {
            if (!configuration.GetValue<bool>("DebugEnabled"))
            {
                return Forbidden("You cannot set game state out of debug mode");
            }

            return from game in GetGame(gameId)
                   from player in rightAsync<Player, AppError>(playerService.GetPlayerForGame(userId, gameId))
                   from view in connector.SetGameState(game, player, state).MapLeft(Invalid)
                   select view;
        }

        public async Task<Unit> DeleteGame(Game game)
        {
            await gameRepository.Delete(game.Id);
            if (game.Lookup != null)
            {
                await connector.RemoveGameInstance(game);
                await lookupService.DeleteLookup(game.Lookup);
                await playerService.RemoveForGame(game.Id);
            }

            return unit;
        }

        public async Task<Unit> CleanExpiredGames()
        {
            logger.LogInformation("Cleaning expired games.");
            var expiryThreshold = DateTime.UtcNow.Subtract(TimeSpan.FromHours(InactiveExpiryHours));
            var count = 0;
            await foreach (var game in gameRepository.GetAll().Where(game => game.LastUpdated < expiryThreshold))
            {
                count++;
                await DeleteGame(game);
            }

            logger.LogInformation($"Removed {count} expired games.");

            return unit;
        }

        private static AppError NotFoundError(Guid gameId) => NotFound($"No game exists with the id '{gameId}'");
    }
}