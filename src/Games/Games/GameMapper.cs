using Games.Games.Models;
using Games.Mapping;
using Games.Views;

namespace Games
{
    public class GameMapper : IViewMapper<Game, GameView>
    {
        public GameView MapToView(Game model)
            => new GameView(
                model.Id.ToString(),
                model.Type.ToString(),
                model.Lookup,
                model.HasJoined
            );
    }
}