using System;
using System.Collections.Generic;
using System.Linq;
using Games.Interfaces;
using Games.KingOfTokyo;
using Games.KingOfTokyo.State;
using Games.KingOfTokyo.State.Player;
using Games.KingOfTokyo.View;
using Games.KingOfTokyo.View.Models;
using Games.KingOfTokyo.View.State;
using Games.Shared;

namespace Games.Connections.KingOfTokyo
{
    public class KingOfTokyoStateMapper : IGameStateMapper<KingOfTokyoGame, KingOfTokyoView>
    {
        public KingOfTokyoView MapToView(KingOfTokyoGame game, Guid accessingPlayer)
        {
            return new KingOfTokyoView
            {
                CreatorPlayerId = game.CreatorPlayerId.ToString(),
                AccessingPlayerId = accessingPlayer.ToString(),
                LockState = game.LockState,
                State = MapState(game.State),
                Players = game.Players.Select(MapPlayer(game.Avatars)).ToList(),
                PlayerState = game.PlayerState?.ToDictionary(p => p.Key.ToString(), p => PlayerStateView.FromState(p.Value)),
                PlayerInventories = game.PlayerInventories?.ToDictionary(p => p.Key.ToString(), p => p.Value.Select(c => c.ToString()).ToArray()),
                PlayerInTokyoCity = game.PlayerInTokyoCity?.ToString(),
                PlayerInTokyoBay = game.PlayerInTokyoBay?.ToString(),
                DiceRoll = game.DiceRoll.Select(DiceRollView.FromDiceRoll).ToArray(),
                CardStore = game.CardStore.Select(KingOfTokyoCardView.FromCard).ToArray(),
                DiscardedCards = game.DiscardedCards?.Select(c => c.ToString()).ToArray()
            };
        }

        private Func<IPlayer<KingOfTokyoGame>, KingOfTokyoPlayerView> MapPlayer(IDictionary<Guid, AvatarType?> avatars)
            => (player) => KingOfTokyoPlayerView.FromPlayer(player, avatars.ContainsKey(player.Id) ? avatars[player.Id] : null);

        private static IKingOfTokyoStateView MapState(IState state)
            => state switch
            {
                LobbyState lobby => new LobbyStateView(),
                PlayerTurnStartState turnStart => PlayerTurnStartStateView.FromState(turnStart),
                PlayerRollStartState rollStart => PlayerRollStartStateView.FromState(rollStart),
                PlayerRollState roll => PlayerRollView.FromState(roll),
                PlayerWinState winState => PlayerWinStateView.FromState(winState),
                LeaveTokyoState leaveTokyoState => LeaveTokyoStateView.FromState(leaveTokyoState),
                BuyCardsState buyCards => new SimplePlayerStateView("BuyCards", buyCards.Player),
                _ => throw new NotImplementedException()
            };
    }
}