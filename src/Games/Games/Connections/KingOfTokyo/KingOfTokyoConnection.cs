using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Games.Data.Interfaces;
using Games.Hubs.Models;
using Games.Interfaces;
using Games.KingOfTokyo;
using Games.KingOfTokyo.Actions;
using Games.KingOfTokyo.View;
using Games.Players.Models;
using Games.Shared;
using LanguageExt;
using static LanguageExt.Prelude;
using static Games.Prelude;
using System.Text.Json;
using Games.Players.Interfaces;
using Games.Data.Entities.KingOfTokyo;
using AutoMapper;
using Games.Repository;

namespace Games.Connections.KingOfTokyo
{
    public class KingOfTokyoConnection : IGameConnection
    {
        private readonly IPlayerService playerService;
        private readonly IGameExecutor<KingOfTokyoGame> executor;
        private readonly IActionMapper<KingOfTokyoGame> actionMapper;
        private readonly IEventMapper<KingOfTokyoGame> eventMapper;
        private readonly IGameStateMapper<KingOfTokyoGame, KingOfTokyoView> gameStateMapper;
        private readonly IRepository<KingOfTokyoGame> gameRepository;

        public KingOfTokyoConnection(
            IRepository<KingOfTokyoGame> gameRepository,
            IPlayerService playerService,
            IGameExecutor<KingOfTokyoGame> executor,
            IActionMapper<KingOfTokyoGame> actionMapper,
            IEventMapper<KingOfTokyoGame> eventMapper,
            IGameStateMapper<KingOfTokyoGame, KingOfTokyoView> gameStateMapper)
        {
            this.executor = executor;
            this.actionMapper = actionMapper;
            this.eventMapper = eventMapper;
            this.gameStateMapper = gameStateMapper;
            this.playerService = playerService;
            this.gameRepository = gameRepository;
        }

        public GameType SupportedGame => GameType.KingOfTokyo;

        public async Task CreateGameInstance(Guid gameId, Player creator)
        {
            var game = new KingOfTokyoGame(gameId, new KingOfTokyoPlayer
            {
                Id = creator.Id,
                GameId = creator.GameId,
                Name = creator.Name
            });

            await gameRepository.Create(game);
        }

        public async Task RemoveGameInstance(Guid gameId)
        {
            await gameRepository.GetById(gameId).MapAsync(async (game) =>
            {
                await gameRepository.Delete(gameId);
                return unit;
            });
        }

        public EitherAsync<string, IList<GameEvent>> Execute(Guid gameId, Player player, GameAction action)
            => from gameAction in actionMapper.MapAction(action, player).ToAsync()
               from gameResult in ExecuteAction(gameId, gameAction)
               select gameResult;

        private EitherAsync<string, IList<GameEvent>> ExecuteAction(Guid gameId, IAction<KingOfTokyoGame> action)
        {
            var result = from kingOfTokyo in GetGame(gameId)
                         from gameResult in executor.ExecuteAction(kingOfTokyo, action).ToAsync()
                         from _ in UpdateGame(gameResult)
                         select gameResult;

            return result.Map(r => r.Events.Select(e => eventMapper.MapEvent(gameId, e)).ToList() as IList<GameEvent>);
        }

        public EitherAsync<string, IGameView> GetGameState(Guid gameId, Player accessingPlayer, int? lockState)
            => from kingOfTokyo in GetGame(gameId)
               from _ in ValidateLockState(kingOfTokyo, lockState)
               select (IGameView)gameStateMapper.MapToView(kingOfTokyo, accessingPlayer.Id);

        public EitherAsync<string, IGameView> SetGameState(Guid gameId, Player accessingPlayer, JsonElement state)
        {
            var isAction = KingOfTokyoDebugHelper.IsAction(state);
            if (isAction)
            {
                var player = new KingOfTokyoPlayer
                {
                    Id = accessingPlayer.Id,
                    GameId = accessingPlayer.GameId,
                    Name = accessingPlayer.Name,
                };
                return from action in KingOfTokyoDebugHelper.CreateAction(state, player)
                       from events in ExecuteAction(gameId, action)
                       from kingOfTokyo in GetGame(gameId)
                       select (IGameView)gameStateMapper.MapToView(kingOfTokyo, accessingPlayer.Id);
            }
            else
            {
                return from kingOfTokyo in GetGame(gameId)
                       from updated in KingOfTokyoDebugHelper.SetState(kingOfTokyo, state, GetPlayerId(gameId))
                       from _ in EitherAsync<string, KingOfTokyoGame>.RightAsync(gameRepository.Update(updated))
                       select (IGameView)gameStateMapper.MapToView(updated, accessingPlayer.Id);
            }

        }

        public Task<bool> HasJoinedGame(Guid gameId, Player player)
            => (from kingOfTokyo in GetGame(gameId)
                select kingOfTokyo.Players.Any(p => p.Id == player.Id)).Match(found => found, _ => false);

        public EitherAsync<string, IList<GameEvent>> Join(Guid gameId, Player player)
        {
            var gamePlayer = new KingOfTokyoPlayer
            {
                Id = player.Id,
                GameId = player.GameId,
                Name = player.Name,
            };
            var action = new JoinAction(gamePlayer);

            var result = from kingOfTokyo in GetGame(gameId)
                         from gameResult in executor.ExecuteAction(kingOfTokyo, action).ToAsync()
                         from update in UpdateGame(gameResult)
                         select gameResult;

            return result.Map(r => r.Events.Select(e => eventMapper.MapEvent(gameId, e)).ToList() as IList<GameEvent>);
        }

        public EitherAsync<string, int> Reconnect(Guid gameId, Player player)
            => from kingOfTokyo in GetGame(gameId)
               from _ in ValidateReconnect(kingOfTokyo, player)
               select kingOfTokyo.LockState;

        private Func<Guid, Guid> GetPlayerId(Guid gameId)
            => (Guid userId) => playerService.GetPlayerForGame(userId, gameId).Result.Id;

        private async Task<Either<string, KingOfTokyoGame>> GetGameValue(Guid gameId)
        {
            var game = Optional<KingOfTokyoGame>((await gameRepository.GetById(gameId))!);
            return game.ToEither("Game not found");
        }

        private EitherAsync<string, KingOfTokyoGame> GetGame(Guid gameId)
            => GetGameValue(gameId).ToAsync();

        private static EitherAsync<string, Unit> ValidateLockState(KingOfTokyoGame game, int? lockState)
        {
            if (!lockState.HasValue || game.LockState == lockState.Value)
            {
                return unit;
            }

            return "Game has changed state.";
        }

        private static EitherAsync<string, Unit> ValidateReconnect(KingOfTokyoGame game, Player player)
        {
            if (game.Players.All(p => p.Id != player.Id))
            {
                return "You are not a player in this game.";
            }

            return unit;
        }

        private EitherAsync<string, Unit> UpdateGame(KingOfTokyoGame game, int originalLockState)
        {
            if (game.LockState == originalLockState)
            {
                return unit;
            }


            return rightAsync<KingOfTokyoGame, string>(gameRepository.Update(game))
                .Map(ignore);
        }

        private EitherAsync<string, Unit> UpdateGame(ActionHandleResult<KingOfTokyoGame> gameResult)
        {
            var game = gameResult.GameState;
            if (game.IsUpdated)
            {
                game.IsUpdated = false;
                return EitherAsync<string, KingOfTokyoGame>
                    .RightAsync(gameRepository.Update(game))
                    .Map(ignore);
            }

            return EitherAsync<string, Unit>.Right(unit);
        }
    }
}