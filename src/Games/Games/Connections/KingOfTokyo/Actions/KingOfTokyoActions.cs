namespace Games.Connections.KingOfTokyo.Actions
{
    public enum KingOfTokyoActions
    {
        StartGame,
        StartTurn,
        ChangeAvatar,
        RollDice,
        AcceptDice,
        BuyCard,
        FinishCardBuying,
        LeaveTokyo,
        DiscardCards
    }
}