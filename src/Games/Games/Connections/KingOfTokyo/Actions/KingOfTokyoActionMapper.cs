using Games.Hubs.Models;
using Games.Interfaces;
using Games.KingOfTokyo;
using Games.KingOfTokyo.Actions;
using Games.Players.Models;
using Games.Shared;
using LanguageExt;
using static LanguageExt.Prelude;
using static Games.Prelude;
using System.Text.Json;
using static Games.Shared.Parsing;
using Games.KingOfTokyo.State.Models;
using Games.KingOfTokyo.State.Player;

namespace Games.Connections.KingOfTokyo.Actions
{
    public class KingOfTokyoActionMapper : IActionMapper<KingOfTokyoGame>
    {
        public Either<string, IAction<KingOfTokyoGame>> MapAction(GameAction action, Player player)
        {
            var gamePlayer = MapPlayer(player);
            return parseEnumIgnoreCase<KingOfTokyoActions>(action.ActionType)
                .ToEither("Invalid action type")
                .Bind<IAction<KingOfTokyoGame>>(actionType =>
                    actionType switch
                    {
                        KingOfTokyoActions.StartGame => StartGame(gamePlayer),
                        KingOfTokyoActions.ChangeAvatar => ChangeAvatar(gamePlayer, action),
                        KingOfTokyoActions.StartTurn => StartTurn(gamePlayer),
                        KingOfTokyoActions.RollDice => RollDice(gamePlayer, action),
                        KingOfTokyoActions.AcceptDice => AcceptDice(gamePlayer),
                        KingOfTokyoActions.BuyCard => BuyCard(gamePlayer, action),
                        KingOfTokyoActions.FinishCardBuying => FinishBuying(gamePlayer),
                        KingOfTokyoActions.LeaveTokyo => LeaveTokyo(gamePlayer, action),
                        KingOfTokyoActions.DiscardCards => DiscardCards(gamePlayer),
                        _ => "Invalid action type"
                    });
        }

        private static IPlayer<KingOfTokyoGame> MapPlayer(Player player)
            => new KingOfTokyoPlayer
            {
                Id = player.Id,
                GameId = player.GameId,
                Name = player.Name,
            };

        private static StartGameAction StartGame(IPlayer<KingOfTokyoGame> player)
            => new StartGameAction(player);

        private static StartTurnAction StartTurn(IPlayer<KingOfTokyoGame> player)
                    => new StartTurnAction(player);

        private static AcceptDiceAction AcceptDice(IPlayer<KingOfTokyoGame> player)
            => new AcceptDiceAction(player);

        private static FinishCardBuying FinishBuying(IPlayer<KingOfTokyoGame> player)
            => new FinishCardBuying(player);

        private static DiscardCardsAction DiscardCards(IPlayer<KingOfTokyoGame> player)
            => new DiscardCardsAction(player);

        private static Either<string, IAction<KingOfTokyoGame>> LeaveTokyo(IPlayer<KingOfTokyoGame> player, GameAction action)
        {
            var leaveTokyoProp = toCamelCase(nameof(LeaveTokyoAction.LeaveTokyo));
            var leaveTokyoElement = action.Parameters?[leaveTokyoProp];
            if (!(leaveTokyoElement is JsonElement element) ||
                (element.ValueKind != JsonValueKind.True && element.ValueKind != JsonValueKind.False))
            {
                return "Must choose whether to leave Tokyo.";
            }

            var leaveTokyo = element.GetBoolean();

            return new LeaveTokyoAction(player, leaveTokyo);
        }

        private static Either<string, IAction<KingOfTokyoGame>> BuyCard(IPlayer<KingOfTokyoGame> player, GameAction action)
        {
            var cardTypeProp = toCamelCase(nameof(BuyCardAction.CardType));
            var cardTypeElement = action.Parameters?[cardTypeProp];
            if (!(cardTypeElement is JsonElement element) || element.ValueKind != JsonValueKind.String)
            {
                return "Must provide card type.";
            }

            var cardType = element.GetString();

            return parseEnumIgnoreCase<CardType>(cardType)
                .Map<IAction<KingOfTokyoGame>>(cardType => new BuyCardAction(player, cardType))
                .ToEither("Must provide a valid card type.");
        }

        private static Either<string, IAction<KingOfTokyoGame>> ChangeAvatar(IPlayer<KingOfTokyoGame> player, GameAction action)
        {
            var avatarProp = toCamelCase(nameof(ChangeAvatarAction.Avatar));
            var avatarTypeElement = action.Parameters?[avatarProp];
            if (avatarTypeElement == null)
            {
                return new ChangeAvatarAction(player, null);
            }

            if (!(avatarTypeElement is JsonElement element) || element.ValueKind != JsonValueKind.String)
            {
                return "Must provide avatar type.";
            }

            var avatarType = element.GetString();

            return parseEnumIgnoreCase<AvatarType>(avatarType)
                .Map<IAction<KingOfTokyoGame>>(avatar => new ChangeAvatarAction(player, avatar))
                .ToEither("Must provide a valid avatar type.");
        }

        private static Either<string, IAction<KingOfTokyoGame>> RollDice(IPlayer<KingOfTokyoGame> player, GameAction action)
        {
            var diceToKeepKey = toCamelCase(nameof(RollDiceAction.DiceToKeep));
            var diceToKeep =
            (action.Parameters?.ContainsKey(diceToKeepKey) ?? false) && action.Parameters[diceToKeepKey] is JsonElement jsonElement ?
                ToBoolArray(jsonElement, "Dice to keep")
                : (Either<string, bool[]>)new bool[8];

            return diceToKeep.Map<IAction<KingOfTokyoGame>>(toKeep => new RollDiceAction(player, toKeep));
        }
    }
}