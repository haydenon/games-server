using System;
using Games.Shared;

namespace Games.Games.Models
{
    public class Game : IUserModel
    {
        public Guid Id { get; set; }

        public Guid OwnerId { get; set; }

        public GameType Type { get; set; }

        public string? Lookup { get; set; }

        public DateTime LastUpdated { get; set; }

        public bool? HasJoined { get; set; }
    }
}