namespace Games.Views
{
    public class GameView
    {
        public GameView(string id, string gameType, string? lookup, bool? hasJoined)
        {
            Id = id;
            GameType = gameType;
            Lookup = lookup;
            HasJoined = hasJoined;
        }

        public string Id { get; }
        public string GameType { get; }
        public string? Lookup { get; }
        public bool? HasJoined { get; }
    }
}