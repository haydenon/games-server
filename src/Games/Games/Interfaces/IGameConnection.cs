using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using Games.Hubs.Models;
using Games.Players.Models;
using Games.Shared;
using LanguageExt;

namespace Games.Interfaces
{
    public interface IGameConnection
    {
        GameType SupportedGame { get; }

        EitherAsync<string, IList<GameEvent>> Execute(Guid gameId, Player player, GameAction action);

        EitherAsync<string, IList<GameEvent>> Join(Guid gameId, Player player);

        EitherAsync<string, int> Reconnect(Guid gameId, Player player);

        Task CreateGameInstance(Guid gameId, Player creator);

        Task RemoveGameInstance(Guid gameId);

        Task<bool> HasJoinedGame(Guid gameId, Player player);

        EitherAsync<string, IGameView> GetGameState(Guid gameId, Player accessingPlayer, int? lockState);

        EitherAsync<string, IGameView> SetGameState(Guid gameId, Player accessingPlayer, JsonElement state);
    }
}