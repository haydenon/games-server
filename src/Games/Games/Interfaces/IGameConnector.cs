using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using Games.Games.Models;
using Games.Hubs.Models;
using Games.Players.Models;
using Games.Shared;
using LanguageExt;

namespace Games.Interfaces
{
    public interface IGameConnector
    {
        EitherAsync<string, IList<GameEvent>> JoinGame(Game game, Player player);

        EitherAsync<string, int> Reconnect(Game game, Player player);

        EitherAsync<string, IList<GameEvent>> ExecuteAction(Game game, Player player, GameAction action);

        Task CreateGameInstance(Game game, Player creator);

        Task<bool> HasJoinedGame(Game game, Player player);

        EitherAsync<string, IGameView> GetGameState(Game game, Player accessingPlayer, int? lockState);

        EitherAsync<string, IGameView> SetGameState(Game game, Player accessingPlayer, JsonElement state);

        Task RemoveGameInstance(Game game);
    }
}