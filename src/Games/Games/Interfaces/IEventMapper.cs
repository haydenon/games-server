using System;
using Games.Hubs.Models;
using Games.Shared;

namespace Games.Interfaces
{
    public interface IEventMapper<T> where T : IGame
    {
        GameEvent MapEvent(Guid gameId, IGameEvent<T> gameEvent);
    }
}