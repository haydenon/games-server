using System.Collections.Generic;
using System.Threading.Tasks;
using Games.Hubs.Models;

namespace Games.Interfaces
{
    public interface IGameClient
    {
        Task ReceiveEvents(IList<GameEvent> events);

        Task ReceiveActionErrorResponse(string message);
    }
}