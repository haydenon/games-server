using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using Games.Errors;
using Games.Games.Models;
using Games.Shared;
using LanguageExt;

namespace Games.Interfaces
{
    public interface IGameService
    {
        Task<IEnumerable<Game>> GetGames(Guid userId);

        EitherAsync<AppError, Game> GetGameForUser(Guid userId, Guid gameId);

        EitherAsync<AppError, Game> GetGame(Guid userId);

        EitherAsync<AppError, Game> GetGameAndMarkUpdated(Guid gameId);

        EitherAsync<AppError, Game> GetGameForLookup(string lookup, Guid userId);

        EitherAsync<AppError, IGameView> GetGameState(Guid gameId, Guid userId, int? stateLock);

        EitherAsync<AppError, IGameView> SetGameState(Guid gameId, Guid userId, JsonElement state);

        Task<Game> CreateGame(GameType type, Guid userId);

        Task<Unit> DeleteGame(Game game);

        Task<Unit> CleanExpiredGames();
    }
}