using System;
using Games.Shared;

namespace Games.Interfaces
{
    public interface IGameStateMapper<T, TView> where T : IGame
    {
        TView MapToView(T game, Guid accessingPlayer);
    }
}