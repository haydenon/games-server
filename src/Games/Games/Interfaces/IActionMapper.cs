using Games.Hubs.Models;
using Games.Players.Models;
using Games.Shared;
using LanguageExt;

namespace Games.Interfaces
{
    public interface IActionMapper<T> where T : IGame
    {
        Either<string, IAction<T>> MapAction(GameAction action, Player player);
    }
}