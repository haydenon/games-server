using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Games.Games.Models;
using Games.Hubs.Models;
using Games.Interfaces;
using Games.Players.Models;
using Games.Shared;
using LanguageExt;

namespace Games
{
    public class GameConnector : IGameConnector
    {
        private readonly Dictionary<GameType, IGameConnection> gameConnections;

        public GameConnector(ICollection<IGameConnection> gameConnections)
        {
            this.gameConnections = gameConnections.ToDictionary(c => c.SupportedGame);
        }

        public EitherAsync<string, IList<GameEvent>> ExecuteAction(Game game, Player player, GameAction action)
            => gameConnections[game.Type].Execute(game.Id, player, action);

        public EitherAsync<string, IList<GameEvent>> JoinGame(Game game, Player player)
            => gameConnections[game.Type].Join(game.Id, player);

        public Task CreateGameInstance(Game game, Player creator)
            => gameConnections[game.Type].CreateGameInstance(game.Id, creator);

        public Task RemoveGameInstance(Game game)
            => gameConnections[game.Type].RemoveGameInstance(game.Id);

        public Task<bool> HasJoinedGame(Game game, Player player)
            => gameConnections[game.Type].HasJoinedGame(game.Id, player);

        public EitherAsync<string, IGameView> GetGameState(Game game, Player accessingPlayer, int? lockState)
            => gameConnections[game.Type].GetGameState(game.Id, accessingPlayer, lockState);

        public EitherAsync<string, IGameView> SetGameState(Game game, Player accessingPlayer, JsonElement state)
            => gameConnections[game.Type].SetGameState(game.Id, accessingPlayer, state);

        public EitherAsync<string, int> Reconnect(Game game, Player player)
            => gameConnections[game.Type].Reconnect(game.Id, player);
    }
}