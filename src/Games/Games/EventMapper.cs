using System;
using System.Collections.Generic;
using System.Linq;
using Games.Hubs.Models;
using Games.Interfaces;
using Games.Shared;

using static Games.Prelude;

namespace Games
{
    public class EventMapper<T> : IEventMapper<T> where T : IGame
    {
        private static readonly string[] IgnoredNames =
        {
            nameof(IGameEvent<T>.EventType),
            nameof(IGameEvent<T>.LockState)
        };

        public GameEvent MapEvent(Guid gameId, IGameEvent<T> gameEvent)
        {
            return new GameEvent
            {
                GameId = gameId,
                LockState = gameEvent.LockState,
                EventType = gameEvent.EventType,
                Data = GetData(gameEvent)
            };
        }

        private static Dictionary<string, object?> GetData(IGameEvent<T> gameEvent)
        {
            var properties = gameEvent.GetType()
                .GetProperties()
                .Where(p => IgnoredNames.All(n => n != p.Name) && p.CanRead);

            return properties.ToDictionary(p => toCamelCase(p.Name), p => p.GetValue(gameEvent));
        }

    }
}