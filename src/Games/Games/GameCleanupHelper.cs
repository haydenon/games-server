using System;
using System.Timers;
using Games.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Games
{
    public class GameCleanupHelper
    {
        private const int Frequency = 1000 * 60 * 10; // 10 minutes
        private static IServiceProvider? services;
        private static Timer? timer;

        public static void SetupCleaning(IServiceProvider services)
        {
            GameCleanupHelper.services = services;

            timer = new Timer(Frequency);
            timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            timer.Start();

            try
            {
                CleanGames();
            }
            catch (Exception err)
            {
                var logger = services.GetRequiredService<ILogger<GameCleanupHelper>>();
                logger.LogError(1, err, err.Message);
            }
        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e)
            => CleanGames();

        private static void CleanGames()
        {

            if (services != null)
            {
                var gameService = services.GetRequiredService<IGameService>();
                var result = gameService.CleanExpiredGames().Result;
            }
        }
    }
}