const connection = new signalR.HubConnectionBuilder()
  .withUrl("http://localhost:5000/game/hub")
  .withAutomaticReconnect()
  .build();
connection.start().catch((err) => console.error(err));
connection.on("ReceiveEvents", (events) => {
  if (events.length === 0 && events[0].eventType === "Accepted") {
    console.log("Accepted");
  } else if (events.length > 0) {
    console.p;
    const lockState = Math.max.apply(
      null,
      events.map((e) => e.lockState)
    );
    const gameId = events[0].gameId;
    localStorage["game-state"] = JSON.stringify({ gameId, lockState });
    refreshState(gameId, lockState);
  }
});
connection.on("ReceiveActionErrorResponse", (err) => {
  console.error(err);
  $("#action-error").text(err);
});

const reconnect = () => connection.send("Reconnect", gameId);

let currentUser = undefined;
let gameId = undefined;

const defaultUserList = [
  { name: "Ann", id: "9e92fe67-0430-4518-9bc6-9bbb14023993" },
  { name: "Bob", id: "10f37b88-bfcd-4c89-8507-0de84d5e446d" },
  { name: "Carl", id: "6a74b96b-dac9-4802-b7d0-2235f8ee61b3" },
  { name: "Dan", id: "0eb9fac7-1c30-4d62-afd3-6da6772c7dd3" },
  { name: "Emma", id: "ea0da123-0c65-45e7-abd7-ee0fa3130924" },
];

const logIn = fetch("/auth/signin", { method: "POST" });
const impersonate = (id) =>
  fetch("/auth/impersonate", { method: "DELETE" }).then(() =>
    fetch("/auth/impersonate", {
      method: "POST",
      body: `"${id}"`,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    })
  );
const removeImpersonation = () =>
  fetch("/auth/impersonate", { method: "DELETE" })
    .then(() => connection.stop())
    .then(() => connection.start())
    .then(reconnect)
    .then(() => {
      currentUser = undefined;
    });

const gameStateString = localStorage["game-state"];
const gameState = gameStateString ? JSON.parse(gameStateString) : null;
if (gameState) {
  fetch(`/game/${gameState.gameId}/state?lockState${gameState.lockState}`)
    .then((state) => {
      if (state.status < 300) {
        return state.json();
      } else {
        throw new Error("Game does not exist");
      }
    })
    .then((state) => {
      $("#game-state").text(JSON.stringify(state, null, 2));
      gameId = gameState.gameId;
    })
    .catch(() => localStorage.removeItem("game-state"));
}

function refreshState(gameId, lockState) {
  fetch(`/game/${gameId}/state?lockState=${lockState}`)
    .then((state) => state.json())
    .then((state) => $("#game-state").text(JSON.stringify(state, null, 2)));
}

const userListString = localStorage["user-list"];
const userList = userListString ? JSON.parse(userListString) : defaultUserList;
userList.forEach(addUser);

function addUser(user) {
  const impersonateButton = `<button id="impersonate-${user.id}" type="button">Impersonate</button>`;
  const removeButton = `<button id="remove-${user.id}" type="button">Remove</button>`;
  const row = `<li data-id="${user.id}">${user.name} - ${user.id} ${impersonateButton}${removeButton}</li>`;
  $("#impersonate-list").append(row);
  $(`#impersonate-${user.id}`).click(() => {
    impersonate(user.id)
      .then(() => connection.stop())
      .then(() => connection.start())
      .then(reconnect)
      .then(() => {
        currentUser = user.name;
        $("#impersonated-user").text(`Impersonated: ${user.id}`);
        $("#remove-impersonation").removeClass("impersonation__remove--hidden");
      });
  });
  $(`#remove-${user.id}`).click(() => {
    $(`li[data-id="${user.id}"]`).remove();
    const userListString = localStorage["user-list"];
    let userList = userListString ? JSON.parse(userListString) : [];
    userList = userList.filter((u) => u.id !== user.id);
    localStorage["user-list"] = JSON.stringify(userList);
  });
}

const addGameRow = (element, game) => {
  const row = `<li data-id="${game.id}">${game.gameType} <button id="join-game-${game.id}" type="button">Join game</button></li>`;
  $(element).append(row);
  $(`#join-game-${game.id}`).click((event) => {
    gameId = event.target.parentElement.attributes["data-id"].value;
    $("#action-error").text("");
    connection
      .send("JoinGame", gameId, currentUser || "User")
      .catch(console.error);
  });
};

logIn
  .then(() => removeImpersonation())
  .then(() => {
    fetch("auth")
      .then((user) => user.json())
      .then((user) => $("#current-user").text(`Logged in: ${user}`));
    fetch("/game")
      .then((games) => games.json())
      .then((games) => games.forEach((game) => addGameRow("#game-list", game)));
  });

$("#create-game").click(() => {
  fetch("/game", {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    method: "POST",
    body: JSON.stringify({ gameType: "KingOfTokyo" }),
  })
    .then((game) => game.json())
    .then((game) => addGameRow("#game-list", game));
});

$("#join-game-code").on("change", (event) => {
  const code = $(event.target).val();
  fetch(`/game/lookup/${code}`)
    .then((game) => game.json())
    .then((game) => {
      if (game.id) {
        localStorage["game-state"] = JSON.stringify({
          gameId: game.id,
          lockState: game.lockState,
        });
        refreshState(game.id, game.lockState);
      }
    })
    .catch();
});

$("#join-game-by-code").click(() => {
  const code = $("#join-game-code").val();
  fetch(`/game/lookup/${code}`)
    .then((game) => game.json())
    .then((game) => {
      if (game.id) {
        gameId = game.id;
        $("#action-error").text("");
        connection
          .send("JoinGame", gameId, currentUser || "User")
          .catch(console.error);
      }
    })
    .catch();
});

$("#impersonate-user").click(() => {
  const userListString = localStorage["user-list"];
  const userList = userListString
    ? JSON.parse(userListString)
    : defaultUserList;
  const id = $("#impersonate-id").val();
  const name = $("#impersonate-name").val();
  const user = { id, name };
  addUser(user);
  userList.push(user);
  localStorage["user-list"] = JSON.stringify(userList);
});

$("#remove-impersonation").click(() => {
  removeImpersonation().then(() => {
    $("#impersonated-user").text("Impersonated:");
    $("#remove-impersonation").addClass("impersonation__remove--hidden");
  });
});

const actions = [
  { name: "StartGame", parameters: [] },
  {
    name: "ChangeAvatar",
    parameters: [{ name: "avatar", default: "MekaDragon" }],
    buildParameters: ({ avatar }) => ({
      avatar: !avatar.trim() ? null : avatar,
    }),
  },
  { name: "StartTurn", parameters: [] },
  {
    name: "RollDice",
    parameters: [
      { name: "diceToKeep", default: "false,false,false,false,false,false" },
    ],
    buildParameters: ({ diceToKeep }) => {
      diceToKeep = diceToKeep.trim();
      if (!diceToKeep) {
        return { diceToKeep: null };
      }
      return {
        diceToKeep: diceToKeep
          .trim()
          .split(",")
          .map((item) => item === "true"),
      };
    },
  },
  { name: "AcceptDice", parameters: [] },
  {
    name: "BuyCard",
    parameters: [{ name: "cardType", default: "CommuterTrain" }],
    buildParameters: ({ cardType }) => ({ cardType }),
  },
  { name: "DiscardCards", parameters: [] },
  { name: "FinishCardBuying", parameters: [] },
  {
    name: "LeaveTokyo",
    parameters: [{ name: "leaveTokyo", default: "true" }],
    buildParameters: ({ leaveTokyo }) => ({
      leaveTokyo: leaveTokyo === "true",
    }),
  },
];

$("#execute-action").click(() => {
  const action = $("#game-action-select").val();
  const actionDef = actions.find((a) => a.name == action);
  const parameters = actionDef.buildParameters
    ? actionDef.buildParameters(
        actionDef.parameters.reduce((params, p) => {
          params[p.name] = $(`#param-${p.name}`).val();
          return params;
        }, {})
      )
    : {};

  connection
    .send("ExecuteAction", {
      GameId: gameId,
      ActionType: action,
      Parameters: parameters,
    })
    .catch(console.error);
});

$("#game-action-select").val("StartGame");

$("#game-action-select").on("change", (event) => {
  $("#action-parameters").empty();
  const parameters = actions.find((a) => a.name == $(event.target).val())
    .parameters;
  parameters.forEach((p) => {
    const param = `<div><label for="param-${p.name}">${p.name}</label></div><div><input id="param-${p.name}" type="text" value="${p.default}"/></div>`;
    $("#action-parameters").append(param);
  });
});

$("#set-state").click(() => {
  const state = JSON.parse($("#set-state-contents").val());
  fetch(`/game/${gameId}/state`, {
    method: "POST",
    body: JSON.stringify(state),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  })
    .then((state) => {
      if (state.status < 300) {
        return state.json();
      } else {
        return state.json().then((err) => {
          throw new Error(err.error);
        });
      }
    })
    .then((state) => {
      localStorage["game-state"] = JSON.stringify({
        gameId,
        lockState: state.lockState,
      });
      $("#game-state").text(JSON.stringify(state, null, 2));
    })
    .catch((err) => $("#set-state-error").text(err.message));
});
