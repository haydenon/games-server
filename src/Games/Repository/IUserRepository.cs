using System;
using System.Collections.Generic;
using Games.Data.Entities;

namespace Games.Repository
{
    public interface IUserRepository<T> : IRepository<T>
    {
        IAsyncEnumerable<T> GetAllForUser(Guid userId);
    }
}