using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LanguageExt;

namespace Games.Repository
{
    public interface IRepository<T>
    {
        IAsyncEnumerable<T> GetAll();

        Task<T?> GetById(Guid id);

        Task<T> Create(T game);

        Task<T> Update(T game);

        Task<Unit> Delete(Guid id);
    }
}