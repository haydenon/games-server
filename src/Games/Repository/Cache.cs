using System;
using System.Collections.Concurrent;

namespace Games.Repository
{
    public class Cache<T>
    {
        private readonly ConcurrentDictionary<Guid, T> items = new ConcurrentDictionary<Guid, T>();

        public T? GetItem(Guid id)
            => items.TryGetValue(id, out var item) ? item : default;

        public T SetItem(Guid id, T item)
            => items.AddOrUpdate(id, item, (_, _) => item);

        public void ClearItem(Guid id)
        {
            items.TryRemove(id, out T? retrievedValue);
        }
    }
}