using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Games.Data.Entities;
using Games.Data.Interfaces;

using static LanguageExt.Prelude;

namespace Games.Repository
{
    public class Repository<T, TData> : IRepository<T>
        where TData : IEntity
    {
        protected readonly IRecordStore<TData> store;
        protected readonly IMapper mapper;
        protected readonly Cache<T> cache;

        public Repository(
            IStoreFactory storeFactory,
            IMapper mapper,
            Cache<T> cache)
        {
            this.store = storeFactory.GetRecordStore<TData>();
            this.mapper = mapper;
            this.cache = cache;
        }


        public IAsyncEnumerable<T> GetAll()
            => store.GetAll().Select(item => mapper.Map<T>(item));

        public async Task<T?> GetById(Guid id)
        {
            var value = cache.GetItem(id);
            if (value != null)
            {
                return value;
            }

            var stored = await store.GetById(id);
            if (stored == null)
            {
                return default(T?);
            }

            var mapped = mapper.Map<T>(stored);
            cache.SetItem(id, mapped);
            return mapped;
        }

        public async Task<T> Create(T item)
        {
            var data = mapper.Map<TData>(item);
            var created = await store.Create(data);
            var mappedCreated = mapper.Map<T>(created);
            cache.SetItem(Guid.Parse(created.Id!), mappedCreated);
            return mappedCreated;
        }

        public async Task<T> Update(T item)
        {
            var data = mapper.Map<TData>(item);
            var updated = await store.Update(data);
            var mappedUpdated = mapper.Map<T>(updated);
            cache.SetItem(Guid.Parse(updated.Id!), mappedUpdated);
            return mappedUpdated;
        }

        public async Task<LanguageExt.Unit> Delete(Guid id)
        {
            await store.Delete(id);
            cache.ClearItem(id);
            return unit;
        }
    }
}