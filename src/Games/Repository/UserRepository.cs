using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Games.Data.Entities;
using Games.Data.Interfaces;

namespace Games.Repository
{
    public class UserRepository<T, TData> : Repository<T, TData>, IUserRepository<T>
        where TData : IUserEntity
    {
        private readonly IUserRecordStore<TData> userStore;
        public UserRepository(
            IStoreFactory storeFactory,
            IMapper mapper,
            Cache<T> cache) : base(storeFactory, mapper, cache)
        {
            userStore = storeFactory.GetUserRecordStore<TData>();
        }

        public IAsyncEnumerable<T> GetAllForUser(Guid userId)
            => userStore.GetAll(userId)
            .Select(game => mapper.Map<T>(game));
    }
}