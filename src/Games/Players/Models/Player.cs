using System;
using Games.Shared;

namespace Games.Players.Models
{
    public class Player : IUserModel
    {
        public Guid Id { get; set; }

        public Guid OwnerId { get; set; }

        public Guid GameId { get; set; }

        public string? Name { get; set; }
    }
}