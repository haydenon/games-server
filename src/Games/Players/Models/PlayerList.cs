using System;
using System.Collections.Generic;
using Games.Shared;

namespace Games.Players.Models
{
    public class PlayerList : IModel
    {
        public Guid Id { get; set; }

        public List<Guid>? Players { get; set; }
    }
}