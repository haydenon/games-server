using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Games.Data.Entities;
using Games.Players.Interfaces;
using Games.Players.Models;
using LanguageExt;
using static LanguageExt.Prelude;
using Games.Repository;

namespace Games.Players
{
    public class PlayerService : IPlayerService
    {
        private readonly IUserRepository<Player> playerRepository;
        private readonly IRepository<PlayerList> playerListRepository;

        public PlayerService(
            IUserRepository<Player> playerRepository,
            IRepository<PlayerList> playerListRepository)
        {
            this.playerRepository = playerRepository;
            this.playerListRepository = playerListRepository;
        }

        public Task<Player> GetPlayerForGame(Guid userId, Guid gameId)
            => GetPlayer(userId, gameId);

        public EitherAsync<string, Player> GetPlayerForGameWithName(Guid userId, Guid gameId, string name)
        {
            name = name.Trim();
            if (string.IsNullOrWhiteSpace(name))
            {
                return "Invalid player name";
            }

            return EitherAsync<string, Player>.RightAsync(GetPlayer(userId, gameId, name));
        }

        public async Task RemoveForGame(Guid gameId)
        {
            await playerListRepository.GetById(gameId).MapAsync(async (playerList) =>
            {
                foreach (var player in playerList?.Players ?? new List<Guid>())
                {
                    await RemovePlayer(player);
                }

                if (playerList != null)
                {
                    await playerListRepository.Delete(playerList.Id);
                }

                return unit;
            });
        }

        private async Task RemovePlayer(Guid playerId)
        {
            await playerRepository.GetById(playerId).MapAsync(async (player) =>
            {
                if (player != null)
                {
                    await playerRepository.Delete(player.Id);
                }
                return unit;
            });
        }

        private async Task<Player> GetPlayer(Guid userId, Guid gameId, string? name = null)
        {
            var players = await playerRepository.GetAllForUser(userId).ToListAsync();

            var player = players.FirstOrDefault(p => p.OwnerId == userId && p.GameId == gameId);
            if (player == null)
            {
                var playerList = await playerListRepository.GetById(gameId);
                if (playerList == null)
                {
                    var list = new PlayerList { Players = new List<Guid>(), Id = gameId };
                    await playerListRepository.Create(list);
                    playerList = list;
                }

                player = new Player
                {
                    Name = name,
                    OwnerId = userId,
                    GameId = gameId,
                };

                player = await playerRepository.Create(player);

                if (playerList?.Players != null)
                {
                    playerList.Players.Add(player.Id);
                    await playerListRepository.Update(playerList);
                }
            }
            else if (name != null && player.Name == null)
            {
                player.Name = name;
                await playerRepository.Update(player);
            }

            return player;
        }
    }
}