using System;
using System.Threading.Tasks;
using Games.Players.Models;
using LanguageExt;

namespace Games.Players.Interfaces
{
    public interface IPlayerService
    {
        Task<Player> GetPlayerForGame(Guid userId, Guid gameId);

        EitherAsync<string, Player> GetPlayerForGameWithName(Guid userId, Guid gameId, string name);

        Task RemoveForGame(Guid gameId);
    }
}