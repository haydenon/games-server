﻿using Games.Interfaces;
using Games.Games.Models;
using Games.Mapping;
using Games.Views;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;
using Games.Auth;

using static Games.Controllers.Prelude;
using static LanguageExt.Prelude;
using System.Text.Json;
using Games.Errors;
using System.Net;

namespace Games.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GameController : ControllerBase
    {
        private readonly ILogger<GameController> logger;
        private readonly IGameService gameService;
        private readonly IAuthService authService;
        private readonly IViewMapper<Game, GameView> gameMapper;

        public GameController(
            ILogger<GameController> logger,
            IGameService gameService,
            IAuthService authService,
            IViewMapper<Game, GameView> gameMapper)
        {
            this.logger = logger;
            this.gameService = gameService;
            this.gameMapper = gameMapper;
            this.authService = authService;
        }

        [HttpGet]
        public Task<IActionResult> GetGames()
        {
            var games = from userId in authService.GetUserIdForHttp(HttpContext)
                        from gamesResult in rightAsync(gameService.GetGames(userId))
                        select gamesResult;
            var view = map(games, games => games.Select(gameMapper.MapToView));
            return toResult(view);
        }

        [HttpGet]
        [Route("{id}")]
        public Task<IActionResult> GetGame(string id)
        {
            var game = from userId in authService.GetUserIdForHttp(HttpContext)
                       from gameId in parseUserId(id, "game id")
                       from gameResult in mapToApi(gameService.GetGameForUser(userId, gameId))
                       select gameResult;
            var view = map(game, gameMapper.MapToView);
            return toResult(view);
        }

        [HttpPost]
        public Task<IActionResult> CreateGame([FromBody] CreateGameView createView)
        {
            var game = from viewType in validateNotNull(createView?.GameType, "game type")
                       from gameType in parseUserEnum<GameType>(viewType, "game type")
                       from userId in authService.GetUserIdForHttp(HttpContext)
                       from createResult in rightAsync(gameService.CreateGame(gameType, userId))
                       select createResult;
            var view = map(game, gameMapper.MapToView);
            return toResult(view);
        }

        [HttpGet]
        [Route("{id}/state")]
        public Task<IActionResult> GetGameState(string id, [FromQuery] int? stateLock)
        {
            var game = from userId in authService.GetUserIdForHttp(HttpContext)
                       from gameId in parseUserId(id, "game id")
                       from gameResult in mapToApi(gameService.GetGameState(gameId, userId, stateLock))
                       select gameResult;
            return toResult(game);
        }

        [HttpPost]
        [Route("{id}/state")]
        public Task<IActionResult> SetGameState(string id, [FromBody] JsonElement state)
        {
            var view = from userId in authService.GetUserIdForHttp(HttpContext)
                       from gameId in parseUserId(id, "game id")
                       from gameResult in mapToApi(gameService.SetGameState(gameId, userId, state))
                       select gameResult;
            return toResult(view);
        }

        [HttpGet]
        [Route("lookup/{id}")]
        public Task<IActionResult> GetFromLookup(string id)
        {
            if (string.IsNullOrWhiteSpace(id) || id.Length != 4)
            {
                return toResult<object>(new ApiError(HttpStatusCode.BadRequest, "Invalid lookup id"));
            }

            var game = from userId in authService.GetUserIdForHttp(HttpContext)
                       from gameResult in mapToApi(gameService.GetGameForLookup(id, userId))
                       select gameResult;

            var view = map(game, gameMapper.MapToView);
            return toResult(view);
        }

        [HttpDelete]
        [Route("{id}")]
        public Task<IActionResult> Delete(string id)
        {
            var result = from userId in authService.GetUserIdForHttp(HttpContext)
                         from gameId in parseUserId(id, "game id")
                         from gameResult in mapToApi(gameService.GetGameForUser(userId, gameId))
                         from _ in rightAsync(gameService.DeleteGame(gameResult))
                         select unit;
            return toResult(result);
        }
    }
}
