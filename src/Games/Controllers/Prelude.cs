using System;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Games.Errors;
using LanguageExt;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static LanguageExt.Prelude;

namespace Games.Controllers
{
    public class Prelude
    {
        public static ApiError Error(HttpStatusCode code, string? error = null)
            => new ApiError(code, error);

        public static EitherAsync<ApiError, T> parseUserEnum<T>(string value, string name) where T : struct
            => parseEnumIgnoreCase<T>(value).ToEither(Error(HttpStatusCode.BadRequest, $"Invalid {name}")).ToAsync();

        public static EitherAsync<ApiError, Guid> parseUserId(string value, string name)
            => parseGuid(value).ToEither(Error(HttpStatusCode.BadRequest, $"Invalid {name}")).ToAsync();

        public static EitherAsync<ApiError, T> validateNotNull<T>(T value, string name, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            if (value == null)
            {
                return Error(statusCode, $"Must provide a {name} value");
            }

            return value;
        }

        public static EitherAsync<ApiError, T> rightAsync<T>(Task<T> item)
        {
            return EitherAsync<ApiError, T>.RightAsync(item);
        }

        public static EitherAsync<ApiError, Unit> rightAsync(Task item)
        {
            return EitherAsync<ApiError, Unit>.RightAsync(item.ToUnit());
        }

        public static Task<IActionResult> toResult<T>(EitherAsync<ApiError, T> result, HttpStatusCode? validStatusCode = null)
        {
            return result.Match(
                (item) => item?.GetType() == typeof(Unit)
                    ? (IActionResult)(new StatusCodeResult((int)(validStatusCode ?? HttpStatusCode.NoContent)))
                    : new ObjectResult(item) { StatusCode = (int)(validStatusCode ?? HttpStatusCode.OK) },
                (err) => err.ToResult());
        }

        public static EitherAsync<ApiError, T> mapToApi<T>(EitherAsync<AppError, T> item)
            => item.MapLeft(ApiError.FromAppError);
    }
}