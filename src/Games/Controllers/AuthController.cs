using System.Threading.Tasks;
using Games.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using static Games.Controllers.Prelude;

namespace Games.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [AllowAnonymous]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService authService;

        public AuthController(IAuthService authService)
        {
            this.authService = authService;
        }

        [HttpGet]
        public async Task<IActionResult> GetUser()
        {
            return await toResult(authService.GetUserIdForHttp(HttpContext));
        }

        [HttpPost]
        [Route("signin")]
        public async Task<IActionResult> SignIn()
        {
            var userId = authService.GetUserIdForHttp(HttpContext);
            if (await userId.IsRight)
            {
                return NoContent();
            }

            await authService.SignInUser(HttpContext);

            return NoContent();
        }

        [HttpPost]
        [Route("impersonate")]
        public async Task<IActionResult> Impersonate([FromBody] string user)
        {
            var impersonateResult = from userId in parseUserId(user, "user id")
                                    from actualUser in authService.GetUserIdForHttp(HttpContext)
                                    from result in mapToApi(authService.ImpersonateUser(HttpContext, actualUser, userId))
                                    select result;
            return await toResult(impersonateResult);
        }

        [HttpDelete]
        [Route("impersonate")]
        public async Task<IActionResult> Impersonate()
        {
            var impersonateResult = from actualUser in authService.GetRealUserIdForHttp(HttpContext)
                                    from result in rightAsync(authService.SignInUser(HttpContext, actualUser))
                                    select result;
            return await toResult(impersonateResult);
        }
    }
}