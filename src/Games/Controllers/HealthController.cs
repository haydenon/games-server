using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Games.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [AllowAnonymous]
    public class HealthcheckController : ControllerBase
    {
        [HttpGet]
        public IActionResult HealthCheck()
        {
            return new JsonResult(new { Status = "Healthy" });
        }
    }
}