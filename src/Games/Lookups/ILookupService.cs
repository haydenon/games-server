using System;
using System.Threading.Tasks;
using LanguageExt;

namespace Games.Lookups
{
    public interface ILookupService
    {
        Task<string> CreateLookup(Guid id);

        OptionAsync<Guid> GetIdFromLookup(string lookup);

        Task<Unit> DeleteLookup(string lookup);
    }
}