using System;
using System.Linq;
using System.Threading.Tasks;
using Games.Data.Interfaces;
using LanguageExt;
using static LanguageExt.Prelude;

namespace Games.Lookups
{
    public class LookupService : ILookupService
    {
        private static Random random = new Random();

        private readonly ILookupStore store;
        public LookupService(ILookupStore store)
        {
            this.store = store;
        }

        public async Task<string> CreateLookup(Guid id)
        {
            var existingLookups = await store.GetLookups();
            var lookup = RandomString(4);
            while (existingLookups.Contains(lookup))
            {
                lookup = RandomString(4);
            }

            await store.Create(lookup, id);
            return lookup;
        }

        public OptionAsync<Guid> GetIdFromLookup(string lookup)
            => store.GetLookupId(lookup).Bind(id => id == Guid.Empty ? None : (OptionAsync<Guid>)id);

        public Task<Unit> DeleteLookup(string lookup)
            => store.Delete(lookup);

        private static string RandomString(int length)
        {
            const string chars = "ABDEFGHJKLMNOPQRSUVWXYZ";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

    }
}