using System;
using System.Linq;
using System.Collections.Generic;
using AutoMapper;
using Games.Data.Entities;
using Games.Data.Entities.KingOfTokyo;
using Games.Games.Models;
using Games.KingOfTokyo;
using Games.KingOfTokyo.State.Player;
using Games.Mapping.Data;
using Games.Players.Models;
using Games.Shared;
using Microsoft.Extensions.DependencyInjection;
using Games.KingOfTokyo.State.Models;

namespace Games.Setup
{
    public static class MapperSetup
    {
        public static IServiceCollection AddMapper(this IServiceCollection services)
        {
            var configuration = new MapperConfiguration(cfg =>
            {

                cfg.CreateMap<Game, GameData>().ReverseMap();
                cfg.CreateMap<Player, PlayerData>().ReverseMap();
                cfg.CreateMap<PlayerList, PlayerListData>().ReverseMap();

                cfg.CreateMap<KingOfTokyoGame, KingOfTokyoGameData>()
                    .ForMember(g => g.State, opt => opt.MapFrom(game => KingOfTokyoStateDataMapper.MapToData(game.State)))
                    .ReverseMap()
                    .IgnoreAllPropertiesWithAnInaccessibleSetter()
                    .ForMember(g => g.State, opt => opt.MapFrom(game => KingOfTokyoStateDataMapper.MapFromData(game.State)))
                    .ConstructUsing((g, ctx) => new KingOfTokyoGame(
                        Guid.Parse(g.Id!),
                        Guid.Parse(g.CreatorPlayerId!),
                        ctx.Mapper.Map<List<KingOfTokyoPlayer>>(g.Players).Select(p => p as IPlayer<KingOfTokyoGame>).ToList(),
                        ctx.Mapper.Map<IDictionary<Guid, AvatarType?>>(g.Avatars)));
                cfg.CreateMap<KingOfTokyoPlayer, KingOfTokyoPlayerData>().ReverseMap();
                cfg.CreateMap<PlayerState, PlayerStateData>().ReverseMap();
                cfg.CreateMap<Card, CardData>().ReverseMap();
                cfg.CreateMap<DiceRoll, DiceRollData>().ReverseMap();

                // cfg.CreateEnumMap<CardType>();
                // cfg.CreateEnumMap<AvatarType>();
            });
            services.AddSingleton<IMapper>(configuration.CreateMapper());

            return services;
        }

        // private static void CreateEnumMap<TEnum>(this IMapperConfigurationExpression cfg)
        //     where TEnum : struct
        // {
        //     cfg.CreateMap<string, TEnum>().ConstructUsing(str => Enum.Parse<TEnum>(str));
        //     cfg.CreateMap<TEnum, string>().ConstructUsing(e => e.ToString()!);
        // }
    }
}