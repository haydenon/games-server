using System.Collections.Generic;
using Games.Connections.KingOfTokyo;
using Games.Connections.KingOfTokyo.Actions;
using Games.Data;
using Games.Data.Interfaces;
using Games.Games.Models;
using Games.Interfaces;
using Games.KingOfTokyo;
using Games.KingOfTokyo.View;
using Games.Mapping;
using Games.Players;
using Games.Players.Interfaces;
using Games.Shared;
using Games.Views;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Games.Auth;
using System.Linq;
using Games.Lookups;
using Games.Shared.StateUpdates;
using Google.Cloud.Firestore;
using AutoMapper;
using Games.Data.StoreStrategies;
using Games.Data.StoreStrategies.Firestore;
using Games.Data.StoreStrategies.FileSystem;
using Microsoft.AspNetCore.Hosting;
using Games.Repository;
using Games.Data.Entities;
using Games.Data.Entities.KingOfTokyo;
using Games.Players.Models;

namespace Games.Setup
{
    public static class ServiceSetup
    {
        public static void AddGameServices(this IServiceCollection services)
        {
            services.AddSingleton<IStoreFactory, StoreFactory>();
            services.AddSingleton<ILookupStore, LookupStore>();

            services.AddTransient<IViewMapper<Game, GameView>, GameMapper>();

            services.AddTransient<IAuthService, AuthService>();
            services.AddTransient<IGameService, GameService>();
            services.AddTransient<ILookupService, LookupService>();
            services.AddTransient<IPlayerService, PlayerService>();
            services.AddTransient<IGameConnector, GameConnector>();
            services.AddMapper();


            services.AddSingleton<Cache<Game>>();
            services.AddSingleton<Cache<KingOfTokyoGame>>();
            services.AddSingleton<Cache<Player>>();
            services.AddSingleton<Cache<PlayerList>>();
            services.AddTransient<IUserRepository<Game>, UserRepository<Game, GameData>>();
            services.AddTransient<IRepository<KingOfTokyoGame>, Repository<KingOfTokyoGame, KingOfTokyoGameData>>();
            services.AddTransient<IUserRepository<Player>, UserRepository<Player, PlayerData>>();
            services.AddTransient<IRepository<PlayerList>, Repository<PlayerList, PlayerListData>>();

            services.AddTransient<ICollection<IGameConnection>>(services =>
            {
                var gameRepository = services.GetRequiredService<IRepository<KingOfTokyoGame>>();
                var playerService = services.GetRequiredService<IPlayerService>();
                var executor = services.GetRequiredService<IGameExecutor<KingOfTokyoGame>>();
                var actionMapper = services.GetRequiredService<IActionMapper<KingOfTokyoGame>>();
                var eventMapper = services.GetRequiredService<IEventMapper<KingOfTokyoGame>>();
                var stateMapper = services.GetRequiredService<IGameStateMapper<KingOfTokyoGame, KingOfTokyoView>>();
                return new IGameConnection[]
                {
                    new KingOfTokyoConnection(gameRepository, playerService, executor, actionMapper, eventMapper, stateMapper)
                };
            });

        }

        public static void AddDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            var dbSection = configuration.GetSection("Database");
            if (dbSection.GetValue<bool>("UseFileSystem"))
            {
                services.AddSingleton<IStoreStrategy>(services =>
                {
                    var rootDirectory = services.GetRequiredService<IConfiguration>().GetValue<string>(WebHostDefaults.ContentRootKey);
                    var directory = $"{rootDirectory}/test_data";
                    return new FileSystemStrategy(directory);
                });
            }
            else
            {
                var project = configuration["ProjectId"];
                var db = FirestoreDb.Create(project);
                services.AddSingleton(db);
                services.AddSingleton<IStoreStrategy, FirestoreStrategy>();
            }
        }

        public static void AddKingOfTokyo(this IServiceCollection services)
        {
            var handlers = KingOfTokyoServices.GetAllHandlers().ToArray();
            var stateUpdateInstantiators = KingOfTokyoServices.GetAllStateUpdaterInstantiators().ToArray();
            var stateUpdaterFactory = new StateUpdater<KingOfTokyoGame>(stateUpdateInstantiators);

            services.AddTransient<IGameExecutor<KingOfTokyoGame>, KingOfTokyoExecutor>();
            services.AddTransient<IActionMapper<KingOfTokyoGame>, KingOfTokyoActionMapper>();
            services.AddTransient<IGameStateMapper<KingOfTokyoGame, KingOfTokyoView>, KingOfTokyoStateMapper>();
            services.AddTransient<IEventMapper<KingOfTokyoGame>, EventMapper<KingOfTokyoGame>>();
            services.AddSingleton<ICollection<IActionHandler<KingOfTokyoGame>>>(handlers);
            services.AddSingleton<IStateUpdater<KingOfTokyoGame>>(stateUpdaterFactory);
        }
    }
}