using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Games.Auth;
using Games.Hubs.Models;
using Games.Interfaces;
using Games.Players.Interfaces;
using LanguageExt;
using Microsoft.AspNetCore.SignalR;

using static Games.Hubs.Prelude;

namespace Games.Hubs
{
    public class GameHub : Hub<IGameClient>
    {
        private readonly IGameService gameService;
        private readonly IAuthService authService;
        private readonly IPlayerService playerService;
        private readonly IGameConnector gameConnector;

        public GameHub(
            IGameService gameService,
            IPlayerService playerService,
            IAuthService authService,
            IGameConnector gameConnector)
        {
            this.gameService = gameService;
            this.playerService = playerService;
            this.authService = authService;
            this.gameConnector = gameConnector;
        }

        public async Task JoinGame(Guid gameId, string playerName)
        {
            var result = from userId in GetUserId()
                         from game in mapError(gameService.GetGameAndMarkUpdated(gameId))
                         from playerResult in playerService.GetPlayerForGameWithName(userId, game.Id, playerName)
                         from actionResult in gameConnector.JoinGame(game, playerResult)
                         select actionResult;

            await result.Match(
                async (events) =>
                {
                    await Groups.AddToGroupAsync(Context.ConnectionId, GameGroup(gameId));
                    await SendEvents(events, gameId);
                },
                err => Clients.Caller.ReceiveActionErrorResponse(err));
        }

        public async Task Reconnect(Guid gameId)
        {
            var result = from userId in GetUserId()
                         from game in mapError(gameService.GetGameAndMarkUpdated(gameId))
                         from playerResult in rightAsync(playerService.GetPlayerForGame(userId, game.Id))
                         from actionResult in gameConnector.Reconnect(game, playerResult)
                         select actionResult;

            await result.Match(async (lockState) =>
            {
                await Groups.AddToGroupAsync(Context.ConnectionId, GameGroup(gameId));
                await Clients.Caller.ReceiveEvents(new[] { GameEvent.Reconnect(gameId, lockState) });
            }, (_) => Task.CompletedTask);
        }

        public async Task ExecuteAction(GameAction action)
        {
            var result = from userId in GetUserId()
                         from game in mapError(gameService.GetGameAndMarkUpdated(action.GameId))
                         from playerResult in rightAsync(playerService.GetPlayerForGame(userId, game.Id))
                         from actionResult in gameConnector.ExecuteAction(game, playerResult, action)
                         select actionResult;

            await result.Match(
                events => SendEvents(events, action.GameId),
                err => Clients.Caller.ReceiveActionErrorResponse(err));
        }

        private async Task SendEvents(IList<GameEvent> events, Guid gameId)
        {
            if (events.Any())
            {
                await Clients.Group(GameGroup(gameId)).ReceiveEvents(events);
            }
            else
            {
                await Clients.Caller.ReceiveEvents(new[] { GameEvent.Accepted(gameId) });
            }
        }

        private static string GameGroup(Guid gameId)
            => $"Game-{gameId}";

        private EitherAsync<string, Guid> GetUserId()
            => authService.GetUserIdForHub(Context).MapLeft(err => err.Message);
    }
}