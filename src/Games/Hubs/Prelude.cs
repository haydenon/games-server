using System.Threading.Tasks;
using Games.Errors;
using LanguageExt;

namespace Games.Hubs
{
    public static class Prelude
    {
        public static EitherAsync<string, T> rightAsync<T>(Task<T> item)
        {
            return EitherAsync<string, T>.RightAsync(item);
        }

        public static EitherAsync<string, T> mapError<T>(EitherAsync<AppError, T> item)
            => item.MapLeft(err => err.Message);
    }
}