using System;
using System.Collections.Generic;

namespace Games.Hubs.Models
{
    public class GameEvent
    {
        public Guid GameId { get; set; }

        public string? EventType { get; set; }

        public int LockState { get; set; }

        public Dictionary<string, object?>? Data { get; set; }

        public static GameEvent Accepted(Guid gameId) =>
            new GameEvent
            {
                GameId = gameId,
                EventType = "Accepted",
                Data = new Dictionary<string, object?>()
            };

        public static GameEvent Reconnect(Guid gameId, int lockState) =>
            new GameEvent
            {
                GameId = gameId,
                EventType = "Reconnect",
                LockState = lockState,
                Data = new Dictionary<string, object?>
                {
                    { "lockState", lockState}
                }
            };
    }
}