namespace Games.Hubs.Models
{
    public class GameActionResult
    {
        private GameActionResult(string? error)
        {
            ActionError = error;
            WasSuccessful = error == null;
        }

        public bool WasSuccessful { get; }
        public string? ActionError { get; }

        public static GameActionResult Success()
            => new GameActionResult(null);

        public static GameActionResult Error(string error)
            => new GameActionResult(error);
    }
}