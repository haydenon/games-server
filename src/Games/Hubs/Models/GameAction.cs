using System;
using System.Collections.Generic;

namespace Games.Hubs.Models
{
    public class GameAction
    {
        public Guid GameId { get; set; }

        public string? ActionType { get; set; }

        public Dictionary<string, object>? Parameters { get; set; }
    }
}