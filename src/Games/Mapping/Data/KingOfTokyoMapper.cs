// using System.Linq;
// using Games.Data.Entities.KingOfTokyo;
// using Games.KingOfTokyo;
// using Games.Shared;
// using LanguageExt;
// using static LanguageExt.Prelude;

// namespace Games.Mapping.Data
// {
//     public static class KingOfTokyoMapper
//     {
//         public static KingOfTokyoGameData MapToData(KingOfTokyoGame game)
//             => new KingOfTokyoGameData
//             {
//                 Id = game.Id,
//                 LockState = game.LockState,
//                 State = KingOfTokyoStateMapper.MapToData(game.State),
//                 CreatorPlayerId = game.CreatorPlayerId,
//                 Players = game.Players.Select(p => (KingOfTokyoPlayer)p).ToList(),
//                 PlayerState = game.PlayerState,
//                 Avatars = game.Avatars,
//                 PlayerInventories = game.PlayerInventories,
//                 PlayerInTokyoCity = game.PlayerInTokyoCity,
//                 PlayerInTokyoBay = game.PlayerInTokyoBay,
//                 DiceRoll = game.DiceRoll,
//                 CardStore = game.CardStore,
//                 DiscardedCards = game.DiscardedCards
//             };
//         public static Option<KingOfTokyoGame> MapFromData(KingOfTokyoGameData data)
//         {
//             var players = data.Players?.Select(p => (IPlayer<KingOfTokyoGame>)p).ToList();
//             var avatars = data.Avatars;
//             var diceRoll = data.DiceRoll;
//             var cardStore = data.CardStore;
//             var discarded = data.DiscardedCards;
//             var state = KingOfTokyoStateMapper.MapFromData(data.State);

//             if (players == null ||
//                 avatars == null ||
//                 diceRoll == null ||
//                 cardStore == null ||
//                 discarded == null ||
//                 state.IsNone)
//             {
//                 return None;
//             }

//             return state.Map(gameState => new KingOfTokyoGame(data.Id, data.CreatorPlayerId, players, avatars)
//             {
//                 LockState = data.LockState,
//                 State = gameState,
//                 PlayerState = data.PlayerState,
//                 PlayerInventories = data.PlayerInventories,
//                 PlayerInTokyoCity = data.PlayerInTokyoCity,
//                 PlayerInTokyoBay = data.PlayerInTokyoBay,
//                 DiceRoll = diceRoll,
//                 CardStore = cardStore,
//                 DiscardedCards = discarded
//             });
//         }

//     }
// }