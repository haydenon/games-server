using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using Games.Data.Entities.KingOfTokyo;
using Games.KingOfTokyo.State;
using LanguageExt;
using static LanguageExt.Prelude;

namespace Games.Mapping.Data
{
    public static class KingOfTokyoStateDataMapper
    {
        #region MapToData

        public static KingOfTokyoStateData MapToData(IState state)
            => new KingOfTokyoStateData
            {
                State = GetStateType(state).ToString(),
                Attributes = state switch
                {
                    BuyCardsState buyCards => MapToBuyCards(buyCards),
                    LeaveTokyoState leaveTokyo => MapToLeaveTokyo(leaveTokyo),
                    LobbyState lobby => MapToLobby(lobby),
                    PlayerRollStartState playerRollStart => MapToPlayerRollStart(playerRollStart),
                    PlayerRollState playerRoll => MapToPlayerRoll(playerRoll),
                    PlayerTurnStartState playerTurnStart => MapToPlayerTurnStart(playerTurnStart),
                    PlayerWinState playerWin => MapToPlayerWin(playerWin),
                    _ => throw new NotImplementedException($"Mapping not implemented for state '{state.GetType().Name}'")
                }
            };

        private static StateType GetStateType(IState state)
            => state switch
            {
                BuyCardsState => StateType.BuyCards,
                LeaveTokyoState => StateType.LeaveTokyo,
                LobbyState => StateType.LobbyState,
                PlayerRollStartState => StateType.PlayerRollStart,
                PlayerRollState => StateType.PlayerRoll,
                PlayerTurnStartState => StateType.PlayerTurnStart,
                PlayerWinState => StateType.PlayerWin,
                _ => throw new NotImplementedException($"Mapping not implemented for state '{state.GetType().Name}'")
            };

        private static Dictionary<string, object?> MapToBuyCards(BuyCardsState model)
            => new Dictionary<string, object?>
            {
                { nameof(BuyCardsState.Player), model.Player.ToString() }
            };

        private static Dictionary<string, object?> MapToLeaveTokyo(LeaveTokyoState model)
            => new Dictionary<string, object?>
            {
                { nameof(LeaveTokyoState.OriginalPlayer), model.OriginalPlayer.ToString() },
                { nameof(LeaveTokyoState.NextState), model.NextState.ToString() },
                { nameof(LeaveTokyoState.AwaitingConfirmationPlayers), model.AwaitingConfirmationPlayers?.Select(p => p.ToString()).ToArray() ?? new string[0] },
            };

        private static Dictionary<string, object?> MapToLobby(LobbyState model)
            => new Dictionary<string, object?>();

        private static Dictionary<string, object?> MapToPlayerRollStart(PlayerRollStartState model)
            => new Dictionary<string, object?>
            {
                { nameof(PlayerRollStartState.Player), model.Player.ToString() }
            };

        private static Dictionary<string, object?> MapToPlayerRoll(PlayerRollState model)
            => new Dictionary<string, object?>
            {
                { nameof(PlayerRollState.Player), model.Player.ToString() },
                { nameof(PlayerRollState.RollNumber), model.RollNumber}
            };

        private static Dictionary<string, object?> MapToPlayerTurnStart(PlayerTurnStartState model)
            => new Dictionary<string, object?>
            {
                { nameof(PlayerTurnStartState.Player), model.Player.ToString() }
            };

        private static Dictionary<string, object?> MapToPlayerWin(PlayerWinState model)
            => new Dictionary<string, object?>
            {
                { nameof(PlayerWinState.Player), model.Player?.ToString() }
            };

        #endregion

        #region MapFromData

        public static Option<IState> MapFromData(KingOfTokyoStateData? data)
        {
            if (data == null)
            {
                return None;
            }

            return Enum.Parse<StateType>(data.State!) switch
            {
                StateType.BuyCards => MapFromBuyCards(data),
                StateType.LeaveTokyo => MapFromLeaveTokyo(data),
                StateType.LobbyState => MapFromLobby(data),
                StateType.PlayerRollStart => MapFromPlayerRollStart(data),
                StateType.PlayerRoll => MapFromPlayerRoll(data),
                StateType.PlayerTurnStart => MapFromPlayerTurnStart(data),
                StateType.PlayerWin => MapFromPlayerWin(data),
                _ => throw new NotImplementedException($"Mapping not implemented for state '{data.State!.ToString()}'")
            };
        }

        private static BuyCardsState MapFromBuyCards(KingOfTokyoStateData data)
            => new BuyCardsState
            {
                Player = ParseGuid(data, nameof(BuyCardsState.Player))
            };

        private static LeaveTokyoState MapFromLeaveTokyo(KingOfTokyoStateData data)
            => new LeaveTokyoState
            {
                OriginalPlayer = ParseGuid(data, nameof(LeaveTokyoState.OriginalPlayer)),
                NextState = EnumValue<LeaveTokyoNextState>(data, nameof(LeaveTokyoState.NextState)),
                AwaitingConfirmationPlayers = ParseArray(data, Guid.Parse, nameof(LeaveTokyoState.AwaitingConfirmationPlayers))
            };

        private static LobbyState MapFromLobby(KingOfTokyoStateData data)
            => new LobbyState();

        private static PlayerRollStartState MapFromPlayerRollStart(KingOfTokyoStateData data)
            => new PlayerRollStartState
            {
                Player = ParseGuid(data, nameof(PlayerRollStartState.Player))
            };

        private static PlayerRollState MapFromPlayerRoll(KingOfTokyoStateData data)
            => new PlayerRollState
            {
                Player = ParseGuid(data, nameof(PlayerRollState.Player)),
                RollNumber = GetInt(data, nameof(PlayerRollState.RollNumber))
            };

        private static PlayerTurnStartState MapFromPlayerTurnStart(KingOfTokyoStateData data)
            => new PlayerTurnStartState
            {
                Player = ParseGuid(data, nameof(PlayerTurnStartState.Player))
            };

        private static PlayerWinState MapFromPlayerWin(KingOfTokyoStateData data)
            => new PlayerWinState
            {
                Player = ParseGuid(data, nameof(PlayerWinState.Player))
            };

        #endregion

        private static int GetInt(KingOfTokyoStateData data, string attribute)
            => data.Attributes?[attribute] switch
            {
                JsonElement elem => elem.GetInt32(),
                long val => (int)val,
                int val => val,
                _ => throw new InvalidOperationException("Invalid data")
            };

        private static Guid ParseGuid(KingOfTokyoStateData data, string attribute)
        {
            var str = data.Attributes?[attribute] switch
            {
                JsonElement elem => elem.GetString(),
                object value => value as string,
                _ => throw new InvalidOperationException("Invalid data")
            };
            return str != null ? Guid.Parse(str) : Guid.Empty;
        }

        private static object GetValueFromElem(JsonElement element)
            => element.ValueKind switch
            {
                JsonValueKind.String => element.GetString()!,
                JsonValueKind.Null => element.GetInt32()!,
                _ => throw new InvalidOperationException("Invalid data")
            };

        private static T[] ParseArray<T>(KingOfTokyoStateData data, Func<string, T> map, string attribute)
        {
            var objs = data.Attributes?[attribute] switch
            {
                JsonElement elem => elem.EnumerateArray().Select(GetValueFromElem),
                IEnumerable<object> values => values,
                _ => throw new InvalidOperationException("Invalid data")
            };

            if (objs == null || !objs.Any())
            {
                return new T[0];
            }

            var strings = objs.Select(o => (string)o);
            return strings.Map(map).ToArray();
        }

        private static T EnumValue<T>(KingOfTokyoStateData data, string attribute)
            where T : struct
        {
            var value = data.Attributes?[attribute] switch
            {
                JsonElement elem => elem.GetString(),
                object val => val as string,
                _ => throw new InvalidOperationException("Invalid data")
            };

            if (value == null)
            {
                return default;
            }

            return Enum.Parse<T>(value);
        }
    }
}