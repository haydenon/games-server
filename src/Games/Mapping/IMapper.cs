using Games.Shared;

namespace Games.Mapping
{
    public interface IViewMapper<TData, TView> where TData : IModel
    {
        TView MapToView(TData model);
    }
}