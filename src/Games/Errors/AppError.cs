namespace Games.Errors
{
    public class AppError
    {
        private AppError(AppErrorType errorType, string message)
        {
            ErrorType = errorType;
            Message = message;
        }

        public AppErrorType ErrorType { get; }
        public string Message { get; }

        public static AppError NotFound(string message = "Item not found")
            => new AppError(AppErrorType.NotFound, message);

        public static AppError Invalid(string message = "Invalid request")
                    => new AppError(AppErrorType.Invalid, message);

        public static AppError LockStateError(string message = "Lock state does not match current state")
            => new AppError(AppErrorType.LockStateMismatch, message);

        public static AppError Unauthenticated(string message = "Not authenticated")
            => new AppError(AppErrorType.Unauthenticated, message);

        public static AppError Forbidden(string message = "You are not permitted to perform this action")
            => new AppError(AppErrorType.Forbidden, message);
    }
}