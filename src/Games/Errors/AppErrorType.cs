namespace Games.Errors
{
    public enum AppErrorType
    {
        NotFound,
        Invalid,
        LockStateMismatch,
        Unauthenticated,
        Forbidden
    }
}