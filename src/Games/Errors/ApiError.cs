using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace Games.Errors
{
    public class ApiError
    {
        private readonly HttpStatusCode code;
        private readonly string? error;

        public ApiError(HttpStatusCode code, string? error = null)
        {
            this.code = code;
            this.error = error;
        }

        public static ApiError FromAppError(AppError error)
        {
            var statusCode = error.ErrorType switch
            {
                AppErrorType.NotFound => HttpStatusCode.NotFound,
                AppErrorType.LockStateMismatch => HttpStatusCode.BadRequest,
                AppErrorType.Unauthenticated => HttpStatusCode.Unauthorized,
                AppErrorType.Forbidden => HttpStatusCode.Forbidden,
                AppErrorType.Invalid => HttpStatusCode.BadRequest,
                _ => HttpStatusCode.InternalServerError
            };
            return new ApiError(statusCode, error.Message);
        }

        public IActionResult ToResult()
        {
            return new ObjectResult(new ErrorResponse { Error = error })
            {
                StatusCode = (int)code
            };
        }

        private class ErrorResponse
        {
            public string? Error { get; set; }
        }
    }
}