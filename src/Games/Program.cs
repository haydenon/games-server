using System;
using System.Linq;
using System.IO;
using Google.Cloud.Storage.V1;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Games
{
    public class Program
    {
        public static void Main(string[] args)
        {
            SetupDataProtection();

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();

                    var portVar = Environment.GetEnvironmentVariable("PORT");
                    if (!string.IsNullOrWhiteSpace(portVar) && int.TryParse(portVar, out var port))
                    {
                        webBuilder.UseKestrel(options =>
                        {
                            options.ListenAnyIP(port);

                        });
                    }
                });

        private static void SetupDataProtection()
        {
            var projectId = Environment.GetEnvironmentVariable("ProjectId");
            var bucketName = Environment.GetEnvironmentVariable("DataProtection__BucketName");
            var localDirectory = Environment.GetEnvironmentVariable("DataProtection__LocalPath");
            if (string.IsNullOrEmpty(projectId)
                || string.IsNullOrEmpty(bucketName)
                || string.IsNullOrEmpty(localDirectory))
            {
                Console.WriteLine("Skipping data protection setup");
                return;
            }

            Directory.CreateDirectory(localDirectory);

            const string bucketDirectory = "/data-protection";

            var storage = StorageClient.Create();
            var bucket = storage.GetBucket(bucketName);

            var remoteKeys = storage.ListObjects(bucketName, bucketDirectory);
            foreach (var key in remoteKeys)
            {
                var filename = key.Name.Split("/").Last();
                Console.WriteLine($"Downloading key '{filename}'");
                using var fileStream = File.Create($"{localDirectory}/{filename}");
                storage.DownloadObject(bucketName, key.Name, fileStream);
            }

            var serviceCollection = new ServiceCollection();
            serviceCollection.AddDataProtection()
                .PersistKeysToFileSystem(new DirectoryInfo(localDirectory));
            var services = serviceCollection.BuildServiceProvider();
            var keyManager = services.GetRequiredService<IKeyManager>();
            var allKeys = keyManager.GetAllKeys();

            Console.WriteLine($"The key ring contains {allKeys.Count} key(s).");
            if (!allKeys.Any() || DateTimeOffset.UtcNow > allKeys.Max(k => k.ExpirationDate).AddDays(-2))
            {
                var key = keyManager.CreateNewKey(
                                activationDate: DateTimeOffset.UtcNow,
                                expirationDate: DateTimeOffset.UtcNow.AddMonths(3));
                var filename = $"key-{key.KeyId}.xml";
                var file = $"{localDirectory}/{filename}";
                using var fileStream = File.Open(file, FileMode.Open);
                storage.UploadObject(bucketName, $"{bucketDirectory}/{filename}", "text/xml; charset=utf-8", fileStream);
                Console.WriteLine($"Added a new protection key '{key.KeyId}'.");
            }

            foreach (var key in allKeys.Where(k => k.ExpirationDate < DateTimeOffset.UtcNow))
            {
                var filename = $"key-{key.KeyId}.xml";
                var file = $"{localDirectory}/{filename}";
                storage.DeleteObject(bucketName, $"{bucketDirectory}/{filename}");
                File.Delete(file);
            }
        }
    }
}
