using System.Linq;
using System.Threading.Tasks;
using LanguageExt;

namespace Games
{
    public static class Prelude
    {
        public static EitherAsync<TErr, T> rightAsync<T, TErr>(Task<T> item)
            => EitherAsync<TErr, T>.RightAsync(item);

        public static string toCamelCase(string name)
            => $"{char.ToLowerInvariant(name.First())}{name.Substring(1)}";
    }
}