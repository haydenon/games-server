using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Games.Hubs;
using Games.Setup;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Games
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private const string AllowOrigins = "AllowOrigins";

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var origins = Configuration.GetValue<string>("AllowedOrigin");
            services.AddCors(options =>
            {
                options.AddPolicy(
                    name: AllowOrigins,
                    builder =>
                    {
                        builder.WithOrigins(origins)
                            .AllowCredentials()
                            .WithMethods(
                                HttpMethods.Get,
                                HttpMethods.Post,
                                HttpMethods.Put,
                                HttpMethods.Patch,
                                HttpMethods.Delete,
                                HttpMethods.Options
                            )
                            .WithHeaders(
                                "Origin",
                                "Authorization",
                                "Cookie",
                                "Content-Type",
                                "X-Requested-With"
                            );
                    });
            });

            services.AddSignalR();
            var dataProtectionPath = Configuration.GetSection("DataProtection")["LocalPath"];
            services.AddDataProtection()
                .PersistKeysToFileSystem(new DirectoryInfo(dataProtectionPath));

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(options =>
            {
                options.Cookie.SameSite = SameSiteMode.None;
                options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                options.Cookie.Name = "AuthCookie";
                options.Events.OnRedirectToAccessDenied = UnAuthorizedResponse;
                options.Events.OnRedirectToLogin = UnAuthorizedResponse;
            });

            static async Task UnAuthorizedResponse(RedirectContext<CookieAuthenticationOptions> context)
            {
                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                var jsonString = "{\"error\": \"You must sign in to use this endpoint.\" }";
                byte[] data = Encoding.UTF8.GetBytes(jsonString);
                context.Response.ContentType = "application/json";
                await context.Response.Body.WriteAsync(data, 0, data.Length);
            }

            services.AddAuthorization();

            services.AddGameServices();
            services.AddDatabase(Configuration);
            services.AddKingOfTokyo();


            services.AddControllers(config =>
                        {
                            var policy = new AuthorizationPolicyBuilder()
                             .RequireAuthenticatedUser()
                             .Build();
                            config.Filters.Add(new AuthorizeFilter(policy));
                        });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors(AllowOrigins);
            app.UsePathBase(Configuration["BasePath"] ?? "");
            app.UseRouting();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseStaticFiles();
            }

            app.UseAuthorization();
            app.UseAuthentication();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<GameHub>("/game/hub");
            });

            var serviceProvider = app.ApplicationServices;
            GameCleanupHelper.SetupCleaning(serviceProvider);
        }
    }
}
