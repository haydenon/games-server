using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Games.Errors;
using LanguageExt;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;

using static LanguageExt.Prelude;
using static Games.Errors.AppError;

namespace Games.Auth
{
    public class AuthService : IAuthService
    {
        public const string Impersonation = "https://games.oneill.net.nz/identity/claims/impersonation";

        private readonly IConfiguration configuration;

        public AuthService(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        private bool IsImpersonationEnabled => configuration.GetValue<bool>("DebugEnabled");

        public EitherAsync<AppError, Guid> GetUserIdForHub(HubCallerContext context)
            => GetUserIdFromClaims(context.User?.Claims ?? new Claim[0]);

        public EitherAsync<ApiError, Guid> GetUserIdForHttp(HttpContext context)
            => GetUserIdFromClaims(context.User.Claims).MapLeft(ApiError.FromAppError);

        public EitherAsync<ApiError, Guid> GetRealUserIdForHttp(HttpContext context)
            => GetUserIdFromClaims(context.User.Claims, false).MapLeft(ApiError.FromAppError);

        public async Task SignInUser(HttpContext context, Guid userId)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, userId.ToString())
            };

            await SignIn(context, claims);
        }

        public async Task SignInUser(HttpContext context)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, Guid.NewGuid().ToString())
            };

            await SignIn(context, claims);
        }

        public EitherAsync<AppError, Unit> ImpersonateUser(HttpContext context, Guid signInUserId, Guid impersonateUserId)
        {
            if (!IsImpersonationEnabled)
            {
                return Forbidden("Impersonation is disabled");
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, signInUserId.ToString()),
                new Claim(Impersonation, impersonateUserId.ToString())
            };

            return EitherAsync<AppError, Unit>.RightAsync(SignIn(context, claims));
        }

        private EitherAsync<AppError, Guid> GetUserIdFromClaims(
            IEnumerable<Claim> claims,
            bool allowImpersonation = true)
        {
            var validImpersonation = allowImpersonation && IsImpersonationEnabled && claims.Any(c => c.Type == Impersonation);
            var identifier = validImpersonation ?
                claims.First(c => c.Type == Impersonation).Value :
                claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;

            if (identifier == null)
            {
                return Unauthenticated("Not signed in");
            }

            return parseGuid(identifier).ToEitherAsync(Unauthenticated("Invalid user id"));
        }

        private async Task<Unit> SignIn(HttpContext context, IEnumerable<Claim> claims)
        {
            var claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties
            {
                IsPersistent = true,
            };

            await context.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                authProperties);

            return unit;
        }
    }
}