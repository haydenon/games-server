using System;
using System.Threading.Tasks;
using Games.Errors;
using LanguageExt;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;

namespace Games.Auth
{
    public interface IAuthService
    {
        EitherAsync<AppError, Guid> GetUserIdForHub(HubCallerContext context);

        EitherAsync<ApiError, Guid> GetUserIdForHttp(HttpContext context);

        EitherAsync<ApiError, Guid> GetRealUserIdForHttp(HttpContext context);

        Task SignInUser(HttpContext context);

        Task SignInUser(HttpContext context, Guid userId);

        EitherAsync<AppError, Unit> ImpersonateUser(HttpContext context, Guid signInUserId, Guid impersonateUserId);
    }
}