using Games.KingOfTokyo.State.Models;
using Games.Shared;

namespace Games.KingOfTokyo.Actions
{
    public class RollDiceDebugAction : IAction<KingOfTokyoGame>
    {
        public RollDiceDebugAction(KingOfTokyoPlayer player, DiceRollType[] diceRoll)
        {
            ExecutingPlayer = player;
            DiceRoll = diceRoll;
        }

        public string Name => "roll dice debug";

        public IPlayer<KingOfTokyoGame> ExecutingPlayer { get; }

        public DiceRollType[] DiceRoll { get; }
    }
}