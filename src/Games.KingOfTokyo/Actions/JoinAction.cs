using Games.Shared;

namespace Games.KingOfTokyo.Actions
{
    public class JoinAction : IAction<KingOfTokyoGame>
    {
        public JoinAction(IPlayer<KingOfTokyoGame> executingPlayer)
        {
            ExecutingPlayer = executingPlayer;
        }

        public string Name => "join";

        public IPlayer<KingOfTokyoGame> ExecutingPlayer { get; }
    }
}