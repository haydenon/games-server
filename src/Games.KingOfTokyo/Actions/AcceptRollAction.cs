using Games.Shared;

namespace Games.KingOfTokyo.Actions
{
    public class AcceptDiceAction : IAction<KingOfTokyoGame>
    {
        public AcceptDiceAction(IPlayer<KingOfTokyoGame> executingPlayer)
        {
            ExecutingPlayer = executingPlayer;
        }

        public string Name => "accept dice roll";

        public IPlayer<KingOfTokyoGame> ExecutingPlayer { get; }
    }
}