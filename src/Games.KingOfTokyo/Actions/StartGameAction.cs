using Games.Shared;

namespace Games.KingOfTokyo.Actions
{
    public class StartGameAction : IAction<KingOfTokyoGame>
    {
        public StartGameAction(IPlayer<KingOfTokyoGame> executingPlayer)
        {
            ExecutingPlayer = executingPlayer;
        }

        public string Name => "start";

        public IPlayer<KingOfTokyoGame> ExecutingPlayer { get; }
    }
}