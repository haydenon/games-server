using Games.Shared;

namespace Games.KingOfTokyo.Actions
{
    public class FinishCardBuying : IAction<KingOfTokyoGame>
    {
        public FinishCardBuying(IPlayer<KingOfTokyoGame> executingPlayer)
        {
            ExecutingPlayer = executingPlayer;
        }

        public string Name => "finish buying";

        public IPlayer<KingOfTokyoGame> ExecutingPlayer { get; }
    }
}