using Games.Shared;

namespace Games.KingOfTokyo.Actions
{
    public class RollDiceAction : IAction<KingOfTokyoGame>
    {
        public RollDiceAction(IPlayer<KingOfTokyoGame> executingPlayer, bool[] diceToKeep)
        {
            ExecutingPlayer = executingPlayer;
            DiceToKeep = diceToKeep;
        }

        public string Name => "roll dice";

        public IPlayer<KingOfTokyoGame> ExecutingPlayer { get; }

        public bool[] DiceToKeep { get; }
    }
}