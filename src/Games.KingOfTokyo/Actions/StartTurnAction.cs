using Games.Shared;

namespace Games.KingOfTokyo.Actions
{
    public class StartTurnAction : IAction<KingOfTokyoGame>
    {
        public StartTurnAction(IPlayer<KingOfTokyoGame> executingPlayer)
        {
            ExecutingPlayer = executingPlayer;
        }

        public string Name => "start turn";

        public IPlayer<KingOfTokyoGame> ExecutingPlayer { get; }
    }
}