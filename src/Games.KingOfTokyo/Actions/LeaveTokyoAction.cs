using Games.Shared;

namespace Games.KingOfTokyo.Actions
{
    public class LeaveTokyoAction : IAction<KingOfTokyoGame>
    {
        public LeaveTokyoAction(IPlayer<KingOfTokyoGame> executingPlayer, bool leaveTokyo)
        {
            ExecutingPlayer = executingPlayer;
            LeaveTokyo = leaveTokyo;
        }

        public string Name => "leave tokyo";

        public IPlayer<KingOfTokyoGame> ExecutingPlayer { get; }

        public bool LeaveTokyo { get; }
    }
}