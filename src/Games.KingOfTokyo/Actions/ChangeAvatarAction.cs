using Games.KingOfTokyo.State.Player;
using Games.Shared;

namespace Games.KingOfTokyo.Actions
{
    public class ChangeAvatarAction : IAction<KingOfTokyoGame>
    {
        public ChangeAvatarAction(IPlayer<KingOfTokyoGame> executingPlayer, AvatarType? avatar)
        {
            ExecutingPlayer = executingPlayer;
            Avatar = avatar;
        }

        public string Name => "change avatar";

        public IPlayer<KingOfTokyoGame> ExecutingPlayer { get; }

        public AvatarType? Avatar { get; }
    }
}