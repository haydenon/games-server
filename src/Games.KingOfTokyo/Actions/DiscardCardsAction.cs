using Games.Shared;

namespace Games.KingOfTokyo.Actions
{
    public class DiscardCardsAction : IAction<KingOfTokyoGame>
    {
        public DiscardCardsAction(IPlayer<KingOfTokyoGame> executingPlayer)
        {
            ExecutingPlayer = executingPlayer;
        }

        public string Name => "discard cards";

        public IPlayer<KingOfTokyoGame> ExecutingPlayer { get; }
    }
}