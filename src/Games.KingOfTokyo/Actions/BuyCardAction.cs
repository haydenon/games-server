using Games.KingOfTokyo.State.Models;
using Games.Shared;

namespace Games.KingOfTokyo.Actions
{
    public class BuyCardAction : IAction<KingOfTokyoGame>
    {
        public BuyCardAction(IPlayer<KingOfTokyoGame> executingPlayer, CardType cardType)
        {
            ExecutingPlayer = executingPlayer;
            CardType = cardType;
        }

        public string Name => "buy card";

        public IPlayer<KingOfTokyoGame> ExecutingPlayer { get; }

        public CardType CardType { get; }
    }
}