using System.Collections.Generic;
using System.Linq;
using Games.KingOfTokyo.Events;
using Games.KingOfTokyo.State;
using Games.KingOfTokyo.State.Models;
using Games.Shared.StateUpdates;

namespace Games.KingOfTokyo.Updates
{
    public record EndOfTurnUpdate() : IStateUpdateDefinition;

    public static class EndOfTurnUpdater
    {
        public static UpdateFunc<KingOfTokyoGame> EndOfTurn(
            IStateUpdater<KingOfTokyoGame> updater,
            EndOfTurnUpdate _) =>
            (result) =>
        {
            var player = result.ExecutingPlayer;
            var game = result.GameState;

            result.Then(HandleCardUpdates(updater));

            if (GameWinUpdater.HasEnded(game))
            {
                return result.Then(updater.Update(new EndGameByPointsUpdate()));
            }

            game.MarkUpdated();
            var currentPlayer = game.Players.First(p => p.Id == player);
            var currentIndex = game.Players.IndexOf(currentPlayer);
            var nextIndex = (currentIndex + 1) % game.Players.Count;
            var nextPlayer = game.Players[nextIndex];
            while (game.IsPlayerDead(nextPlayer.Id))
            {
                nextIndex = (nextIndex + 1) % game.Players.Count;
                nextPlayer = game.Players[nextIndex];
            }

            var nextId = nextPlayer.Id;
            if (game.IsInTokyo(nextId))
            {
                const int TokyoTurnStartPoints = 2;
                result.Then(updater.Update(new ChangeVictoryPointsUpdate(nextId, TokyoTurnStartPoints)));
            }

            var playerTurnStartState = new PlayerTurnStartState { Player = nextPlayer.Id };
            game.State = playerTurnStartState;

            var playerStartEvent = new PlayerTurnStartEvent(game.LockState, nextPlayer.Id);

            return result.Append(playerStartEvent);
        };

        private static UpdateFunc<KingOfTokyoGame> HandleCardUpdates(
            IStateUpdater<KingOfTokyoGame> updater) =>
            (result) =>
        {
            var game = result.GameState;
            var player = result.ExecutingPlayer;

            if (game.IsPlayerDead(player))
            {
                return result;
            }

            if (game.HasCard(player, CardType.RootingForTheUnderdog))
            {
                var minPoints = game.PlayerState?.Select(s => s.Value.VictoryPoints).Min() ?? 0;
                var playerState = game.PlayerState?[player];
                if (playerState != null && playerState.VictoryPoints <= minPoints)
                {
                    result.Then(updater.Update(new ChangeVictoryPointsUpdate(player, 1, CardType.RootingForTheUnderdog)));
                }
            }

            if (game.HasCard(player, CardType.EnergyHoarder))
            {
                var playerState = game.PlayerState?[player];
                if (playerState != null && playerState.Energy >= Constants.EnergyHoarderDivisor)
                {
                    var points = playerState.Energy / Constants.EnergyHoarderDivisor;
                    result.Then(updater.Update(new ChangeVictoryPointsUpdate(player, points, CardType.EnergyHoarder)));
                }
            }

            return result;
        };
    }

    public class EndOfTurnContainer : IStateUpdaterContainer<KingOfTokyoGame>
    {
        public IEnumerable<StateUpdaterInstantiator<KingOfTokyoGame>> GetInstantiators(IStateUpdaterInstantiator<KingOfTokyoGame> instantiator)
        {
            yield return instantiator.Create<EndOfTurnUpdate>((factory, endOfTurnUpdate) => EndOfTurnUpdater.EndOfTurn(factory, endOfTurnUpdate));
        }
    }
}