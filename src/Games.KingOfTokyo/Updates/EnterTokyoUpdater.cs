using System;
using System.Collections.Generic;
using System.Linq;
using Games.KingOfTokyo.Events;
using Games.Shared;
using Games.Shared.StateUpdates;

namespace Games.KingOfTokyo.Updates
{
    public record EnterTokyoUpdate() : IStateUpdateDefinition;

    public static class EnterTokyoUpdater
    {
        public static UpdateFunc<KingOfTokyoGame> EnterTokyo(
            IStateUpdater<KingOfTokyoGame> updater, EnterTokyoUpdate _) =>
            (result) =>
        {
            var game = result.GameState;
            var player = result.ExecutingPlayer;

            var playerAlreadyInTokyo = game.PlayerInTokyoCity == player || game.PlayerInTokyoBay == player;

            var wasUpdated = false;
            var bayEnabled = game.PlayerState != null && game.PlayerState.Count(state => !state.Value.IsDead) >= Constants.BayEnabledPlayers;
            if (!game.PlayerInTokyoCity.HasValue && !playerAlreadyInTokyo)
            {
                wasUpdated = true;
                game.PlayerInTokyoCity = player;
            }
            else if (bayEnabled && !game.PlayerInTokyoBay.HasValue && !playerAlreadyInTokyo)
            {
                wasUpdated = true;
                game.PlayerInTokyoBay = player;
            }

            if (wasUpdated)
            {
                game.MarkUpdated();

                var enteredEvent = new EnteredTokyoEvent(game.LockState, player, game.PlayerInTokyoBay == player);
                result.Append(enteredEvent);

                const int TokyoEnterPoints = 1;
                result.Then(updater.Update(new ChangeVictoryPointsUpdate(player, TokyoEnterPoints)));
            }

            return result.Then(updater.Update(new BuyCardsUpdate()));
        };
    }

    public class EnterTokyoContainer : IStateUpdaterContainer<KingOfTokyoGame>
    {
        public IEnumerable<StateUpdaterInstantiator<KingOfTokyoGame>> GetInstantiators(IStateUpdaterInstantiator<KingOfTokyoGame> instantiator)
        {
            yield return instantiator.Create<EnterTokyoUpdate>((factory, enterTokyo) => EnterTokyoUpdater.EnterTokyo(factory, enterTokyo));
        }
    }
}