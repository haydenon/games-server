using System;
using System.Collections.Generic;
using System.Linq;
using Games.KingOfTokyo.Events;
using Games.KingOfTokyo.State.Models;
using Games.Shared;
using Games.Shared.StateUpdates;

namespace Games.KingOfTokyo.Updates
{
  public record ActionDiscardCardUpdate(CardType CardType) : IStateUpdateDefinition;

  public record DeathFromAboveUpdate() : IStateUpdateDefinition;

  public static class DiscardCardActionUpdater
  {
    public static UpdateFunc<KingOfTokyoGame> ActionDiscardCard(
        IStateUpdater<KingOfTokyoGame> updater,
        ActionDiscardCardUpdate discardUpdate) =>
        (ActionHandleResult<KingOfTokyoGame> result) =>
        {
          var cardType = discardUpdate.CardType;
          if (cardType == CardType.DeathFromAbove)
          {
            return result.Then(updater.Update(new DeathFromAboveUpdate()));
          }

          return result;
        };

    public static UpdateFunc<KingOfTokyoGame> HandleDeathFromAbove(
        IStateUpdater<KingOfTokyoGame> updater, DeathFromAboveUpdate _) =>
        (result) =>
    {
      var game = result.GameState;
      var player = result.ExecutingPlayer;

      var playerAlreadyInTokyo = game.PlayerInTokyoCity == player || game.PlayerInTokyoBay == player;
      var bayEnabled = game.PlayerState != null && game.PlayerState.Count(state => !state.Value.IsDead) >= Constants.BayEnabledPlayers;
      if (!playerAlreadyInTokyo)
      {
        game.MarkUpdated();
        if (game.PlayerInTokyoCity.HasValue && bayEnabled && !game.PlayerInTokyoBay.HasValue)
        {
          game.PlayerInTokyoBay = player;
          var enteredEvent = new EnteredTokyoEvent(game.LockState, player, true);
          result.Append(enteredEvent);
        }
        else
        {
          if (game.PlayerInTokyoCity.HasValue)
          {
            result.Append(new LeaveTokyoEvent(game.LockState, game.PlayerInTokyoCity.Value, true));
          }

          game.PlayerInTokyoCity = player;
          var enteredEvent = new EnteredTokyoEvent(game.LockState, player, false);
          result.Append(enteredEvent);
        }
      }

      return result;
    };
  }

  public class DiscardCardActionContainer : IStateUpdaterContainer<KingOfTokyoGame>
  {
    public IEnumerable<StateUpdaterInstantiator<KingOfTokyoGame>> GetInstantiators(IStateUpdaterInstantiator<KingOfTokyoGame> instantiator)
    {
      yield return instantiator.Create<ActionDiscardCardUpdate>((factory, def) => DiscardCardActionUpdater.ActionDiscardCard(factory, def));

      yield return instantiator.Create<DeathFromAboveUpdate>((factory, def) => DiscardCardActionUpdater.HandleDeathFromAbove(factory, def));
    }
  }
}