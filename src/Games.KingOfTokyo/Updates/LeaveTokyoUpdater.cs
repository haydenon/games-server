using System;
using System.Collections.Generic;
using System.Linq;
using Games.KingOfTokyo.Events;
using Games.KingOfTokyo.State;
using Games.Shared;
using Games.Shared.StateUpdates;

namespace Games.KingOfTokyo.Updates
{
    public record TokyoDeadPlayersLeaveUpdate() : IStateUpdateDefinition;

    public record RequestLeaveTokyoUpdate(Guid[] DamagedPlayers, LeaveTokyoNextState NextState) : IStateUpdateDefinition;

    public static class LeaveTokyoUpdater
    {
        public static UpdateFunc<KingOfTokyoGame> HandleDeadPlayersInTokyo(
            IStateUpdater<KingOfTokyoGame> updater) =>
            (result) =>
        {
            var game = result.GameState;
            var deadInTokyo = game.Players.Where(p => game.IsPlayerDead(p.Id) && game.IsInTokyo(p.Id));
            var aliveCount = game.Players.Count(p => !game.IsPlayerDead(p.Id));

            if (deadInTokyo.Any() || (aliveCount < Constants.BayEnabledPlayers && game.PlayerInTokyoBay.HasValue))
            {
                game.MarkUpdated();
            }

            if (deadInTokyo.Any(p => game.PlayerInTokyoBay == p.Id))
            {
                result.Append(new LeaveTokyoEvent(game.LockState, game.PlayerInTokyoBay ?? default, true));
                game.PlayerInTokyoBay = null;
            }

            if (deadInTokyo.Any(p => game.PlayerInTokyoCity == p.Id))
            {
                result.Append(new LeaveTokyoEvent(game.LockState, game.PlayerInTokyoCity ?? default, true));
                game.PlayerInTokyoCity = null;
            }

            if (aliveCount < Constants.BayEnabledPlayers && game.PlayerInTokyoBay.HasValue)
            {
                var bayPlayer = game.PlayerInTokyoBay.Value;

                game.PlayerInTokyoBay = null;
                result.Append(new LeaveTokyoEvent(game.LockState, bayPlayer, true));

                if (!game.PlayerInTokyoCity.HasValue)
                {
                    game.PlayerInTokyoCity = bayPlayer;
                    result.Append(new EnteredTokyoEvent(game.LockState, bayPlayer, false));
                }
            }

            return result;
        };

        public static bool RequiresLeaveRequest(KingOfTokyoGame game, Guid[] damagedPlayers)
            => damagedPlayers.Any(p => game.IsInTokyo(p) && !game.IsPlayerDead(p));

        public static UpdateFunc<KingOfTokyoGame> RequestLeaveTokyo(
            IStateUpdater<KingOfTokyoGame> updater,
            Guid[] damagedPlayers,
            LeaveTokyoNextState nextState) =>
            (result) =>
            {
                var game = result.GameState;
                game.MarkUpdated();

                var toRequest = game.Players
                    .Where(p => game.IsInTokyo(p.Id) && damagedPlayers.Contains(p.Id) && !game.IsPlayerDead(p.Id))
                    .Select(p => p.Id)
                    .ToArray();

                var state = new LeaveTokyoState
                {
                    OriginalPlayer = result.ExecutingPlayer,
                    AwaitingConfirmationPlayers = toRequest,
                    NextState = nextState
                };
                game.State = state;

                var leaveRequestEvent = new LeaveTokyoRequestEvent(game.LockState, toRequest, nextState);
                result.Append(leaveRequestEvent);

                return result;
            };
    }

    public class LeaveTokyoContainer : IStateUpdaterContainer<KingOfTokyoGame>
    {
        public IEnumerable<StateUpdaterInstantiator<KingOfTokyoGame>> GetInstantiators(IStateUpdaterInstantiator<KingOfTokyoGame> instantiator)
        {
            yield return instantiator.Create<TokyoDeadPlayersLeaveUpdate>((factory, _) => LeaveTokyoUpdater.HandleDeadPlayersInTokyo(factory));

            yield return instantiator.Create<RequestLeaveTokyoUpdate>((factory, def) => LeaveTokyoUpdater.RequestLeaveTokyo(factory, def.DamagedPlayers, def.NextState));
        }
    }
}