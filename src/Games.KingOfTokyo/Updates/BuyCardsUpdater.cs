using System;
using System.Collections.Generic;
using System.Linq;
using Games.KingOfTokyo.Events;
using Games.KingOfTokyo.State;
using Games.Shared;
using Games.Shared.StateUpdates;

namespace Games.KingOfTokyo.Updates
{
    public record BuyCardsUpdate() : IStateUpdateDefinition;

    public static class BuyCardsUpdater
    {
        public const int DiscardCost = 2;

        public static UpdateFunc<KingOfTokyoGame> BuyCards(
            IStateUpdater<KingOfTokyoGame> updater) =>
            (result) =>
         {
             var player = result.ExecutingPlayer;
             var game = result.GameState;

             // If the player has died, then go straight to end of turn
             if (game.IsPlayerDead(player))
             {
                 return result.Then(updater.Update(new EndOfTurnUpdate()));
             }

             var playerState = game.PlayerState?[player];
             if (playerState != null && playerState.Energy >= DiscardCost)
             {

                 game.MarkUpdated();
                 var buyCardsState = new BuyCardsState { Player = player };
                 game.State = buyCardsState;

                 var buyCardRequestEvent = new BuyCardRequestEvent(game.LockState, player);
                 return result.Append(buyCardRequestEvent);
             }

             var currentPlayer = game.Players.First(p => p.Id == player);
             var message = $"{currentPlayer.Name} does not have enough energy to buy a card or discard.";
             var refuseCardEvent = new RefuseCardBuyEvent(game.LockState, player, message);
             result.Append(refuseCardEvent);

             return result.Then(updater.Update(new EndOfTurnUpdate()));
         };
    }

    public class BuyCardsContainer : IStateUpdaterContainer<KingOfTokyoGame>
    {

        public IEnumerable<StateUpdaterInstantiator<KingOfTokyoGame>> GetInstantiators(
            IStateUpdaterInstantiator<KingOfTokyoGame> instantiator)
        {
            yield return instantiator.Create<BuyCardsUpdate>((factory, _) => BuyCardsUpdater.BuyCards(factory));
        }
    }
}