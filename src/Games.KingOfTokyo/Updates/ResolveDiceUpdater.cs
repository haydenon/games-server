using System;
using System.Collections.Generic;
using System.Linq;
using Games.KingOfTokyo.State;
using Games.KingOfTokyo.State.Models;
using Games.Shared;
using Games.Shared.StateUpdates;

namespace Games.KingOfTokyo.Updates
{
    public record ResolveDiceUpdate() : IStateUpdateDefinition;

    public static class ResolveDiceUpdater
    {
        public static UpdateFunc<KingOfTokyoGame> ResolveDice(
            IStateUpdater<KingOfTokyoGame> updater) =>
            (result) =>
        {
            var game = result.GameState;
            var player = result.ExecutingPlayer;
            if (!(game.State is PlayerRollState rollState))
            {
                return result;
            }

            var playerState = game.PlayerState?[result.ExecutingPlayer];

            var diceRoll = game.DiceRoll.Select(dr => dr.DiceRollType).ToArray();

            var points = ResolvePoints(diceRoll);
            if (points > 0)
            {
                result.Then(updater.Update(new ChangeVictoryPointsUpdate(player, points)));
            }

            var health = ResolveHealth(game, player, diceRoll);
            if (health > 0)
            {
                result.Then(updater.Update(new ChangeHealthUpdate(player, health)));
            }

            var energy = ResolveEnergy(diceRoll);
            if (energy > 0 && playerState != null)
            {
                result.Then(updater.Update(new ChangeEnergyUpdate(player, energy)));
            }

            var (damage, players) = ResolveDamage(game, player, diceRoll);
            if (damage > 0 && players.Any() && game.PlayerState != null)
            {
                game.MarkUpdated();
                foreach (var damagedPlayer in players)
                {
                    result.Then(updater.Update(new ChangeHealthUpdate(damagedPlayer, -damage)));
                }
                result.Then(updater.Update(new TokyoDeadPlayersLeaveUpdate()));

                if (GameWinUpdater.HasEndedByCombat(game))
                {
                    return result.Then(updater.Update(new EndGameByCombatUpdate()));
                }
                else if (LeaveTokyoUpdater.RequiresLeaveRequest(game, players))
                {
                    return result.Then(updater.Update(new RequestLeaveTokyoUpdate(players, LeaveTokyoNextState.EnterTokyo)));
                }
            }

            return result.Then(updater.Update(new EnterTokyoUpdate()));
        };

        private static int ResolveEnergy(DiceRollType[] diceRolls)
        {
            return diceRolls.Count(r => r == DiceRollType.Energy);
        }

        private static int ResolveHealth(KingOfTokyoGame game, Guid player, DiceRollType[] diceRolls)
        {
            if (game.PlayerInTokyoCity == player || game.PlayerInTokyoBay == player)
            {
                return 0;
            }

            return diceRolls.Count(r => r == DiceRollType.Heart);
        }

        private static int ResolvePoints(DiceRollType[] diceRolls)
        {
            var onePoints = CountPoints(1, diceRolls.Count(r => r == DiceRollType.One));
            var twoPoints = CountPoints(2, diceRolls.Count(r => r == DiceRollType.Two));
            var threePoints = CountPoints(3, diceRolls.Count(r => r == DiceRollType.Three));
            return onePoints + twoPoints + threePoints;
        }

        private static int CountPoints(int baseNum, int count)
        {
            if (count < 3)
            {
                return 0;
            }

            return baseNum + (count - 3);
        }

        private static (int, Guid[]) ResolveDamage(KingOfTokyoGame game, Guid player, DiceRollType[] diceRolls)
        {
            var damage = diceRolls.Count(r => r == DiceRollType.Claw);
            if (damage >= 1 && game.HasCard(player, CardType.SpikedTail))
            {
                damage++;
                // TODO: attribute damage to card
            }

            var inTokyo = game.PlayerInTokyoCity == player || game.PlayerInTokyoBay == player;
            if (inTokyo)
            {
                var players = game.PlayersOutsideTokyo();
                return (damage, players);
            }
            else
            {
                var city = game.PlayerInTokyoCity;
                var bay = game.PlayerInTokyoBay;

                var players = new[] { city, bay }
                    .Select(p => p ?? default(Guid))
                    .Where(p => p != default(Guid))
                    .ToArray();
                return (damage, players);
            }
        }
    }

    public class ResolveDiceContainer : IStateUpdaterContainer<KingOfTokyoGame>
    {
        public IEnumerable<StateUpdaterInstantiator<KingOfTokyoGame>> GetInstantiators(IStateUpdaterInstantiator<KingOfTokyoGame> instantiator)
        {
            yield return instantiator.Create<ResolveDiceUpdate>((factory, _) => ResolveDiceUpdater.ResolveDice(factory));
        }
    }
}