using System;
using System.Collections.Generic;
using Games.KingOfTokyo.Events;
using Games.KingOfTokyo.State.Models;
using Games.Shared.StateUpdates;

namespace Games.KingOfTokyo.Updates
{
  public record ChangeEnergyUpdate(Guid ChangePlayerId, int Energy, CardType? CardType = null) : IStateUpdateDefinition;

  public static class ChangeEnergyUpdater
  {
    public static UpdateFunc<KingOfTokyoGame> HandleEnergyChange(
        IStateUpdater<KingOfTokyoGame> updater,
        ChangeEnergyUpdate changeEnergyUpdate) =>
    (result) =>
    {
      var changePlayerId = changeEnergyUpdate.ChangePlayerId;
      var energy = changeEnergyUpdate.Energy;
      var cardType = changeEnergyUpdate.CardType;

      var game = result.GameState;

      var changePlayer = game.PlayerState?[changePlayerId];
      if (changePlayer == null)
      {
        return result;
      }

      var cardTypes = new List<CardType>();
      if (cardType.HasValue)
      {
        cardTypes.Add(cardType.Value);
      }

      if (energy > 0 && game.HasCard(changePlayerId, CardType.FriendOfChildren))
      {
        energy += 1;
        cardTypes.Add(CardType.FriendOfChildren);
      }

      game.MarkUpdated();

      var difference = changePlayer.UpdateEnergy(energy);
      var energyEvent = new ChangeEnergyEvent(game.LockState, result.ExecutingPlayer, changePlayerId, difference, cardTypes.ToArray());
      result.Append(energyEvent);

      return result;
    };
  }

  public class ChangeEnergyContainer : IStateUpdaterContainer<KingOfTokyoGame>
  {
    public IEnumerable<StateUpdaterInstantiator<KingOfTokyoGame>> GetInstantiators(IStateUpdaterInstantiator<KingOfTokyoGame> instantiator)
    {
      yield return instantiator.Create<ChangeEnergyUpdate>(
          (factory, def) => ChangeEnergyUpdater.HandleEnergyChange(factory, def));
    }
  }
}