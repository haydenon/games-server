using System;
using System.Collections.Generic;
using Games.KingOfTokyo.Events;
using Games.KingOfTokyo.State.Models;
using Games.Shared.StateUpdates;

namespace Games.KingOfTokyo.Updates
{
  public record ChangeHealthUpdate(Guid ChangePlayerId, int HealthChange, CardType? CardType = null) : IStateUpdateDefinition;

  public static class ChangeHealthUpdater
  {
    public static UpdateFunc<KingOfTokyoGame> HandleHealthChange(
        IStateUpdater<KingOfTokyoGame> updater,
        ChangeHealthUpdate changeHealthUpdate) =>
    (result) =>
    {
      var changePlayerId = changeHealthUpdate.ChangePlayerId;
      var healthChange = changeHealthUpdate.HealthChange;
      var cardType = changeHealthUpdate.CardType;

      var game = result.GameState;

      var changePlayer = game.PlayerState?[changePlayerId];
      if (changePlayer == null)
      {
        return result;
      }

      if (healthChange > 0 && changePlayer.IsAtFullHealth(10))
      {
        return result;
      }

      var cardTypes = new List<CardType>();
      if (cardType.HasValue)
      {
        cardTypes.Add(cardType.Value);
      }

      if (healthChange > 0 && game.HasCard(changePlayerId, CardType.Regeneration))
      {
        healthChange += 1;
        cardTypes.Add(CardType.Regeneration);
      }

      game.MarkUpdated();

      var difference = changePlayer.UpdateHitPoints(healthChange, 10);
      var healthEvent = new ChangeHealthEvent(game.LockState, result.ExecutingPlayer, changePlayerId, difference, cardTypes.ToArray());
      result.Append(healthEvent);

      return result;
    };
  }

  public class ChangeHealthContainer : IStateUpdaterContainer<KingOfTokyoGame>
  {
    public IEnumerable<StateUpdaterInstantiator<KingOfTokyoGame>> GetInstantiators(IStateUpdaterInstantiator<KingOfTokyoGame> instantiator)
    {
      yield return instantiator.Create<ChangeHealthUpdate>(
          (factory, def) => ChangeHealthUpdater.HandleHealthChange(factory, def));
    }
  }
}