using System;
using System.Collections.Generic;
using Games.KingOfTokyo.Events;
using Games.KingOfTokyo.State.Models;
using Games.Shared;
using Games.Shared.StateUpdates;

namespace Games.KingOfTokyo.Updates
{
  public record ChangeVictoryPointsUpdate(Guid ChangePlayerId, int Points, CardType? CardType = null) : IStateUpdateDefinition;

  public static class ChangeVictoryPointsUpdater
  {
    public static UpdateFunc<KingOfTokyoGame> HandlePointsChange(
        IStateUpdater<KingOfTokyoGame> updater,
        ChangeVictoryPointsUpdate changePointsUpdate) =>
    (result) =>
    {
      var changePlayerId = changePointsUpdate.ChangePlayerId;
      var victoryPoints = changePointsUpdate.Points;
      var cardType = changePointsUpdate.CardType;

      var game = result.GameState;

      var changePlayer = game.PlayerState?[changePlayerId];
      if (changePlayer == null)
      {
        return result;
      }

      game.MarkUpdated();

      var difference = changePlayer.UpdateVictoryPoints(victoryPoints);
      var cardTypes = cardType.HasValue ? new[] { cardType.Value } : new CardType[0];
      var pointsEvent = new ChangePointsEvent(game.LockState, result.ExecutingPlayer, changePlayerId, difference, cardTypes);
      result.Append(pointsEvent);

      return result;
    };
  }

  public class ChangeVictoryPointsContainer : IStateUpdaterContainer<KingOfTokyoGame>
  {
    public IEnumerable<StateUpdaterInstantiator<KingOfTokyoGame>> GetInstantiators(IStateUpdaterInstantiator<KingOfTokyoGame> instantiator)
    {
      yield return instantiator.Create<ChangeVictoryPointsUpdate>(
          (factory, def) => ChangeVictoryPointsUpdater.HandlePointsChange(factory, def));
    }
  }
}