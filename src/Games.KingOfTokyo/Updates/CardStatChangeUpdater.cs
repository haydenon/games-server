using System;
using System.Collections.Generic;
using System.Linq;
using Games.KingOfTokyo.Cards;
using Games.KingOfTokyo.State.Models;
using Games.KingOfTokyo.State.Player;
using Games.Shared;
using Games.Shared.StateUpdates;
using static Games.KingOfTokyo.Cards.CardStatUpdate.CardStatType;
using static Games.KingOfTokyo.Cards.CardStatUpdate.CardTarget;

namespace Games.KingOfTokyo.Updates
{
    public record ActionCardStatUpdate(CardType CardType, CardStatUpdate[] Updates) : IStateUpdateDefinition;

    public static class CardStatChangeUpdater
    {
        public static UpdateFunc<KingOfTokyoGame> ActionCardStatUpdate(
            IStateUpdater<KingOfTokyoGame> updater, ActionCardStatUpdate update) =>
            (ActionHandleResult<KingOfTokyoGame> result) =>
            {
                var updates = update.Updates;
                var cardType = update.CardType;
                result.GameState.MarkUpdated();

                foreach (var update in updates)
                {
                    result.Then(update switch
                    {
                        CardStatUpdate { StatType: HitPoints } hp => HandleHitPoints(updater, cardType, hp.Target, hp.CalcDelta),
                        CardStatUpdate { StatType: VictoryPoints } vp => HandleVictoryPoints(updater, cardType, vp.Target, vp.CalcDelta),
                        CardStatUpdate { StatType: Energy } e => HandleEnergy(updater, cardType, e.Target, e.CalcDelta),
                        _ => throw new NotImplementedException()
                    });
                }

                return result;
            };

        private static UpdateFunc<KingOfTokyoGame> HandleHitPoints(
            IStateUpdater<KingOfTokyoGame> updater,
            CardType cardType,
            CardStatUpdate.CardTarget target,
            Func<PlayerState, int> calcPoints)
            => (result) =>
            {
                var lockState = result.GameState.LockState;

                var relevantPlayers = GetPlayerStates(result, target);
                return relevantPlayers.Aggregate<(Guid, PlayerState), ActionHandleResult<KingOfTokyoGame>>(result,
                (res, idAndState) =>
                {
                    var (playerId, player) = idAndState;
                    var points = calcPoints(player);
                    return res.Then(updater.Update(new ChangeHealthUpdate(playerId, points, cardType)));
                });
            };


        private static UpdateFunc<KingOfTokyoGame> HandleVictoryPoints(
               IStateUpdater<KingOfTokyoGame> updater,
               CardType cardType,
               CardStatUpdate.CardTarget target,
               Func<PlayerState, int> calcPoints)
               => (result) =>
               {
                   var lockState = result.GameState.LockState;

                   var relevantPlayers = GetPlayerStates(result, target);
                   return relevantPlayers.Aggregate<(Guid, PlayerState), ActionHandleResult<KingOfTokyoGame>>(result,
                   (res, idAndState) =>
                   {
                       var (playerId, player) = idAndState;
                       var points = calcPoints(player);
                       return res.Then(updater.Update(new ChangeVictoryPointsUpdate(playerId, points, cardType)));
                   });
               };

        private static UpdateFunc<KingOfTokyoGame> HandleEnergy(
                IStateUpdater<KingOfTokyoGame> updater,
               CardType cardType,
               CardStatUpdate.CardTarget target,
               Func<PlayerState, int> calcPoints)
            => (result) =>
            {
                var lockState = result.GameState.LockState;

                var relevantPlayers = GetPlayerStates(result, target);
                return relevantPlayers.Aggregate<(Guid, PlayerState), ActionHandleResult<KingOfTokyoGame>>(result,
                (res, idAndState) =>
                {
                    var (playerId, player) = idAndState;
                    var points = calcPoints(player);
                    return res.Then(updater.Update(new ChangeEnergyUpdate(playerId, points, cardType)));
                });
            };

        private static (Guid, PlayerState)[] GetPlayerStates(ActionHandleResult<KingOfTokyoGame> result, CardStatUpdate.CardTarget target)
        {
            var player = result.ExecutingPlayer;
            var game = result.GameState;

            PlayerState GetState(Guid id)
            {
                var playerState = game?.PlayerState?[id];
                if (playerState == null)
                {
                    throw new InvalidCastException();
                }
                return playerState;
            }

            return target switch
            {
                Self => new[] { (player, GetState(player)) },
                Others => game.Players
                    .Where(p => p.Id != player && !game.IsPlayerDead(p.Id))
                    .Select(p => (p.Id, GetState(p.Id)))
                    .ToArray(),
                _ => game.Players.Where(p => !game.IsPlayerDead(p.Id))
                    .Select(p => (p.Id, GetState(p.Id)))
                    .ToArray()
            };
        }
    }

    public class CardStatChangeContainer : IStateUpdaterContainer<KingOfTokyoGame>
    {
        public IEnumerable<StateUpdaterInstantiator<KingOfTokyoGame>> GetInstantiators(
            IStateUpdaterInstantiator<KingOfTokyoGame> instantiator)
        {
            yield return instantiator.Create<ActionCardStatUpdate>(
                (factory, def) => CardStatChangeUpdater.ActionCardStatUpdate(factory, def));
        }
    }
}