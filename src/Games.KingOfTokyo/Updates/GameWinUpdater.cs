using System;
using System.Collections.Generic;
using System.Linq;
using Games.KingOfTokyo.Events;
using Games.KingOfTokyo.State;
using Games.KingOfTokyo.State.Player;
using Games.Shared;
using Games.Shared.StateUpdates;

namespace Games.KingOfTokyo.Updates
{
    public record EndGameByPointsUpdate() : IStateUpdateDefinition;

    public record EndGameByCombatUpdate() : IStateUpdateDefinition;

    public static class GameWinUpdater
    {
        public static bool HasEndedByCombat(KingOfTokyoGame game)
            => game.PlayerState != null && game.PlayerState.Count(ps => !ps.Value.IsDead) <= 1;

        public static bool HasEnded(KingOfTokyoGame game)
            => game.PlayerState != null && game.PlayerState.Any(ps => WonByPoints(ps.Value)) || HasEndedByCombat(game);

        public static UpdateFunc<KingOfTokyoGame> EndGame(
            IStateUpdater<KingOfTokyoGame> updater,
            EndGameByPointsUpdate _) =>
            (result) =>
        {
            var game = result.GameState;
            if (game.PlayerState != null && game.PlayerState.Any(ps => WonByPoints(ps.Value)))
            {
                game.MarkUpdated();
                var winner = game.PlayerState.First(ps => WonByPoints(ps.Value)).Key;
                game.State = new PlayerWinState { Player = winner };

                var winEvent = new PlayerWinEvent(game.LockState, winner);
                return result.Append(winEvent);
            }

            return result.Then(updater.Update(new EndGameByCombatUpdate()));
        };

        public static UpdateFunc<KingOfTokyoGame> EndGameByCombat(
            IStateUpdater<KingOfTokyoGame> updater,
            EndGameByCombatUpdate _) =>
            (result) =>
        {
            var game = result.GameState;

            var aliveCount = game.PlayerState?.Count(ps => !ps.Value.IsDead) ?? 0;
            if (aliveCount == 0)
            {
                game.MarkUpdated();
                game.State = new PlayerWinState { Player = null };

                var winEvent = new PlayerWinEvent(game.LockState, null);
                return result.Append(winEvent);
            }
            else if (game.PlayerState != null && aliveCount == 1)
            {
                game.MarkUpdated();
                var winner = game.PlayerState.First(ps => !ps.Value.IsDead).Key;
                game.State = new PlayerWinState { Player = winner };

                var winEvent = new PlayerWinEvent(game.LockState, winner);
                return result.Append(winEvent);
            }
            else
            {
                return result;
            }
        };

        private static bool WonByPoints(PlayerState playerState)
            => !playerState.IsDead && playerState.VictoryPoints >= 20;
    }

    public class GameWinContainer : IStateUpdaterContainer<KingOfTokyoGame>
    {
        public IEnumerable<StateUpdaterInstantiator<KingOfTokyoGame>> GetInstantiators(IStateUpdaterInstantiator<KingOfTokyoGame> instantiator)
        {
            yield return instantiator.Create<EndGameByPointsUpdate>((factory, endGameUpdate) => GameWinUpdater.EndGame(factory, endGameUpdate));

            yield return instantiator.Create<EndGameByCombatUpdate>((factory, endGameUpdate) => GameWinUpdater.EndGameByCombat(factory, endGameUpdate));
        }
    }
}