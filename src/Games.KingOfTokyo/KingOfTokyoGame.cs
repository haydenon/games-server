using System;
using System.Collections.Generic;
using Games.KingOfTokyo.State;
using Games.KingOfTokyo.State.Models;
using Games.KingOfTokyo.State.Player;
using Games.Shared;

namespace Games.KingOfTokyo
{
    public class KingOfTokyoGame : IGame
    {
        public const int MaxPlayers = 6;

        public KingOfTokyoGame(
            Guid id,
            IPlayer<KingOfTokyoGame> creator)
            : this(
                id,
                creator.Id,
                new List<IPlayer<KingOfTokyoGame>>(MaxPlayers),
                new Dictionary<Guid, AvatarType?>())
        {
        }

        public KingOfTokyoGame(
            Guid id,
            Guid creatorId,
            List<IPlayer<KingOfTokyoGame>> players,
            IDictionary<Guid, AvatarType?> avatars)
        {
            Id = id;
            CreatorPlayerId = creatorId;
            Players = players;
            Avatars = avatars;
        }

        public Guid Id { get; set; }

        public int LockState { get; set; } = 0;

        public bool IsUpdated { get; set; }

        public void MarkUpdated()
        {
            if (!IsUpdated)
            {
                LockState += 1;
                IsUpdated = true;
            }
        }

        public IState State { get; set; } = new LobbyState();

        public Guid CreatorPlayerId { get; }

        public Guid OwnerId => CreatorPlayerId;

        public IList<IPlayer<KingOfTokyoGame>> Players { get; }

        public IDictionary<Guid, PlayerState>? PlayerState { get; set; }

        public IDictionary<Guid, AvatarType?> Avatars { get; }

        public IDictionary<Guid, CardType[]>? PlayerInventories { get; set; }

        public Guid? PlayerInTokyoCity { get; set; }

        public Guid? PlayerInTokyoBay { get; set; }

        public DiceRoll[] DiceRoll { get; set; } = new DiceRoll[0];

        public Card[] CardStore { get; set; } = new Card[0];

        public CardType[] DiscardedCards { get; set; } = new CardType[0];
    }
}