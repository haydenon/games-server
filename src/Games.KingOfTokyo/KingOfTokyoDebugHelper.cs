using System;
using System.Linq;
using System.Text.Json;
using Games.KingOfTokyo.State;
using Games.Shared;
using LanguageExt;
using static LanguageExt.Prelude;
using static Games.Shared.Parsing;
using Games.KingOfTokyo.Actions;
using Games.KingOfTokyo.State.Models;

namespace Games.KingOfTokyo
{
    public static class KingOfTokyoDebugHelper
    {
        private static string[] ActionTypes =
        {
            "DoRoll"
        };

        public static bool IsAction(JsonElement state)
            => GetStringProperty(state, "action").Match(action => ActionTypes.Any(a => a == action), () => false);

        public static EitherAsync<string, IAction<KingOfTokyoGame>> CreateAction(JsonElement state, KingOfTokyoPlayer player)
        {
            var actionProp = GetStringProperty(state, "action").ToEitherAsync("Action property not set");
            return actionProp.Bind(action => action switch
            {
                "DoRoll" => CreateRollAction(state, player),
                _ => "Invalid action set"
            });
        }

        private static EitherAsync<string, IAction<KingOfTokyoGame>> CreateRollAction(JsonElement state, KingOfTokyoPlayer player)
            => from diceRollProp in GetProperty(state, "diceRoll").ToAsync()
               from diceRoll in ToEnumArray<DiceRollType>(diceRollProp, "diceRoll").ToAsync()
               select (IAction<KingOfTokyoGame>)new RollDiceDebugAction(player, diceRoll);

        public static EitherAsync<string, KingOfTokyoGame> SetState(KingOfTokyoGame currentGame, JsonElement state, Func<Guid, Guid> getPlayerId)
        {
            var stateProp = GetStringProperty(state, "state").ToEitherAsync("State property not set");
            return stateProp.Bind(prop => prop switch
            {
                "PlayerRollStart" => SetPlayerRollStart(currentGame, state, getPlayerId),
                _ => "Invalid state set"
            });
        }

        private static EitherAsync<string, KingOfTokyoGame> SetPlayerRollStart(KingOfTokyoGame currentGame, JsonElement state, Func<Guid, Guid> getPlayerId)
        {
            var playerIdProp = state.GetProperty("player");
            if (playerIdProp.ValueKind != JsonValueKind.String)
            {
                return "Must provide a string value for property 'player'";
            }

            GetStringProperty(state, "tokyoCity").Match(
                userId => { currentGame.PlayerInTokyoCity = getPlayerId(Guid.Parse(userId)); },
                () => { currentGame.PlayerInTokyoCity = default(Guid?); });

            GetStringProperty(state, "tokyoBay").Match(
                userId => { currentGame.PlayerInTokyoBay = getPlayerId(Guid.Parse(userId)); },
                () => { currentGame.PlayerInTokyoBay = default(Guid?); });

            GetProperty(state, "playerState").Do(
                playerStateElement =>
                {
                    if (playerStateElement.ValueKind != JsonValueKind.Object)
                    {
                        return;
                    }

                    var props = playerStateElement.EnumerateObject().ToArray();
                    foreach (var prop in props)
                    {
                        if (!Guid.TryParse(prop.Name, out var userId))
                        {
                            continue;
                        }

                        SetPlayerState(currentGame, getPlayerId(userId), prop.Value);
                    }
                });

            GetProperty(state, "playerInventories").Do(
                playerInventoriesElement =>
                {
                    var props = playerInventoriesElement.EnumerateObject().ToArray();
                    foreach (var prop in props)
                    {
                        if (!Guid.TryParse(prop.Name, out var userId))
                        {
                            continue;
                        }

                        if (prop.Value.ValueKind != JsonValueKind.Array)
                        {
                            return;
                        }

                        var inventory = ToEnumArray<CardType>(prop.Value, "inventory");
                        inventory.Do(inv =>
                        {
                            var id = getPlayerId(userId);
                            if (currentGame.PlayerInventories != null && currentGame.PlayerInventories.ContainsKey(id))
                            {
                                currentGame.PlayerInventories[id] = inv;
                            }
                        });
                    };
                });

            var userIdString = playerIdProp.GetString();
            if (!Guid.TryParse(userIdString, out var userId))
            {
                return "No player id";
            }

            currentGame.State = new PlayerRollStartState
            {
                Player = getPlayerId(userId)
            };

            var diceCount = 6 + (currentGame.PlayerInventories?[getPlayerId(userId)].Count(c => c == CardType.ExtraHead) ?? 0);
            currentGame.DiceRoll = Enumerable.Range(0, diceCount).Select(_ => new DiceRoll
            {
                DiceRollType = DiceRollType.One,
                WasPreserved = false
            }).ToArray();

            return currentGame;
        }

        private static EitherAsync<string, KingOfTokyoGame> SetPlayerState(KingOfTokyoGame currentGame, Guid id, JsonElement playerState)
        {
            var state = currentGame.PlayerState?[id];
            if (state == null)
            {
                return currentGame;
            }

            var hp = GetProperty(playerState, "hitPoints");
            var vp = GetProperty(playerState, "victoryPoints");
            var ep = GetProperty(playerState, "energy");
            hp.Do(hpElement => state.HitPoints = hpElement.GetInt32());
            vp.Do(vpElement => state.VictoryPoints = vpElement.GetInt32());
            ep.Do(epElement => state.Energy = epElement.GetInt32());

            return currentGame;
        }
    }
}