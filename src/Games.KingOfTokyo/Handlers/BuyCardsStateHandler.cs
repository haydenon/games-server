using System;
using System.Collections.Generic;
using System.Linq;
using Games.KingOfTokyo.Actions;
using Games.KingOfTokyo.Cards;
using Games.KingOfTokyo.Events;
using Games.KingOfTokyo.State;
using Games.KingOfTokyo.State.Models;
using Games.KingOfTokyo.Updates;
using Games.Shared;
using Games.Shared.StateUpdates;
using LanguageExt;
using static LanguageExt.Prelude;

namespace Games.KingOfTokyo.Handlers
{
  public class BuyCardsStateHandler : IActionHandler<KingOfTokyoGame>
  {
    private const int DiscardCost = 2;

    public bool CanHandle(KingOfTokyoGame game)
        => game.State is BuyCardsState;

    public ActionHandleResult<KingOfTokyoGame> HandleAction(
        IStateUpdater<KingOfTokyoGame> updater,
        KingOfTokyoGame game,
        IAction<KingOfTokyoGame> action)
    => (action, game.State) switch
    {
      (BuyCardAction buyCard, BuyCardsState buyCardsState) => HandleBuy(updater, game, buyCard),
      (DiscardCardsAction discardCards, BuyCardsState buyCardsState) => HandleDiscard(updater, game, discardCards),
      (FinishCardBuying finishBuy, BuyCardsState buyCardsState) => HandleFinish(updater, game, finishBuy),
      _ => new ActionHandleResult<KingOfTokyoGame>(game, action.ExecutingPlayer.Id)
    };

    public Either<string, Unit> ValidateAction(KingOfTokyoGame game, IAction<KingOfTokyoGame> action)
        => (action, game.State) switch
        {
          (BuyCardAction buyCard, BuyCardsState buyCardsState) => ValidateBuy(game, buyCard, buyCardsState),
          (DiscardCardsAction discardCards, BuyCardsState buyCardsState) => ValidateDiscard(game, discardCards, buyCardsState),
          (FinishCardBuying finishBuy, BuyCardsState buyCardsState) => ValidateFinish(game, finishBuy, buyCardsState),
          _ => $"Cannot perform {action.Name} action at the moment."
        };

    private ActionHandleResult<KingOfTokyoGame> HandleBuy(
        IStateUpdater<KingOfTokyoGame> updater,
        KingOfTokyoGame game,
        BuyCardAction buyCard)
    {
      game.MarkUpdated();
      var player = buyCard.ExecutingPlayer.Id;
      var result = new ActionHandleResult<KingOfTokyoGame>(game, player);

      var card = game.CardStore.First(c => c.CardType == buyCard.CardType);
      var index = System.Array.IndexOf(game.CardStore, card);
      if (CardFactory.RemainingCards(game) < 1)
      {
        game.DiscardedCards = new CardType[0];
        result.Append(new ResetDiscardedCardsEvent(game.LockState));
      }

      var replacementCard = CardFactory.GetUnusedCard(game);
      game.CardStore[index] = replacementCard;

      var playerState = game.PlayerState?[player];
      if (playerState != null)
      {
        playerState.Energy -= card.EnergyCost;
      }

      var buyCardEvent = new BuyCardEvent(
          game.LockState,
          buyCard.ExecutingPlayer.Id,
          card,
          index,
          replacementCard);
      result.Append(buyCardEvent);

      if (card.IsDiscard)
      {
        var discarded = new CardType[game.DiscardedCards.Length + 1];
        discarded[game.DiscardedCards.Length] = card.CardType;
        game.DiscardedCards.CopyTo(discarded, 0);
        game.DiscardedCards = discarded;

        var cardStatUpdates = CardStatChangeDefinitions.UpdatesForCardType(card.CardType);
        result.Then(updater.Update(new ActionCardStatUpdate(card.CardType, cardStatUpdates)));
        result.Then(updater.Update(new ActionDiscardCardUpdate(card.CardType)));
        result.Then(updater.Update(new TokyoDeadPlayersLeaveUpdate()));

        if (GameWinUpdater.HasEndedByCombat(game))
        {
          return result.Then(updater.Update(new EndGameByCombatUpdate()));
        }
        else if (playerState != null && playerState.IsDead)
        {
          return result.Then(updater.Update(new EndOfTurnUpdate()));
        }

        return result.Then(updater.Update(new BuyCardsUpdate()));
      }
      else
      {
        var playerInventories = game.PlayerInventories;
        var playerCards = game.PlayerInventories?[player];
        if (playerInventories == null || playerCards == null)
        {
          return result;
        }

        var inventory = new CardType[playerCards.Length + 1];
        inventory[playerCards.Length] = card.CardType;
        playerCards.CopyTo(inventory, 0);
        playerInventories[player] = inventory;

        return result.Then(updater.Update(new BuyCardsUpdate()));
      }
    }

    private Either<string, Unit> ValidateBuy(KingOfTokyoGame game, BuyCardAction buyCard, BuyCardsState buyCardsState)
    {
      var player = buyCardsState.Player;
      if (player != buyCard.ExecutingPlayer.Id)
      {
        var turnPlayer = game.Players.First(p => p.Id == player);
        return $"It is currently {turnPlayer.Name}'s turn";
      }

      if (game.CardStore.All(c => c.CardType != buyCard.CardType))
      {
        return $"{buyCard.CardType} is not available to buy";
      }

      var playerState = game.PlayerState?[player];
      if (playerState == null)
      {
        return "Invalid player";
      }

      var energy = playerState.Energy;
      var card = game.CardStore.First(c => c.CardType == buyCard.CardType);
      var cost = card.EnergyCost;
      if (cost > energy)
      {
        return $"You do not have enough energy to buy {card.DisplayName}";
      }

      return unit;
    }

    private ActionHandleResult<KingOfTokyoGame> HandleDiscard(
        IStateUpdater<KingOfTokyoGame> updater,
        KingOfTokyoGame game,
        DiscardCardsAction discardCards)
    {
      game.MarkUpdated();
      var player = discardCards.ExecutingPlayer.Id;
      var result = new ActionHandleResult<KingOfTokyoGame>(game, player);

      var remainingCards = CardFactory.RemainingCards(game);
      var cards = new List<Card>();
      cards.AddRange(CardFactory.GetUnusedCards(game, Math.Min(remainingCards, 3)));
      if (remainingCards < 3)
      {
        game.DiscardedCards = new CardType[0];
        result.Append(new ResetDiscardedCardsEvent(game.LockState));
        cards.AddRange(CardFactory.GetUnusedCards(game, 3 - remainingCards, cards.Select(c => c.CardType).ToArray()));
      }

      var discarded = new CardType[game.DiscardedCards.Length + 3];
      game.DiscardedCards.CopyTo(discarded, 0);
      game.CardStore.Select(c => c.CardType).ToArray().CopyTo(discarded, game.DiscardedCards.Length);
      game.DiscardedCards = discarded;
      game.CardStore = cards.ToArray();

      var playerState = game.PlayerState?[player];
      if (playerState != null)
      {
        playerState.Energy -= DiscardCost;
      }

      result.Append(new DiscardCardsEvent(game.LockState, game.CardStore));

      return result.Then(updater.Update(new BuyCardsUpdate()));
    }

    private Either<string, Unit> ValidateDiscard(KingOfTokyoGame game, DiscardCardsAction buyCard, BuyCardsState buyCardsState)
    {
      var player = buyCardsState.Player;
      if (player != buyCard.ExecutingPlayer.Id)
      {
        var turnPlayer = game.Players.First(p => p.Id == player);
        return $"It is currently {turnPlayer.Name}'s turn";
      }

      var playerState = game.PlayerState?[player];
      if (playerState == null)
      {
        return "Invalid player";
      }

      var energy = playerState.Energy;
      if (energy < DiscardCost)
      {
        return $"You do not have enough energy to discard the card deck";
      }

      return unit;
    }

    private ActionHandleResult<KingOfTokyoGame> HandleFinish(
        IStateUpdater<KingOfTokyoGame> updater,
        KingOfTokyoGame game,
        FinishCardBuying finishBuy)
        => new ActionHandleResult<KingOfTokyoGame>(game, finishBuy.ExecutingPlayer.Id)
            .Then(updater.Update(new EndOfTurnUpdate()));

    private Either<string, Unit> ValidateFinish(KingOfTokyoGame game, FinishCardBuying finishBuy, BuyCardsState buyCardsState)
    {
      var player = buyCardsState.Player;
      if (player != finishBuy.ExecutingPlayer.Id)
      {
        var turnPlayer = game.Players.First(p => p.Id == player);
        return $"It is currently {turnPlayer.Name}'s turn";
      }

      return unit;
    }
  }
}