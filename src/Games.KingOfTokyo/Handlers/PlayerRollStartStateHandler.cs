using System;
using System.Linq;
using Games.KingOfTokyo.Actions;
using Games.KingOfTokyo.Events;
using Games.KingOfTokyo.State;
using Games.KingOfTokyo.State.Models;
using Games.Shared;
using Games.Shared.StateUpdates;
using LanguageExt;
using static LanguageExt.Prelude;

namespace Games.KingOfTokyo.Handlers
{
    public class PlayerRollStartStateHandler : IActionHandler<KingOfTokyoGame>
    {
        public bool CanHandle(KingOfTokyoGame game)
            => game.State is PlayerRollStartState;

        public ActionHandleResult<KingOfTokyoGame> HandleAction(
            IStateUpdater<KingOfTokyoGame> updater,
            KingOfTokyoGame game,
            IAction<KingOfTokyoGame> action)
        {
            if (!(game.State is PlayerRollStartState playerTurnState))
            {
                return new ActionHandleResult<KingOfTokyoGame>(game, action.ExecutingPlayer.Id);
            }

            game.MarkUpdated();

            var diceRoll = GetDiceRoll(action, DiceCount(game, action.ExecutingPlayer.Id));
            var diceRollState = new PlayerRollState
            {
                Player = playerTurnState.Player,
                RollNumber = 1
            };
            game.State = diceRollState;
            game.DiceRoll = diceRoll;

            var diceRollEvent = new DiceRollEvent(game.LockState, diceRoll);
            return new ActionHandleResult<KingOfTokyoGame>(game, action.ExecutingPlayer.Id, diceRollEvent);
        }

        private static DiceRoll[] GetDiceRoll(IAction<KingOfTokyoGame> action, int diceCount)
            => action switch
            {
                RollDiceDebugAction debugAction => debugAction.DiceRoll.Select(FromDiceType).ToArray(),
                _ => CreateRoll(diceCount)
            };

        private static DiceRoll[] CreateRoll(int diceCount)
        {
            var rnd = new Random();
            var diceRoll = new DiceRollType[diceCount];
            for (var i = 0; i < diceCount; i++)
            {
                diceRoll[i] = (DiceRollType)rnd.Next(6);
            }
            return diceRoll.Select(FromDiceType).ToArray();
        }

        private static DiceRoll FromDiceType(DiceRollType type)
            => new DiceRoll { DiceRollType = type, WasPreserved = false };

        public Either<string, Unit> ValidateAction(KingOfTokyoGame game, IAction<KingOfTokyoGame> action)
        {
            if (!(action is RollDiceAction) && !(action is RollDiceDebugAction))
            {
                return $"Cannot perform {action.Name} action at the moment.";
            }

            if (!(game.State is PlayerRollStartState playerTurnState))
            {
                return "You are currently not able to roll dice.";
            }

            if (action.ExecutingPlayer.Id != playerTurnState.Player)
            {
                var turnPlayer = game.Players.FirstOrDefault(p => p.Id == playerTurnState.Player);
                return $"It is currently {turnPlayer?.Name}'s turn to roll";
            }

            return unit;
        }

        private int DiceCount(KingOfTokyoGame game, Guid player) => 6 + game.CardCount(player, CardType.ExtraHead);
    }
}