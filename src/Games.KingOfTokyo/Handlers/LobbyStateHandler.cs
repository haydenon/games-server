using System;
using System.Collections.Generic;
using System.Linq;
using Games.KingOfTokyo.Actions;
using Games.KingOfTokyo.Cards;
using Games.KingOfTokyo.Events;
using Games.KingOfTokyo.State;
using Games.KingOfTokyo.State.Models;
using Games.KingOfTokyo.State.Player;
using Games.KingOfTokyo.View;
using Games.Shared;
using Games.Shared.StateUpdates;
using LanguageExt;

using static LanguageExt.Prelude;

namespace Games.KingOfTokyo.Handlers
{
    public class LobbyStateHandler : IActionHandler<KingOfTokyoGame>
    {
        public bool CanHandle(KingOfTokyoGame game)
            => game.State is LobbyState;

        public ActionHandleResult<KingOfTokyoGame> HandleAction(
            IStateUpdater<KingOfTokyoGame> updater,
            KingOfTokyoGame game,
            IAction<KingOfTokyoGame> action)
            => action switch
            {
                JoinAction join => HandleJoin(game, join),
                ChangeAvatarAction changeAvatar => HandleChangeAvatar(game, changeAvatar),
                StartGameAction start => HandleStart(game, start),
                _ => new ActionHandleResult<KingOfTokyoGame>(game, action.ExecutingPlayer.Id)
            };

        public Either<string, Unit> ValidateAction(KingOfTokyoGame game, IAction<KingOfTokyoGame> action)
            => action switch
            {
                JoinAction join => ValidateJoin(game, join),
                ChangeAvatarAction changeAvatar => ValidateChangeAvatar(game, changeAvatar),
                StartGameAction start => ValidateStart(game, start),
                _ => $"Cannot currently perform {action.Name} actions."
            };

        private ActionHandleResult<KingOfTokyoGame> HandleJoin(KingOfTokyoGame game, JoinAction action)
        {
            game.MarkUpdated();

            var player = action.ExecutingPlayer;
            game.Players.Add(player);
            var index = game.Players.IndexOf(player);

            var joinEvent = new PlayerJoinEvent(game.LockState, KingOfTokyoPlayerView.FromPlayer(player, null), index);
            return new ActionHandleResult<KingOfTokyoGame>(game, action.ExecutingPlayer.Id, joinEvent);
        }

        private ActionHandleResult<KingOfTokyoGame> HandleChangeAvatar(KingOfTokyoGame game, ChangeAvatarAction action)
        {
            game.MarkUpdated();

            var player = action.ExecutingPlayer;

            game.Avatars[player.Id] = action.Avatar;

            var joinEvent = new ChangeAvatarEvent(game.LockState, player.Id, action.Avatar);
            return new ActionHandleResult<KingOfTokyoGame>(game, action.ExecutingPlayer.Id, joinEvent);
        }

        private ActionHandleResult<KingOfTokyoGame> HandleStart(KingOfTokyoGame game, StartGameAction start)
        {
            game.MarkUpdated();

            var events = new List<IGameEvent<KingOfTokyoGame>>();
            var noAvatarPlayers = game.Players.Where(p => !game.Avatars.ContainsKey(p.Id) || !game.Avatars[p.Id].HasValue);
            if (noAvatarPlayers.Any())
            {
                events.AddRange(AssignAvatars(game, noAvatarPlayers.ToList()));
            }

            Random rnd = new Random();
            var startingPlayerIndex = rnd.Next(game.Players.Count);
            var startingPlayer = game.Players[startingPlayerIndex];

            game.State = new PlayerRollStartState { Player = startingPlayer.Id };
            game.DiceRoll = Enumerable.Range(0, 6).Select(_ => new DiceRoll
            {
                DiceRollType = DiceRollType.One,
                WasPreserved = false
            }).ToArray();
            game.CardStore = CardFactory.GetUnusedCards(game, 3);
            game.PlayerState = game.Players.ToDictionary(p => p.Id, _ => PlayerState.CreateDefaultState());
            game.PlayerInventories = game.Players.ToDictionary(p => p.Id, _ => new CardType[0]);

            events.Add(new GameStartEvent(game.LockState, startingPlayerIndex, game.CardStore));
            events.Add(new PlayerRollStartEvent(game.LockState, startingPlayer.Id));
            return new ActionHandleResult<KingOfTokyoGame>(game, start.ExecutingPlayer.Id, events.ToArray());
        }

        private ChangeAvatarEvent[] AssignAvatars(KingOfTokyoGame game, IList<IPlayer<KingOfTokyoGame>> players)
        {
            var allAvatars = new[] {
                AvatarType.Alienoid,
                AvatarType.CyberKitty,
                AvatarType.Gigazaur,
                AvatarType.MekaDragon,
                AvatarType.SpacePenguin,
                AvatarType.TheKing
            };

            var unused = allAvatars.Where(a => !game.Avatars.Values.Any(av => av == a)).ToList();
            Random rnd = new Random();
            var startingAvatarIndex = rnd.Next(unused.Count());

            var events = new ChangeAvatarEvent[players.Count()];
            for (var i = 0; i < players.Count; i++)
            {
                var player = players[i];
                var avatar = unused[(startingAvatarIndex + i) % unused.Count()];
                game.Avatars[player.Id] = avatar;
                events[i] = new ChangeAvatarEvent(game.LockState, player.Id, avatar);
            }

            return events;
        }

        private Either<string, Unit> ValidateJoin(KingOfTokyoGame game, JoinAction action)
        {
            if (game.Players.Any(p => p.Id == action.ExecutingPlayer.Id))
            {
                return "You have already joined the game.";
            }

            if (action is JoinAction && game.Players.Count() >= KingOfTokyoGame.MaxPlayers)
            {
                return $"Game is already full. Maximum players is {KingOfTokyoGame.MaxPlayers}.";
            }

            if (string.IsNullOrWhiteSpace(action.ExecutingPlayer.Name))
            {
                return "Must provide a valid player name.";
            }

            return unit;
        }

        private Either<string, Unit> ValidateChangeAvatar(KingOfTokyoGame game, ChangeAvatarAction action)
        {
            if (game.Players.All(p => p.Id != action.ExecutingPlayer.Id))
            {
                return "You need to join the game to be able to change your avatar.";
            }

            if (action.Avatar != null && game.Avatars.Any((kvp) => kvp.Value == action.Avatar && kvp.Key != action.ExecutingPlayer.Id))
            {
                return "You cannot choose an avatar that is already in use.";
            }

            return unit;
        }

        private Either<string, Unit> ValidateStart(KingOfTokyoGame game, StartGameAction action)
        {
            if (action.ExecutingPlayer.Id != game.CreatorPlayerId)
            {
                return "The game can only be started by the player who created it.";
            }

            if (game.Players.Count < 2)
            {
                return "You need at least two players to start the game.";
            }

            return unit;
        }
    }
}