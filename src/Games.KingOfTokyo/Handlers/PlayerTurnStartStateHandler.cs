using System;
using System.Linq;
using Games.KingOfTokyo.Actions;
using Games.KingOfTokyo.Events;
using Games.KingOfTokyo.State;
using Games.KingOfTokyo.State.Models;
using Games.Shared;
using Games.Shared.StateUpdates;
using LanguageExt;
using static LanguageExt.Prelude;

namespace Games.KingOfTokyo.Handlers
{
    public class PlayerTurnStartStateHandler : IActionHandler<KingOfTokyoGame>
    {
        public bool CanHandle(KingOfTokyoGame game)
            => game.State is PlayerTurnStartState;

        public ActionHandleResult<KingOfTokyoGame> HandleAction(
            IStateUpdater<KingOfTokyoGame> updater,
            KingOfTokyoGame game,
            IAction<KingOfTokyoGame> action)
        {
            game.MarkUpdated();
            game.State = new PlayerRollStartState { Player = action.ExecutingPlayer.Id };
            game.DiceRoll = Enumerable.Range(0, DiceCount(game, action.ExecutingPlayer.Id)).Select(_ => new DiceRoll
            {
                DiceRollType = DiceRollType.One,
                WasPreserved = false
            }).ToArray();

            var playerRollStartEvent = new PlayerRollStartEvent(game.LockState, action.ExecutingPlayer.Id);
            return new ActionHandleResult<KingOfTokyoGame>(game, action.ExecutingPlayer.Id, playerRollStartEvent);
        }

        public Either<string, Unit> ValidateAction(KingOfTokyoGame game, IAction<KingOfTokyoGame> action)
        {
            if (!(action is StartTurnAction))
            {
                return $"Cannot perform {action.Name} action at the moment.";
            }

            if (!(game.State is PlayerTurnStartState playerTurnState))
            {
                return "You are currently not able to start your turn.";
            }

            if (action.ExecutingPlayer.Id != playerTurnState.Player)
            {
                var turnPlayer = game.Players.FirstOrDefault(p => p.Id == playerTurnState.Player);
                return $"It is currently {turnPlayer?.Name}'s turn to roll";
            }

            return unit;
        }

        private int DiceCount(KingOfTokyoGame game, Guid player) => 6 + game.CardCount(player, CardType.ExtraHead);
    }
}