using System;
using System.Linq;
using Games.KingOfTokyo.Actions;
using Games.KingOfTokyo.Events;
using Games.KingOfTokyo.State;
using Games.KingOfTokyo.Updates;
using Games.Shared;
using Games.Shared.StateUpdates;
using LanguageExt;
using static LanguageExt.Prelude;

namespace Games.KingOfTokyo.Handlers
{
    public class LeaveTokyoStateHandler : IActionHandler<KingOfTokyoGame>
    {
        public bool CanHandle(KingOfTokyoGame game)
            => game.State is LeaveTokyoState;

        public ActionHandleResult<KingOfTokyoGame> HandleAction(
            IStateUpdater<KingOfTokyoGame> updater,
            KingOfTokyoGame game,
            IAction<KingOfTokyoGame> action)
        => (action, game.State) switch
        {
            (LeaveTokyoAction leaveAction, LeaveTokyoState leaveState) => HandleLeave(updater, game, leaveAction, leaveState),
            _ => new ActionHandleResult<KingOfTokyoGame>(game, action.ExecutingPlayer.Id)
        };

        public Either<string, Unit> ValidateAction(KingOfTokyoGame game, IAction<KingOfTokyoGame> action)
            => (action, game.State) switch
            {
                (LeaveTokyoAction leaveAction, LeaveTokyoState leaveState) => ValidateLeave(leaveAction, leaveState),
                _ => $"Cannot perform {action.Name} action at the moment."
            };

        private ActionHandleResult<KingOfTokyoGame> HandleLeave(
            IStateUpdater<KingOfTokyoGame> updater,
            KingOfTokyoGame game,
            LeaveTokyoAction leaveAction,
            LeaveTokyoState leaveState)
        {
            game.MarkUpdated();
            var leavePlayer = leaveAction.ExecutingPlayer.Id;

            var result = new ActionHandleResult<KingOfTokyoGame>(game, leaveState.OriginalPlayer);
            var isLeaving = leaveAction.LeaveTokyo;
            var leaveEvent = new LeaveTokyoEvent(game.LockState, leavePlayer, isLeaving);
            result.Append(leaveEvent);
            if (isLeaving && game.PlayerInTokyoCity == leavePlayer)
            {
                game.PlayerInTokyoCity = null;
            }
            else if (isLeaving && game.PlayerInTokyoBay == leavePlayer)
            {
                game.PlayerInTokyoBay = null;
            }

            if (leaveState.AwaitingConfirmationPlayers != null &&
                leaveState.AwaitingConfirmationPlayers.Any(p => p != leavePlayer))
            {
                game.State = new LeaveTokyoState
                {
                    OriginalPlayer = leaveState.OriginalPlayer,
                    AwaitingConfirmationPlayers = leaveState.AwaitingConfirmationPlayers.Where(p => p != leavePlayer).ToArray(),
                    NextState = leaveState.NextState
                };
                return result;
            }

            // If the player has died, then go straight to end of turn
            if (game.IsPlayerDead(leaveState.OriginalPlayer))
            {
                return result.Then(updater.Update(new EndOfTurnUpdate()));
            }

            return leaveState.NextState switch
            {
                LeaveTokyoNextState.BuyCard => result.Then(updater.Update(new BuyCardsUpdate())),
                LeaveTokyoNextState.EnterTokyo => result.Then(updater.Update(new EnterTokyoUpdate())),
                _ => throw new NotImplementedException()
            };
        }

        private Either<string, Unit> ValidateLeave(LeaveTokyoAction leaveAction, LeaveTokyoState leaveState)
        {
            if (leaveState.AwaitingConfirmationPlayers == null ||
                !leaveState.AwaitingConfirmationPlayers.Any(p => p == leaveAction.ExecutingPlayer.Id))
            {
                return "You are not able to choose to stay or leave Tokyo";
            }

            return unit;
        }
    }
}