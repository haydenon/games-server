using System;
using System.Linq;
using Games.KingOfTokyo.Actions;
using Games.KingOfTokyo.Events;
using Games.KingOfTokyo.State;
using Games.KingOfTokyo.State.Models;
using Games.KingOfTokyo.Updates;
using Games.Shared;
using Games.Shared.StateUpdates;
using LanguageExt;
using static LanguageExt.Prelude;

namespace Games.KingOfTokyo.Handlers
{
    public class PlayerRollStateHandler : IActionHandler<KingOfTokyoGame>
    {
        public bool CanHandle(KingOfTokyoGame game)
            => game.State is PlayerRollState;

        public ActionHandleResult<KingOfTokyoGame> HandleAction(
            IStateUpdater<KingOfTokyoGame> updater,
            KingOfTokyoGame game,
            IAction<KingOfTokyoGame> action)
            => (action, game.State) switch
            {
                (RollDiceAction rollDiceAction, PlayerRollState rollState) => HandleRoll(updater, game, rollState, rollDiceAction),
                (RollDiceDebugAction rollDiceDebugAction, PlayerRollState rollState) => HandleRoll(updater, game, rollState, rollDiceDebugAction),
                (AcceptDiceAction acceptRollAction, PlayerRollState rollState) => HandleAccept(updater, game, rollState, acceptRollAction),
                _ => new ActionHandleResult<KingOfTokyoGame>(game, action.ExecutingPlayer.Id)
            };

        public Either<string, Unit> ValidateAction(KingOfTokyoGame game, IAction<KingOfTokyoGame> action)
            => (action, game.State) switch
            {
                (RollDiceAction rollDiceAction, PlayerRollState rollState) => ValidateRoll(game, rollState, rollDiceAction),
                (RollDiceDebugAction rollDiceDebugAction, PlayerRollState rollState) => ValidateRoll(game, rollState, rollDiceDebugAction),
                (AcceptDiceAction acceptRollAction, PlayerRollState rollState) => ValidateAccept(game, rollState, acceptRollAction),
                _ => $"Cannot perform {action.Name} action at the moment."
            };

        private ActionHandleResult<KingOfTokyoGame> HandleRoll(
            IStateUpdater<KingOfTokyoGame> updater,
            KingOfTokyoGame game,
            PlayerRollState rollState,
            IAction<KingOfTokyoGame> action)
        {
            game.MarkUpdated();

            var diceCount = DiceCount(game, action.ExecutingPlayer.Id);
            var diceRoll = GetDiceRoll(game, action, diceCount);
            var diceRollEvent = new DiceRollEvent(game.LockState, diceRoll);
            game.DiceRoll = diceRoll;
            if (rollState.RollNumber + 1 >= MaxRolls(game, rollState.Player))
            {
                var result = new ActionHandleResult<KingOfTokyoGame>(game, action.ExecutingPlayer.Id, diceRollEvent);
                return result.Then(updater.Update(new ResolveDiceUpdate()));
            }

            var diceRollState = new PlayerRollState
            {
                Player = rollState.Player,
                RollNumber = rollState.RollNumber + 1
            };
            game.State = diceRollState;

            return new ActionHandleResult<KingOfTokyoGame>(game, action.ExecutingPlayer.Id, diceRollEvent);
        }

        private static DiceRoll[] GetDiceRoll(KingOfTokyoGame game, IAction<KingOfTokyoGame> action, int diceCount)
            => action switch
            {
                RollDiceDebugAction debugAction => debugAction.DiceRoll.Select(dr => CreateDiceRoll(dr, false)).ToArray(),
                RollDiceAction rollAction => CreateRoll(game, rollAction, diceCount),
                _ => throw new NotImplementedException()
            };

        private static DiceRoll[] CreateRoll(KingOfTokyoGame game, RollDiceAction action, int diceCount)
        {
            var diceToKeep = action.DiceToKeep;
            bool ShouldKeep(int i) => diceToKeep != null && diceToKeep.Length > i && diceToKeep[i];
            var rnd = new Random();
            var diceRoll = new DiceRoll[diceCount];
            for (var i = 0; i < diceCount; i++)
            {
                diceRoll[i] = ShouldKeep(i) ?
                    CreateDiceRoll(game.DiceRoll[i].DiceRollType, true) :
                    CreateDiceRoll((DiceRollType)rnd.Next(6), false);
            }

            return diceRoll;
        }

        private static DiceRoll CreateDiceRoll(DiceRollType type, bool wasPreserved)
            => new DiceRoll
            {
                DiceRollType = type,
                WasPreserved = wasPreserved
            };

        private Either<string, Unit> ValidateRoll(KingOfTokyoGame game, PlayerRollState rollState, IAction<KingOfTokyoGame> action)
        {

            if (action.ExecutingPlayer.Id != rollState.Player)
            {
                var turnPlayer = game.Players.FirstOrDefault(p => p.Id == rollState.Player);
                return $"It is currently {turnPlayer?.Name}'s turn";
            }

            if (rollState.RollNumber >= MaxRolls(game, rollState.Player))
            {
                return $"You have already rolled {MaxRolls(game, rollState.Player)} times";
            }

            return unit;
        }

        private ActionHandleResult<KingOfTokyoGame> HandleAccept(
            IStateUpdater<KingOfTokyoGame> updater,
            KingOfTokyoGame game,
            PlayerRollState rollState,
            AcceptDiceAction action)
        {
            game.MarkUpdated();

            var result = new ActionHandleResult<KingOfTokyoGame>(game, action.ExecutingPlayer.Id, new AcceptDiceEvent(game.LockState));
            return result.Then(updater.Update(new ResolveDiceUpdate()));
        }

        private Either<string, Unit> ValidateAccept(KingOfTokyoGame game, PlayerRollState rollState, AcceptDiceAction action)
        {
            if (action.ExecutingPlayer.Id != rollState.Player)
            {
                var turnPlayer = game.Players.FirstOrDefault(p => p.Id == rollState.Player);
                return $"It is currently {turnPlayer?.Name}'s turn";
            }

            return unit;
        }

        private int DiceCount(KingOfTokyoGame game, Guid player) => 6 + game.CardCount(player, CardType.ExtraHead);

        private int MaxRolls(KingOfTokyoGame game, Guid player) => game.HasCard(player, CardType.GiantBrain) ? 4 : 3;
    }
}