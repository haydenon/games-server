using System;
using Games.KingOfTokyo.State.Player;
using Games.Shared;

namespace Games.KingOfTokyo.View
{
    public class KingOfTokyoPlayerView
    {
        public Guid Id { get; set; }

        public string? Name { get; set; }

        public string? Avatar { get; set; }

        public static KingOfTokyoPlayerView FromPlayer(IPlayer<KingOfTokyoGame> player, AvatarType? avatar)
            => new KingOfTokyoPlayerView
            {
                Id = player.Id,
                Name = player.Name,
                Avatar = avatar?.ToString()
            };
    }
}