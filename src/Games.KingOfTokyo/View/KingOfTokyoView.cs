using System;
using System.Collections.Generic;
using Games.KingOfTokyo.View.Models;
using Games.Shared;

namespace Games.KingOfTokyo.View
{
    public class KingOfTokyoView : IGameView
    {
        public string? CreatorPlayerId { get; set; }
        public string? AccessingPlayerId { get; set; }
        public int LockState { get; set; }
        public object? State { get; set; }
        public IList<KingOfTokyoPlayerView>? Players { get; set; }
        public IDictionary<string, PlayerStateView>? PlayerState { get; set; }
        public IDictionary<string, string[]>? PlayerInventories { get; set; }
        public string? PlayerInTokyoCity { get; set; }
        public string? PlayerInTokyoBay { get; set; }

        public DiceRollView[]? DiceRoll { get; set; }

        public KingOfTokyoCardView[]? CardStore { get; set; }

        public string[]? DiscardedCards { get; set; }

    }
}