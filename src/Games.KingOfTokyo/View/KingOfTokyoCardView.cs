using Games.KingOfTokyo.State.Models;

namespace Games.KingOfTokyo.View
{
    public class KingOfTokyoCardView
    {
        public string? CardType { get; set; }
        public int EnergyCost { get; set; }
        public bool IsDiscard { get; set; }

        public static KingOfTokyoCardView FromCard(Card card)
            => new KingOfTokyoCardView
            {
                CardType = card.CardType.ToString(),
                EnergyCost = card.EnergyCost,
                IsDiscard = card.IsDiscard
            };
    }
}