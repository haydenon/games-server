using Games.KingOfTokyo.State.Player;

namespace Games.KingOfTokyo.View
{
    public class PlayerStateView
    {
        public int HitPoints { get; set; }

        public int VictoryPoints { get; set; }

        public int Energy { get; set; }

        public static PlayerStateView FromState(PlayerState state)
            => new PlayerStateView
            {
                HitPoints = state.HitPoints,
                VictoryPoints = state.VictoryPoints,
                Energy = state.Energy
            };
    }
}