using Games.KingOfTokyo.State;

namespace Games.KingOfTokyo.View.State
{
    public class PlayerTurnStartStateView : IKingOfTokyoStateView
    {
        public string State => "PlayerTurnStart";
        public string? Player { get; set; }

        public static PlayerTurnStartStateView FromState(PlayerTurnStartState state)
            => new PlayerTurnStartStateView { Player = state.Player.ToString() };
    }
}