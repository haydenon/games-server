using System;

namespace Games.KingOfTokyo.View.State
{
    public class SimplePlayerStateView : IKingOfTokyoStateView
    {
        public SimplePlayerStateView(string state, Guid playerId)
        {
            State = state;
            Player = playerId.ToString();
        }

        public string Player { get; }
        public string State { get; }
    }
}