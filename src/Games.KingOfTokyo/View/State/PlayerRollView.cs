using Games.KingOfTokyo.State;

namespace Games.KingOfTokyo.View.State
{
    public class PlayerRollView : IKingOfTokyoStateView
    {
        public string State => "PlayerRoll";
        public string? Player { get; set; }
        public int RollNumber { get; set; }

        public static PlayerRollView FromState(PlayerRollState state)
            => new PlayerRollView
            {
                Player = state.Player.ToString(),
                RollNumber = state.RollNumber,
            };
    }
}