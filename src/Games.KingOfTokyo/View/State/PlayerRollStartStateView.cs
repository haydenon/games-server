using Games.KingOfTokyo.State;

namespace Games.KingOfTokyo.View.State
{
    public class PlayerRollStartStateView : IKingOfTokyoStateView
    {
        public string State => "PlayerRollStart";
        public string? Player { get; set; }

        public static PlayerRollStartStateView FromState(PlayerRollStartState state)
            => new PlayerRollStartStateView { Player = state.Player.ToString() };
    }
}