namespace Games.KingOfTokyo.View
{
    public interface IKingOfTokyoStateView
    {
        public string State { get; }
    }
}