namespace Games.KingOfTokyo.View.State
{
    public class LobbyStateView : IKingOfTokyoStateView
    {
        public string State => "Lobby";
    }
}