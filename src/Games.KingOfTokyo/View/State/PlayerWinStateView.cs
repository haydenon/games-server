using Games.KingOfTokyo.State;

namespace Games.KingOfTokyo.View.State
{
    public class PlayerWinStateView : IKingOfTokyoStateView
    {
        public string State => "GameEnd";
        public string? WinningPlayer { get; set; }
        public bool IsTie { get; set; }

        public static PlayerWinStateView FromState(PlayerWinState state)
            => new PlayerWinStateView
            {
                WinningPlayer = state.Player.ToString(),
                IsTie = !state.Player.HasValue
            };
    }
}