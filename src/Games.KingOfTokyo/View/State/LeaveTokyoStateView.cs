using System.Linq;
using Games.KingOfTokyo.State;

namespace Games.KingOfTokyo.View.State
{
    public class LeaveTokyoStateView : IKingOfTokyoStateView
    {
        public string State => "LeaveTokyo";

        public string[]? LeaveRequestPlayers { get; set; }

        public string? NextState { get; set; }

        public static LeaveTokyoStateView FromState(LeaveTokyoState state)
            => new LeaveTokyoStateView
            {
                LeaveRequestPlayers = state.AwaitingConfirmationPlayers?.Select(p => p.ToString()).ToArray() ?? new string[0],
                NextState = state.NextState.ToString()
            };
    }
}