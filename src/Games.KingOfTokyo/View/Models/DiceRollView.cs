using Games.KingOfTokyo.State.Models;

namespace Games.KingOfTokyo.View.Models
{
    public class DiceRollView
    {
        public string? DiceRoll { get; set; }

        public bool WasPreserved { get; set; }

        public static DiceRollView FromDiceRoll(DiceRoll diceRoll)
            => new DiceRollView
            {
                DiceRoll = diceRoll.DiceRollType.ToString(),
                WasPreserved = diceRoll.WasPreserved
            };
    }
}