using System;
using Games.Shared;

namespace Games.KingOfTokyo
{
    public class KingOfTokyoPlayer : IPlayer<KingOfTokyoGame>
    {
        public Guid Id { get; set; }

        public Guid GameId { get; set; }

        public string? Name { get; set; }
    }
}