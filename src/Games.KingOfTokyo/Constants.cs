namespace Games.KingOfTokyo
{
    public static class Constants
    {
        public const int BayEnabledPlayers = 5;
        public const int EnergyHoarderDivisor = 6;
    }
}