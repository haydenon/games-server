using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Games.Shared;
using Games.Shared.StateUpdates;

namespace Games.KingOfTokyo
{
    public static class KingOfTokyoServices
    {
        private static readonly Assembly assembly = Assembly.GetExecutingAssembly();

        public static IEnumerable<IActionHandler<KingOfTokyoGame>> GetAllHandlers()
        {
            var handlers = assembly.DefinedTypes
                .Where(ti => ti.ImplementedInterfaces.Contains(typeof(IActionHandler<KingOfTokyoGame>)))
                .Select(m => m.FullName).ToList();

            foreach (var fullName in handlers)
            {
                yield return (IActionHandler<KingOfTokyoGame>)assembly.CreateInstance(fullName!)!;
            }
        }

        public static IEnumerable<StateUpdaterInstantiator<KingOfTokyoGame>> GetAllStateUpdaterInstantiators()
        {
            var containers = assembly.DefinedTypes
                .Where(ti => ti.ImplementedInterfaces.Contains(typeof(IStateUpdaterContainer<KingOfTokyoGame>)))
                .Select(m => m.FullName).ToList();
            var builder = new StateUpdaterInstantiatorBuilder<KingOfTokyoGame>();

            foreach (var fullName in containers)
            {
                var container = (IStateUpdaterContainer<KingOfTokyoGame>)assembly.CreateInstance(fullName!)!;
                foreach (var instantiator in container.GetInstantiators(builder))
                {
                    yield return instantiator;
                }
            }
        }
    }
}