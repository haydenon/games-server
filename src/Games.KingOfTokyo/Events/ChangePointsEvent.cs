using System;
using System.Linq;
using Games.KingOfTokyo.State.Models;
using Games.Shared;

namespace Games.KingOfTokyo.Events
{
    public class ChangePointsEvent : IGameEvent<KingOfTokyoGame>
    {
        public ChangePointsEvent(int lockState, Guid executingPlayer, Guid changePlayer, int points, params CardType[] cardSources)
        {
            LockState = lockState;
            ExecutingPlayer = executingPlayer;
            ChangePlayer = changePlayer;
            Points = points;
            CardSources = cardSources.Select(c => c.ToString()).ToArray();
        }

        public string EventType => "ChangePoints";

        public Guid ExecutingPlayer { get; }

        public Guid ChangePlayer { get; }

        public int LockState { get; }

        public int Points { get; }

        public string[] CardSources { get; }
    }
}