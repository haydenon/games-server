using Games.Shared;

namespace Games.KingOfTokyo.Events
{
    public class ResetDiscardedCardsEvent : IGameEvent<KingOfTokyoGame>
    {
        public ResetDiscardedCardsEvent(int lockState)
        {
            LockState = lockState;
        }

        public string EventType => "ResetDiscardedCards";

        public int LockState { get; }
    }
}