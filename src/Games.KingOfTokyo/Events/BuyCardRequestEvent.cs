using System;
using Games.Shared;

namespace Games.KingOfTokyo.Events
{
    public class BuyCardRequestEvent : IGameEvent<KingOfTokyoGame>
    {
        public BuyCardRequestEvent(int lockState, Guid player)
        {
            LockState = lockState;
            Player = player;
        }

        public string EventType => "BuyCardRequest";

        public int LockState { get; }

        public Guid Player { get; }
    }
}