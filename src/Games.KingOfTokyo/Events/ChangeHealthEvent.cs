using System;
using System.Linq;
using Games.KingOfTokyo.State.Models;
using Games.Shared;

namespace Games.KingOfTokyo.Events
{
    public class ChangeHealthEvent : IGameEvent<KingOfTokyoGame>
    {
        public ChangeHealthEvent(int lockState, Guid executingPlayer, Guid changePlayer, int points, params CardType[] cardSources)
        {
            LockState = lockState;
            ExecutingPlayer = executingPlayer;
            ChangePlayer = changePlayer;
            HitPoints = points;
            CardSources = cardSources.Select(c => c.ToString()).ToArray();
        }

        public string EventType => "ChangeHealth";

        public Guid ExecutingPlayer { get; }

        public Guid ChangePlayer { get; }

        public int LockState { get; }

        public int HitPoints { get; }

        public string[] CardSources { get; }
    }
}