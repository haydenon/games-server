using System.Linq;
using Games.KingOfTokyo.State.Models;
using Games.KingOfTokyo.View;
using Games.Shared;

namespace Games.KingOfTokyo.Events
{
    public class GameStartEvent : IGameEvent<KingOfTokyoGame>
    {
        public GameStartEvent(int lockState, int startingPlayer, Card[] storeCards)
        {
            LockState = lockState;
            StartingPlayerIndex = startingPlayer;
            StoreCards = storeCards.Select(KingOfTokyoCardView.FromCard).ToArray();
        }

        public int StartingPlayerIndex { get; }

        public KingOfTokyoCardView[] StoreCards { get; }

        public string EventType => "GameStart";

        public int LockState { get; }
    }
}