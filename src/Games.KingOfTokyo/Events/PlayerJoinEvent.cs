using Games.KingOfTokyo.View;
using Games.Shared;

namespace Games.KingOfTokyo.Events
{
    public class PlayerJoinEvent : IGameEvent<KingOfTokyoGame>
    {
        public PlayerJoinEvent(int lockState, KingOfTokyoPlayerView player, int index)
        {
            LockState = lockState;
            Player = player;
            PlayerIndex = index;
        }

        public int PlayerIndex { get; }
        public KingOfTokyoPlayerView Player { get; }

        public string EventType => "PlayerJoin";

        public int LockState { get; }
    }
}