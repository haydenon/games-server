using System;
using Games.Shared;

namespace Games.KingOfTokyo.Events
{
    public class RefuseCardBuyEvent : IGameEvent<KingOfTokyoGame>
    {
        public RefuseCardBuyEvent(int lockState, Guid player, string reason)
        {
            LockState = lockState;
            Player = player;
            Reason = reason;
        }

        public string EventType => "RefuseCardBuy";

        public int LockState { get; }

        public Guid Player { get; }

        public string Reason { get; }
    }
}