using System.Linq;
using Games.KingOfTokyo.State.Models;
using Games.KingOfTokyo.View;
using Games.Shared;

namespace Games.KingOfTokyo.Events
{
    public class DiscardCardsEvent : IGameEvent<KingOfTokyoGame>
    {
        public DiscardCardsEvent(int lockState, Card[] cardStore)
        {
            LockState = lockState;
            CardStore = cardStore.Select(KingOfTokyoCardView.FromCard).ToArray();
        }

        public string EventType => "DiscardCards";

        public int LockState { get; }

        public KingOfTokyoCardView[] CardStore { get; }
    }
}