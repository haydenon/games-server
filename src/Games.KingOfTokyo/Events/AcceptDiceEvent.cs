using Games.Shared;

namespace Games.KingOfTokyo.Events
{
    public class AcceptDiceEvent : IGameEvent<KingOfTokyoGame>
    {
        public AcceptDiceEvent(int lockState)
        {
            LockState = lockState;
        }

        public string EventType => "AcceptDice";

        public int LockState { get; }
    }
}