using System;
using Games.Shared;

namespace Games.KingOfTokyo.Events
{
    public class EnteredTokyoEvent : IGameEvent<KingOfTokyoGame>
    {
        public EnteredTokyoEvent(int lockState, Guid player, bool wasBay)
        {
            LockState = lockState;
            WasTokyoBay = wasBay;
            Player = player;
        }

        public string EventType => "EnteredTokyo";

        public Guid Player { get; }

        public int LockState { get; }

        public bool WasTokyoBay { get; }
    }
}