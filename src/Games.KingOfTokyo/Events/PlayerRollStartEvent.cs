using System;
using Games.Shared;

namespace Games.KingOfTokyo.Events
{
    public class PlayerRollStartEvent : IGameEvent<KingOfTokyoGame>
    {
        public PlayerRollStartEvent(int lockState, Guid player)
        {
            LockState = lockState;
            Player = player;
        }

        public Guid Player { get; }

        public string EventType => "PlayerRollStart";

        public int LockState { get; }
    }
}