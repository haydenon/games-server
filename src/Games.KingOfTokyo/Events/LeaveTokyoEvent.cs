using System;
using Games.Shared;

namespace Games.KingOfTokyo.Events
{
    public class LeaveTokyoEvent : IGameEvent<KingOfTokyoGame>
    {
        public LeaveTokyoEvent(int lockState, Guid player, bool leavingTokyo)
        {
            LockState = lockState;
            Player = player;
            LeavingTokyo = leavingTokyo;
        }

        public string EventType => "LeaveTokyo";

        public int LockState { get; }

        public Guid Player { get; }

        public bool LeavingTokyo { get; }
    }
}