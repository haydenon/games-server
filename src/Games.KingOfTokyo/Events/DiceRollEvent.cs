using System.Linq;
using Games.KingOfTokyo.State.Models;
using Games.KingOfTokyo.View.Models;
using Games.Shared;

namespace Games.KingOfTokyo.Events
{
    public class DiceRollEvent : IGameEvent<KingOfTokyoGame>
    {
        public DiceRollEvent(int lockState, DiceRoll[] diceRoll)
        {
            LockState = lockState;
            DiceRoll = diceRoll.Select(DiceRollView.FromDiceRoll).ToArray();
        }

        public string EventType => "DiceRoll";

        public int LockState { get; }

        public DiceRollView[] DiceRoll { get; }
    }
}