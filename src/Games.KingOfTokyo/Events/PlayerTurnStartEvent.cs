using System;
using Games.Shared;

namespace Games.KingOfTokyo.Events
{
    public class PlayerTurnStartEvent : IGameEvent<KingOfTokyoGame>
    {
        public PlayerTurnStartEvent(int lockState, Guid player)
        {
            LockState = lockState;
            Player = player;
        }

        public Guid Player { get; }

        public string EventType => "PlayerTurnStart";

        public int LockState { get; }
    }
}