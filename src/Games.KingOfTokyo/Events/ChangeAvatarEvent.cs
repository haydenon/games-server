using System;
using Games.KingOfTokyo.State.Player;
using Games.Shared;

namespace Games.KingOfTokyo.Events
{
    public class ChangeAvatarEvent : IGameEvent<KingOfTokyoGame>
    {
        public ChangeAvatarEvent(int lockState, Guid player, AvatarType? avatar)
        {
            LockState = lockState;
            Player = player;
            Avatar = avatar?.ToString();
        }

        public string EventType => "ChangeAvatar";

        public int LockState { get; }

        public Guid Player { get; }

        public string? Avatar { get; }
    }
}