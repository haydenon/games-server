using System;
using Games.KingOfTokyo.State;
using Games.Shared;

namespace Games.KingOfTokyo.Events
{
    public class LeaveTokyoRequestEvent : IGameEvent<KingOfTokyoGame>
    {
        public LeaveTokyoRequestEvent(int lockState, Guid[] players, LeaveTokyoNextState nextState)
        {
            LockState = lockState;
            LeaveRequestPlayers = players;
            NextState = nextState.ToString();
        }

        public string EventType => "LeaveTokyoRequest";

        public int LockState { get; }

        public Guid[] LeaveRequestPlayers { get; }

        public string NextState { get; }
    }
}