using System;
using Games.KingOfTokyo.State.Models;
using Games.KingOfTokyo.View;
using Games.Shared;

namespace Games.KingOfTokyo.Events
{
    public class BuyCardEvent : IGameEvent<KingOfTokyoGame>
    {
        public BuyCardEvent(int lockState, Guid player, Card card, int index, Card replacementCard)
        {
            LockState = lockState;
            Player = player;
            Card = KingOfTokyoCardView.FromCard(card);
            Index = index;
            ReplacementCard = KingOfTokyoCardView.FromCard(replacementCard);
        }

        public string EventType => "BuyCard";

        public int LockState { get; }

        public Guid Player { get; }

        public KingOfTokyoCardView Card { get; }

        public int Index { get; }

        public KingOfTokyoCardView ReplacementCard { get; }
    }
}