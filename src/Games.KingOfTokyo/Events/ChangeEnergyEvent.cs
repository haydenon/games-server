using System;
using System.Linq;
using Games.KingOfTokyo.State.Models;
using Games.Shared;

namespace Games.KingOfTokyo.Events
{
    public class ChangeEnergyEvent : IGameEvent<KingOfTokyoGame>
    {
        public ChangeEnergyEvent(int lockState, Guid executingPlayer, Guid changePlayer, int energy, params CardType[] cardSources)
        {
            LockState = lockState;
            ExecutingPlayer = executingPlayer;
            ChangePlayer = changePlayer;
            Energy = energy;
            CardSources = cardSources.Select(c => c.ToString()).ToArray();
        }

        public string EventType => "ChangeEnergy";

        public Guid ExecutingPlayer { get; }

        public Guid ChangePlayer { get; }

        public int LockState { get; }

        public int Energy { get; }

        public string[] CardSources { get; }
    }
}