using System;
using Games.Shared;

namespace Games.KingOfTokyo.Events
{
    public class PlayerWinEvent : IGameEvent<KingOfTokyoGame>
    {
        public PlayerWinEvent(int lockState, Guid? player)
        {
            LockState = lockState;
            Player = player;
            IsTie = !player.HasValue;
        }

        public Guid? Player { get; }

        public bool IsTie { get; }

        public string EventType => "GameEnd";

        public int LockState { get; }
    }
}