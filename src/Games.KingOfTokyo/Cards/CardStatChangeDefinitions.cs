using System.Collections.Generic;
using System.Collections.ObjectModel;
using Games.KingOfTokyo.State.Models;
using Games.KingOfTokyo.State.Player;
using static Games.KingOfTokyo.Cards.CardStatUpdate;
using static Games.KingOfTokyo.Cards.CardStatUpdate.CardTarget;
using static Games.KingOfTokyo.Cards.CardStatUpdate.CardStatType;
using System;

namespace Games.KingOfTokyo.Cards
{
    public static class CardStatChangeDefinitions
    {
        static CardStatChangeDefinitions()
        {
            CreateCard(CardType.CommuterTrain, Update(VictoryPoints, Self, 2));
            CreateCard(CardType.JetFighters, Update(VictoryPoints, Self, 5), Update(HitPoints, Self, -5));
            CreateCard(CardType.NationalGuard, Update(VictoryPoints, Self, 2), Update(HitPoints, Self, -2));
            CreateCard(CardType.Skyscraper, Update(VictoryPoints, Self, 4));
            CreateCard(CardType.ApartmentBuilding, Update(VictoryPoints, Self, 3));
            CreateCard(CardType.EvacuationOrders, Update(VictoryPoints, Others, -5));
            CreateCard(CardType.VastStorm, Update(VictoryPoints, Self, 2), Update(Energy, Others, VastStormEnergyAmount));
            CreateCard(CardType.CornerStore, Update(VictoryPoints, Self, 1));
            CreateCard(CardType.Energize, Update(Energy, Self, 9));
            CreateCard(CardType.FlameThrower, Update(HitPoints, Others, -2));
            CreateCard(CardType.GasRefinery, Update(VictoryPoints, Self, 2), Update(HitPoints, Others, -3));
            CreateCard(CardType.Heal, Update(HitPoints, Self, 2));
            CreateCard(CardType.HighAltitudeBombing, Update(HitPoints, Everyone, -3));
            CreateCard(CardType.NuclearPowerPlant, Update(VictoryPoints, Self, 2), Update(HitPoints, Self, 3));
            CreateCard(CardType.DeathFromAbove, Update(VictoryPoints, Self, 2));
        }

        private static readonly Dictionary<CardType, CardStatUpdate[]> cardValues = new Dictionary<CardType, CardStatUpdate[]>();

        public static CardStatUpdate[] UpdatesForCardType(CardType type) => cardValues[type] ?? new CardStatUpdate[0];

        private static void CreateCard(CardType cardType, params CardStatUpdate[] updates)
            => cardValues.Add(cardType, updates);

        private static int VastStormEnergyAmount(PlayerState state)
                    => -(int)Math.Floor(state.Energy / 2.0);
    }
}