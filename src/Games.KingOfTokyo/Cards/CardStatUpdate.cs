using System;
using Games.KingOfTokyo.State.Player;

namespace Games.KingOfTokyo.Cards
{
    public class CardStatUpdate
    {
        public CardStatUpdate(CardStatType statType, CardTarget target, int delta)
        {
            StatType = statType;
            Target = target;
            CalcDelta = (PlayerState _) => delta;
        }

        public CardStatUpdate(CardStatType statType, CardTarget target, Func<PlayerState, int> calcDelta)
        {
            StatType = statType;
            Target = target;
            CalcDelta = calcDelta;
        }

        public CardStatType StatType { get; }
        public CardTarget Target { get; }

        public Func<PlayerState, int> CalcDelta { get; }

        public enum CardTarget
        {
            Self,
            Others,
            Everyone
        }

        public enum CardStatType
        {
            HitPoints,
            VictoryPoints,
            Energy
        }

        public static CardStatUpdate Update(CardStatType statType, CardTarget target, int delta)
            => new CardStatUpdate(statType, target, delta);

        public static CardStatUpdate Update(CardStatType statType, CardTarget target, Func<PlayerState, int> calcDelta)
            => new CardStatUpdate(statType, target, calcDelta);
    }
}