using System;
using System.Collections.Generic;
using System.Linq;
using Games.KingOfTokyo.State.Models;
using static Games.KingOfTokyo.State.Models.CardType;

namespace Games.KingOfTokyo.Cards
{
    public static class CardFactory
    {
        private static readonly Random random = new Random();

        private static IList<Card> AllCards = new List<Card>();

        static CardFactory()
        {
            AddCard("Commuter train", CommuterTrain, 4, true);
            AddCard("Jet fighters", JetFighters, 5, true);
            AddCard("National guard", NationalGuard, 3, true);
            AddCard("Skyscraper", Skyscraper, 6, true);
            AddCard("Apartment building", ApartmentBuilding, 5, true);
            AddCard("Evacuation orders", EvacuationOrders, 7, true);
            AddCard("Vast storm", VastStorm, 6, true);
            AddCard("Corner store", CornerStore, 3, true);
            AddCard("Energize", Energize, 8, true);
            AddCard("Flame thrower", FlameThrower, 3, true);
            AddCard("Gas refinery", GasRefinery, 6, true);
            AddCard("Heal", Heal, 3, true);
            AddCard("High altitude bombing", HighAltitudeBombing, 4, true);
            AddCard("Nuclear power plant", NuclearPowerPlant, 6, true);
            AddCard("Death from above", DeathFromAbove, 5, true);

            AddCard("Giant brain", GiantBrain, 5, false);
            AddCard("Extra head", ExtraHead, 7, false);
            AddCard("Spiked tail", SpikedTail, 5, false);
            AddCard("Rooting for the underdog", RootingForTheUnderdog, 3, false);
            AddCard("Energy Hoarder", EnergyHoarder, 3, false);
            AddCard("Friend of Children", FriendOfChildren, 3, false);
            AddCard("Regeneration", Regeneration, 4, false);
        }

        public static Card GetRandomCard()
        {
            var index = random.Next(AllCards.Count);
            return AllCards[index];
        }

        public static int RemainingCards(KingOfTokyoGame kingOfTokyoGame)
         => Math.Max(0, AllCards.Count - UsedTypes(kingOfTokyoGame, new CardType[0]).Count);

        public static Card[] GetUnusedCards(KingOfTokyoGame game, int count, CardType[]? excluded = null)
        {
            excluded = excluded ?? new CardType[0];
            var cards = UsedTypes(game, excluded);
            var returnCards = new Card[count];
            for (var i = 0; i < count; i++)
            {
                var card = GetUnusedCard(cards);
                returnCards[i] = card;
                cards.Add(card.CardType);
            }

            return returnCards;
        }

        public static Card GetUnusedCard(KingOfTokyoGame game)
            => GetUnusedCard(UsedTypes(game, new CardType[0]));

        private static Card GetUnusedCard(IList<CardType> usedTypes)
        {
            var usedCounts = usedTypes.GroupBy(t => t).ToDictionary(t => t.Key, t => t.Count());
            var allCards = AllCards.GroupBy(c => c.CardType);
            var remainingCards = allCards.SelectMany(c =>
            {
                var cards = c.ToArray();
                var totalCount = cards.Length;
                var count = usedCounts.ContainsKey(c.Key) ? usedCounts[c.Key] : 0;
                var remaining = totalCount - count;
                return cards.Take(remaining);
            }).ToArray();

            var index = random.Next(remainingCards.Length);
            return remainingCards[index];
        }

        private static IList<CardType> UsedTypes(KingOfTokyoGame kingOfTokyoGame, CardType[] excluded)
        {
            var usedTypes = new List<CardType>(AllCards.Count);
            usedTypes.AddRange(kingOfTokyoGame.CardStore.Select(c => c.CardType));
            usedTypes.AddRange(kingOfTokyoGame.DiscardedCards);
            foreach (var inventory in kingOfTokyoGame.PlayerInventories ?? new Dictionary<Guid, CardType[]>())
            {
                usedTypes.AddRange(inventory.Value);
            }
            usedTypes.AddRange(kingOfTokyoGame.DiscardedCards);
            usedTypes.AddRange(excluded);
            return usedTypes;
        }

        private static void AddCard(string display, CardType cardType, int cost, bool isDiscard)
        {
            AllCards.Add(new Card
            {
                DisplayName = display,
                CardType = cardType,
                EnergyCost = cost,
                IsDiscard = isDiscard
            });
        }
    }
}