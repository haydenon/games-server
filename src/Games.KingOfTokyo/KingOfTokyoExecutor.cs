using System.Collections.Generic;
using System.Linq;
using Games.Shared;
using Games.Shared.StateUpdates;
using LanguageExt;

namespace Games.KingOfTokyo
{
    public class KingOfTokyoExecutor : IGameExecutor<KingOfTokyoGame>
    {
        private readonly IStateUpdater<KingOfTokyoGame> updater;
        private readonly ICollection<IActionHandler<KingOfTokyoGame>> actionHandlers;

        public KingOfTokyoExecutor(
            IStateUpdater<KingOfTokyoGame> updater,
            ICollection<IActionHandler<KingOfTokyoGame>> actionHandlers)
        {
            this.updater = updater;
            this.actionHandlers = actionHandlers;
        }

        public Either<string, ActionHandleResult<KingOfTokyoGame>> ExecuteAction(KingOfTokyoGame game, IAction<KingOfTokyoGame> action)
        {
            var handler = actionHandlers.FirstOrDefault(h => h.CanHandle(game));
            if (handler == null)
            {
                return $"Cannot perform {action.Name} action at the moment.";
            }

            return from _ in handler.ValidateAction(game, action)
                   let result = handler.HandleAction(updater, game, action)
                   select result;
        }
    }
}