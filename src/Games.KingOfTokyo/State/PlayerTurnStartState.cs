using System;

namespace Games.KingOfTokyo.State
{
    public class PlayerTurnStartState : IState
    {
        public Guid Player { get; set; }
    }
}