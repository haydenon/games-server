namespace Games.KingOfTokyo.State.Models
{
    public enum DiceRollType
    {
        One,
        Two,
        Three,
        Heart,
        Energy,
        Claw
    }
}