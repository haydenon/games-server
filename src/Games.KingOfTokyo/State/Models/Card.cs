using System;

namespace Games.KingOfTokyo.State.Models
{
    public class Card
    {
        public string? DisplayName { get; set; }

        public CardType CardType { get; set; }

        public int EnergyCost { get; set; }

        public bool IsDiscard { get; set; }
    }
}