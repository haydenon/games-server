namespace Games.KingOfTokyo.State.Models
{
    public enum CardType
    {
        CommuterTrain,
        JetFighters,
        NationalGuard,
        Skyscraper,
        ApartmentBuilding,
        EvacuationOrders,
        VastStorm,
        CornerStore,
        Energize,
        FlameThrower,
        GasRefinery,
        Heal,
        HighAltitudeBombing,
        NuclearPowerPlant,
        DeathFromAbove,
        GiantBrain,
        SpikedTail,
        ExtraHead,
        RootingForTheUnderdog,
        EnergyHoarder,
        FriendOfChildren,
        Regeneration
    }
}