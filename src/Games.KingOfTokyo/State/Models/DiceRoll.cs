using System;

namespace Games.KingOfTokyo.State.Models
{
    public class DiceRoll
    {
        public DiceRollType DiceRollType { get; set; }

        public bool WasPreserved { get; set; }
    }
}