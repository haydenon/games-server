using System;

namespace Games.KingOfTokyo.State
{
    public class LeaveTokyoState : IState
    {
        public Guid OriginalPlayer { get; set; }
        public LeaveTokyoNextState NextState { get; set; }
        public Guid[]? AwaitingConfirmationPlayers { get; set; }
    }

    public enum LeaveTokyoNextState
    {
        EnterTokyo,
        BuyCard,
        PlayerTurnStart
    }
}