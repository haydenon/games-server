using System;

namespace Games.KingOfTokyo.State
{
    public class PlayerWinState : IState
    {
        public Guid? Player { get; set; }
    }
}