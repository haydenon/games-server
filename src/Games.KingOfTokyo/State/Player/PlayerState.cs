using System;

namespace Games.KingOfTokyo.State.Player
{
    public class PlayerState
    {
        public int HitPoints { get; set; }

        public int VictoryPoints { get; set; }

        public int Energy { get; set; }

        public static PlayerState CreateDefaultState() => new PlayerState
        {
            HitPoints = 10,
            VictoryPoints = 0,
            Energy = 0
        };

        public int UpdateHitPoints(int delta, int maxHitPoints)
        {
            var original = HitPoints;
            HitPoints = Math.Max(0, Math.Min(maxHitPoints, HitPoints + delta));
            return HitPoints - original;
        }

        public int UpdateVictoryPoints(int delta)
        {
            var original = VictoryPoints;
            VictoryPoints = Math.Min(20, Math.Max(0, VictoryPoints + delta));
            return VictoryPoints - original;
        }

        public int UpdateEnergy(int delta)
        {
            var original = Energy;
            Energy = Math.Max(0, Energy + delta);
            return Energy - original;
        }

        public bool IsDead => HitPoints <= 0;

        public bool IsAtFullHealth(int maxHitPoints) => HitPoints >= maxHitPoints;
    }
}