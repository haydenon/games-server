namespace Games.KingOfTokyo.State.Player
{
    public enum AvatarType
    {
        Alienoid,
        CyberKitty,
        Gigazaur,
        MekaDragon,
        SpacePenguin,
        TheKing
    }
}