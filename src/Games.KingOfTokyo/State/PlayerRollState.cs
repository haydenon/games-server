using System;

namespace Games.KingOfTokyo.State
{
    public class PlayerRollState : IState
    {
        public Guid Player { get; set; }
        public int RollNumber { get; set; }
    }
}