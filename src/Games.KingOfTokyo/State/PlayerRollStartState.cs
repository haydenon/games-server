using System;

namespace Games.KingOfTokyo.State
{
    public class PlayerRollStartState : IState
    {
        public Guid Player { get; set; }
    }
}