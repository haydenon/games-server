using System;
using System.Linq;
using Games.KingOfTokyo.State.Models;

namespace Games.KingOfTokyo
{
    public static class KingOfTokyoGameExtensions
    {
        public static bool IsPlayerDead(this KingOfTokyoGame game, Guid player)
        {
            var playerState = game.PlayerState?[player];
            return playerState == null || playerState.IsDead;
        }

        public static bool IsInTokyo(this KingOfTokyoGame game, Guid player)
            => game.PlayerInTokyoBay == player || game.PlayerInTokyoCity == player;

        public static Guid[] PlayersOutsideTokyo(this KingOfTokyoGame game)
            => game.Players
                .Select(p => p.Id)
                .Where(p => !game.IsInTokyo(p) && !game.IsPlayerDead(p))
                .ToArray();

        public static bool HasCard(this KingOfTokyoGame game, Guid player, CardType cardType)
        {
            var inventory = game.PlayerInventories?[player];
            return inventory != null && inventory.Any(c => c == cardType);
        }

        public static int CardCount(this KingOfTokyoGame game, Guid player, CardType cardType)
        {
            var inventory = game.PlayerInventories?[player];
            return inventory != null ? inventory.Count(c => c == cardType) : 0;
        }
    }
}