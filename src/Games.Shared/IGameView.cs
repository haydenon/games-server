namespace Games.Shared
{
    public interface IGameView
    {
        string? AccessingPlayerId { get; set; }
    }
}