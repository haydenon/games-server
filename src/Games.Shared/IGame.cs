namespace Games.Shared
{
    public interface IGame : IUserModel
    {
        int LockState { get; }
    }
}