using System;

namespace Games.Shared
{
    public interface IUserModel : IModel
    {
        Guid OwnerId { get; }
    }
}