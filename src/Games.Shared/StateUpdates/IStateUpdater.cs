using System;

namespace Games.Shared.StateUpdates
{
    public interface IStateUpdater<T> where T : IGame
    {
        UpdateFunc<T> Update(IStateUpdateDefinition definition);
    }
}