using System;
using System.Collections.Generic;
using System.Linq;

namespace Games.Shared.StateUpdates
{
    public class StateUpdater<T> : IStateUpdater<T> where T : IGame
    {
        private readonly ICollection<StateUpdaterInstantiator<T>> instantiators;

        public StateUpdater(ICollection<StateUpdaterInstantiator<T>> instantiators)
        {
            this.instantiators = instantiators;
        }

        public UpdateFunc<T> Update(
            IStateUpdateDefinition definition)
            => instantiators.First(i => i.CanInstantiate(definition)).Instantiate(this, definition);
    }
}