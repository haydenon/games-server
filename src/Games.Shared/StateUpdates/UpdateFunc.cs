namespace Games.Shared.StateUpdates
{
    public delegate ActionHandleResult<T> UpdateFunc<T>(ActionHandleResult<T> result) where T : IGame;
}