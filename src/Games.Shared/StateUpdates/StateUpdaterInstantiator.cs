using System;

namespace Games.Shared.StateUpdates
{
    public class StateUpdaterInstantiator<T> where T : IGame
    {
        private readonly Func<object, bool> canInstantiate;
        private readonly Func<IStateUpdater<T>, IStateUpdateDefinition, object> instantiate;

        private StateUpdaterInstantiator(
            Func<object, bool> canInstantiate,
            Func<IStateUpdater<T>, IStateUpdateDefinition, object> instantiate
        )
        {
            this.canInstantiate = canInstantiate;
            this.instantiate = instantiate;
        }

        public bool CanInstantiate(IStateUpdateDefinition definition)
            => canInstantiate(definition);

        public UpdateFunc<T> Instantiate(
            IStateUpdater<T> updaterFactory,
            IStateUpdateDefinition definition)
        {
            var instantiated = instantiate(updaterFactory, definition);
            if (instantiated is UpdateFunc<T> updater)
            {
                return updater;
            }

            throw new InvalidOperationException("Cannot create state updater");
        }

        public static StateUpdaterInstantiator<T> Create<Definition>(
            Func<IStateUpdater<T>, Definition, UpdateFunc<T>> updaterConstructor
        )
            where Definition : IStateUpdateDefinition
        {
            Func<object, bool> canInstantiate = (object definition) => definition is Definition;
            Func<IStateUpdater<T>, IStateUpdateDefinition, object> create = (factory, definition) =>
            {
                if (definition is Definition def)
                {
                    return updaterConstructor(factory, def);
                }

                throw new InvalidOperationException("Cannot create state updater");
            };
            return new StateUpdaterInstantiator<T>(canInstantiate, create);
        }
    }

    public class StateUpdaterInstantiatorBuilder<T> : IStateUpdaterInstantiator<T> where T : IGame
    {
        public StateUpdaterInstantiator<T> Create<Definition>(
                    Func<IStateUpdater<T>, Definition, UpdateFunc<T>> updaterConstructor
                ) where Definition : IStateUpdateDefinition
                => StateUpdaterInstantiator<T>.Create(updaterConstructor);
    }
}