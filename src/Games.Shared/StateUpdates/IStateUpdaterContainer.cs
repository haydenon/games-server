using System.Collections.Generic;

namespace Games.Shared.StateUpdates
{
    public interface IStateUpdaterContainer<T> where T : IGame
    {
        IEnumerable<StateUpdaterInstantiator<T>> GetInstantiators(IStateUpdaterInstantiator<T> instantiator);
    }
}