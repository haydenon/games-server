using System;

namespace Games.Shared.StateUpdates
{
    public interface IStateUpdaterInstantiator<T> where T : IGame
    {
        StateUpdaterInstantiator<T> Create<Definition>(
            Func<IStateUpdater<T>, Definition, UpdateFunc<T>> updaterConstructor
        ) where Definition : IStateUpdateDefinition;
    }
}