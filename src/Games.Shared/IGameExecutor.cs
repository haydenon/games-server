﻿using LanguageExt;

namespace Games.Shared
{
    public interface IGameExecutor<T> where T : IGame
    {
        Either<string, ActionHandleResult<T>> ExecuteAction(T game, IAction<T> action);
    }
}
