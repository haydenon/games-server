using System;

namespace Games.Shared
{
    public interface IModel
    {
        Guid Id { get; }
    }
}