namespace Games.Shared
{
    public interface IGameEvent<T> where T : IGame
    {
        string EventType { get; }

        int LockState { get; }
    }
}