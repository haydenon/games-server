using System;
using System.Collections.Generic;
using System.Linq;
using Games.Shared.StateUpdates;
using LanguageExt;

namespace Games.Shared
{
    public class ActionHandleResult<T> where T : IGame
    {
        public ActionHandleResult(T updatedState, Guid player, params IGameEvent<T>[] events)
        {
            GameState = updatedState;
            Events = events.ToList() ?? new List<IGameEvent<T>>();
            ExecutingPlayer = player;
        }

        public Guid ExecutingPlayer { get; }
        public T GameState { get; }
        public IList<IGameEvent<T>> Events { get; private set; }

        public ActionHandleResult<T> Append(ActionHandleResult<T> result)
        {
            if (!object.ReferenceEquals(result.GameState, GameState))
            {
                throw new ArgumentException(nameof(result), "Must provide a result with the same game");
            }

            if (result.Events.Count == 0)
            {
                return this;
            }

            Events = Events.Append(result.Events).ToList();
            return this;
        }

        public ActionHandleResult<T> Append(params IGameEvent<T>[] events)
        {
            if (events.Count() == 0)
            {
                return this;
            }

            Events = Events.Append(events).ToList();
            return this;
        }

        public ActionHandleResult<T> Then(UpdateFunc<T> updater)
            => updater(this);
    }
}