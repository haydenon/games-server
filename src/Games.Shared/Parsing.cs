using System.Collections.Generic;
using System.Text.Json;
using LanguageExt;
using static LanguageExt.Prelude;

namespace Games.Shared
{
    public static class Parsing
    {
        public static Either<string, bool[]> ToBoolArray(JsonElement element, string elementName)
        {
            if (element.ValueKind != JsonValueKind.Array)
            {
                return $"{elementName} is not an array";
            }

            var array = element.EnumerateArray().ToArr();
            if (array.Any(e => e.ValueKind != JsonValueKind.False && e.ValueKind != JsonValueKind.True))
            {
                return $"{elementName} contains non boolean values";
            }

            return array.Select(e => e.GetBoolean()).ToArray();
        }

        public static Either<string, JsonElement> GetProperty(JsonElement element, string elementName)
        {
            if (element.ValueKind != JsonValueKind.Object)
            {
                return "Element isn't an object.";
            }

            if (element.TryGetProperty(elementName, out var propElement))
            {
                return propElement;
            }
            return $"Property {elementName} does not exist on the object";
        }

        public static Either<string, T[]> ToEnumArray<T>(JsonElement element, string elementName)
            where T : struct
        {
            if (element.ValueKind != JsonValueKind.Array)
            {
                return $"{elementName} is not an array";
            }

            var array = element.EnumerateArray().ToArr();
            if (array.Any(e => e.ValueKind != JsonValueKind.String))
            {
                return $"{elementName} contains invalid values";
            }

            return MapOptionArray(
                array.Select(e => parseEnumIgnoreCase<T>(e.GetString())),
                $"Invalid elements for {elementName}"
            );
        }

        private static Either<string, T[]> MapOptionArray<T>(Arr<Option<T>> items, string error)
            => items.Fold<Either<string, List<T>>>(
                new List<T>(),
                (list, b) => list.Bind((List<T> ls) =>
                    b.Match<Either<string, List<T>>>(item =>
                    {
                        ls.Add(item);
                        return ls;
                    }, () => error)))
                    .Map(ls => ls.ToArray());

        public static Option<string> GetStringProperty(JsonElement element, string property)
        {
            if (element.ValueKind != JsonValueKind.Object)
            {
                return None;
            }

            if (!element.TryGetProperty(property, out var prop) || prop.ValueKind != JsonValueKind.String)
            {
                return None;
            }

            var result = prop.GetString();
            return result != null ? (Option<string>)result : None;
        }
    }
}