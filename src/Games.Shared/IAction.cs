namespace Games.Shared
{
    public interface IAction<T> where T : IGame
    {
        string Name { get; }
        IPlayer<T> ExecutingPlayer { get; }
    }
}