using Games.Shared.StateUpdates;
using LanguageExt;

namespace Games.Shared
{
    public interface IActionHandler<T> where T : IGame
    {
        bool CanHandle(T game);

        Either<string, Unit> ValidateAction(T game, IAction<T> action);

        ActionHandleResult<T> HandleAction(IStateUpdater<T> factory, T game, IAction<T> action);
    }
}