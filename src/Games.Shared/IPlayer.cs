using System;

namespace Games.Shared
{
    public interface IPlayer<T> where T : IGame
    {
        Guid Id { get; }
        Guid GameId { get; }
        string? Name { get; }
    }
}