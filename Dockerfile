FROM mcr.microsoft.com/dotnet/sdk:5.0@sha256:eda85a60aeb00e7904347d321095d8115f171dacc5d84feba4644ca4905c0204 AS build-env
WORKDIR /app

# Copy project files
COPY ./src/Games.Shared/Games.Shared.csproj ./src/Games.Shared/
COPY ./src/Games.Data/Games.Data.csproj ./src/Games.Data/
COPY ./src/Games.KingOfTokyo/Games.KingOfTokyo.csproj ./src/Games.KingOfTokyo/
COPY ./src/Games/Games.csproj ./src/Games/

RUN cd ./src/Games && dotnet restore

# Copy everything else and build
COPY ./src ./src
RUN dotnet publish -c Release -o out ./src/Games

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:5.0@sha256:a0060fb799d2fea733be97c24ddb58c98462b1e909ee8e82f3bd1cd4871ba167 AS bin
WORKDIR /app

ENV NEW_RELIC_VERSION=newrelic-netcore20-agent
ENV CORECLR_NEWRELIC_HOME=/usr/local/${NEW_RELIC_VERSION}
RUN apt-get update -y && apt-get install wget gnupg -y \
  && echo 'deb http://apt.newrelic.com/debian/ newrelic non-free' | tee /etc/apt/sources.list.d/newrelic.list \
  && wget -O- https://download.newrelic.com/548C16BF.gpg | apt-key add - \
  && apt-get update -y && apt-get install -y ${NEW_RELIC_VERSION} \
  && apt-get remove -y wget

COPY --from=build-env /app/out .
ENTRYPOINT ["/usr/local/newrelic-netcore20-agent/run.sh", "dotnet", "Games.dll"]